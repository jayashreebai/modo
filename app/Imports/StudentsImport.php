<?php

namespace App\Imports;

use App\School\Student;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StudentsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Student([
            'name'     => $row['name'],
            'dob' => $row['dob'],
            'cluster' => $row['cluster'],
            'batch' => $row['batch'],
            'gender' => $row['gender']
        ]);
    }
}
