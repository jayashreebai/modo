<?php

namespace App\Jobs;

use App\Http\Controllers\SupportingFunctions;
use App\Models\School\CourseContent;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PopulateCourseContent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $modules;
    public $schoolCourseId;

    /**
     * Create a new job instance for adding module content to coursecontent table
     *
     * @return void
     */
    public function __construct($modules, $schoolCourseId)
    {
        $this->modules = $modules;
        $this->schoolCourseId = $schoolCourseId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->modules as $module) {
            $lessons = json_decode($module->lessons, true);
            $activeLesson = SupportingFunctions::multivalue('active', 1, $lessons)->toArray();

            usort($activeLesson, function ($a, $b) {
                return strcmp($a['order'], $b['order']);
            });

            CourseContent::create(['school_course_id' => $this->schoolCourseId,
                                        'module_id' => $module->id,
                                        'module_content' => json_encode($activeLesson),
                                        'order' => $module->order]);
        }
    }
}
