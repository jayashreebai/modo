<?php

namespace App\Jobs\Password;

use App\Mail\Password\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendPasswordResetMail implements ShouldQueue
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Mail Job
    |--------------------------------------------------------------------------
    |
    | This file will create a job in queue and will send
    | the mail to the user's mail.
    |
     */

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    public $token;
    public $flag;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $token, $flag)
    {
        $this->user = $user;
        $this->token = $token;
        $this->flag = $flag;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Sending mail
        Mail::to($this->user->email)->send(new PasswordReset($this->token, $this->user, $this->flag));
    }
}
