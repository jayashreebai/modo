<?php

namespace App\Jobs\Password;

use App\Mail\Password\PasswordChanged;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendPasswordChangedMail implements ShouldQueue
{
    /*
    |--------------------------------------------------------------------------
    | Password Changed Mail Job
    |--------------------------------------------------------------------------
    |
    | This file will create a job in queue and will send
    | the mail to the user's mail.
    |
     */

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Sending mail
        Mail::to($this->user->email)->send(new PasswordChanged($this->user));
    }
}
