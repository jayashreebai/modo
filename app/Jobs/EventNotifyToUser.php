<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class EventNotifyToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $user, $batch, $date;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $batch, $date)
    {
        $this->user = $user;
        $this->batch = $batch;
        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user->email)->send(new \App\Mail\EventNotifyToUser($this->user, $this->batch, $this->date));
    }
}
