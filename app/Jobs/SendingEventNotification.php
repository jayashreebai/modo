<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendingEventNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $calendar ,$batch, $flag;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($calendar ,$batch, $flag)
    {
        $this->calendar = $calendar;
        $this->batch = $batch;
        $this->flag = $flag;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->calendar->coordinator->email)->send(new \App\Mail\SendingEventNotification($this->calendar,$this->batch,$this->flag));

    }
}
