<?php

namespace App\Mail\Coordinator;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CoordinatorLogin extends Mailable
{

    /**
     *
     * used when coordinator assigned to school
     *
     */
    use Queueable, SerializesModels;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.coordinator.login')->subject('Modo');

//        return $this->from(getenv('MAIL_FROM_ADDRESS'))
//                    ->markdown('emails.coordinator.login')
//                    ->subject('Assigned Coordinator')
//                    ->with(['user' => $this->user]);
    }
}
