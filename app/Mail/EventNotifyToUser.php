<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventNotifyToUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $batch, $date;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $batch, $date)
    {
        $this->user = $user;
        $this->batch = $batch;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.coordinator.calendarNotify');
    }
}
