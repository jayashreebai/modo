<?php

namespace App\Mail\Password;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReset extends Mailable implements ShouldQueue
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Mail
    |--------------------------------------------------------------------------
    |
    | This file will create a mail file which contains the mail items
    | and will send the mail to the user's mail
    |
     */

    use Queueable, SerializesModels;

    public $token;
    public $user;
    public $flag;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $user, $flag)
    {
        $this->token = $token;
        $this->user = $user;
        $this->flag = $flag;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.password.password-reset')->subject('Modo');

//        return $this->markdown('emails.password.password-reset')
//            ->from(getenv('MAIL_FROM_ADDRESS'))
//            ->subject('Heymodo-Reset Password')
//            ->with(['data' => $this->token, 'user' => $this->user]);
    }
}
