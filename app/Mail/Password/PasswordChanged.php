<?php

namespace App\Mail\Password;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordChanged extends Mailable implements ShouldQueue
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Mail
    |--------------------------------------------------------------------------
    |
    | This file will create a mail file which contains the mail items
    | and will send the mail to the user's mail
    |
     */
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * PasswordChanged constructor.
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.password.password-changed')
            ->from(getenv('MAIL_FROM_ADDRESS'))
            ->subject('Ta-Password Changed')
            ->with(['user' => $this->user]);
    }
}
