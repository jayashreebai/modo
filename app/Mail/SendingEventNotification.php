<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendingEventNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $calendar ,$batch, $flag;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($calendar ,$batch, $flag)
    {
        $this->calendar = $calendar;
        $this->batch = $batch;
        $this->flag = $flag;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.coordinator.CalendarNotification')->subject('Modo');
    }
}
