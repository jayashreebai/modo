<?php
namespace App;

use League\Fractal\Serializer\ArraySerializer;

class DataSerializer extends ArraySerializer
{
    /**
     *
     * This is used to remove data key from the response
     */
    //If given array is collection
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }
        return $data;
    }

    //If given array has single item
    public function item($resourceKey, array $data)
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }
        return $data;
    }
}
