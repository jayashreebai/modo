<?php

namespace App\Console\Commands;

use App\Http\Controllers\School\CalendarController;
use Illuminate\Console\Command;

class SendCalendarDetail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:notifyCoordinator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used for notifying coordinator about the upcoming classes and to unlock course content';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $calendarFeature = new CalendarController();
        $calendarFeature->notify();
    }
}
