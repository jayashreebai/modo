<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class EngagementType extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   EngagementType model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the engagement_types table.
    |   Also takes care of relations between School and EngagementType.
    |
    |
    */

    use SoftDeletes, Sluggable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */

    protected $fillable = [
        'type'
    ];

    protected $hidden = ['deleted_at','updated_at','created_at'];
    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'type',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */

    protected $dates = ['deleted_at'];

    /**
     * One To Many relationship between EngagementType and School
     * EngagementType will have many schools
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
    */

    public function schools()
    {
        return $this->hasMany('App\Models\School\School');
    }
}
