<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   LoginLog model
    |--------------------------------------------------------------------------
    |   This keeps track of different user logs timing
    |
    |
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */

    protected $fillable = [
        'user_id', 'entered_date'
    ];
}
