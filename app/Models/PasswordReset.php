<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    /*
    |--------------------------------------------------------------------------
    | PasswordReset model
    |--------------------------------------------------------------------------
    |
    | Through this file services interacts with the database.
    | Also takes care of the model relations.
    |
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'token'];
}
