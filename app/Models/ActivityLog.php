<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   ActivityLog model
    |--------------------------------------------------------------------------
    |   This keeps track of different user activity.
    |
    |
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */

    protected $fillable = [
        'user_id', 'activity'
    ];
}
