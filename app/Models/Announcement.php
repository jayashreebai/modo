<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Announcement extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Announcement model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the announcements table.
    |  Also takes care of relations between announcement and school.
    |
    */
    use SoftDeletes, Sluggable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'title', 'description', 'url', 'slug'
    ];

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Many to Many relationship between School and Announcement
     * Each announcement belongs to many schools
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     */

    public function school()
    {
        return $this->belongsToMany('App\Models\School\School', 'school_announcements', 'announcement_id', 'school_id')->withPivot('date');
    }
}
