<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Role;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    /*
    |--------------------------------------------------------------------------
    |  User model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the users table.
    |  Also takes care of relations between User, Coordinator, Role etc.
    |
    */

    use Notifiable, SoftDeletes, Sluggable;

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'first_name',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';                                              
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role_id', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * Here only name and role data will be added to JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'name'      => $this->first_name,
            'role'      => $this->role->role,
            'user'      => $this->id,
            'slug'      => $this->slug,
        ];
    }

    /*-------------------Relationships--------------------------------*/

    /**
    * belongsTo relationship between User and Role
    * One User will have one Role
    *
    *  @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * One To One relationship between User and coordinator
     * users will have many coordinators
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasOne
     */

    public function coordinator()
    {
        return $this->hasOne('App\Models\CoordinatorDetail', 'coordinator_id');
    }

    public function schoolAdmin()
    {
        return $this->hasOne('App\Models\School\School', 'school_admin_id');
    }

    /**
     * One To Many relationship between User and Batch
     * Coordinator has many batches
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
    */

    public function batches()
    {
        return $this->hasMany('App\Models\School\Batch', 'coordinator_id');
    }

    /**
     * One To Many relationship between User and calendar
     * coordinator can have planned calendars
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
     */

    public function calendars()
    {
        return $this->hasMany('App\Models\School\Calendar', 'coordinator_id');
    }

    /**
     * Many to Many relationship between School and Coordinator
     * Each coordinator belongs to many schools
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     */

    public function school()
    {
        return $this->belongsToMany('App\Models\School\School', 'school_coordinators', 'coordinator_id', 'school_id');
    }

    /**
     * One To Many relationship between User and Attendance
     * coordinator can have taken attendance
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
     */

    public function attendances()
    {
        return $this->hasMany('App\Models\School\Attendance', 'coordinator_id');
    }
}
