<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   Inventory model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the inventories table.
    |   Also takes care of relations between inventory and inventory category and school.
    |
    */

    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'school_id', 'category_id', 'item_name', 'serial_number', 'quantity'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /***********relationships ************/

    /**
    * One to Many relationship between Inventory and School
    * Each Inventory belongs to one School
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    *
    */

    public function school()
    {
        return $this->belongsTo('App\Models\School\School');
    }

    /**
     * One to Many relationship between Inventory and InventoryCategory
     * Each Inventory belongs to one InventoryCategory
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function category()
    {
        return $this->belongsTo('App\Models\InventoryCategory');
    }

    public function inventoryManagement()
    {
        return $this->hasMany('App\Models\School\SchoolInventory');
    }
}
