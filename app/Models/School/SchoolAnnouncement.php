<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolAnnouncement extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  SchoolAnnouncement model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the school_announcements table.
    |  Also takes care of relations between school, announcement.
    |
    */
    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'school_id', 'announcement_id','date'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
