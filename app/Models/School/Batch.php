<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batch extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   Batch model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the batches table.
    |   Also takes care of relations between Course, students, coordinator, calendars.
    |
    */
    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = [
        'cluster', 'batch', 'coordinator_id', 'school_course_id', 'modules_per_day', 'completed', 'active_module_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between Batch and SchoolCourse
     * One batch will atleast belongs under one schoolCourse
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function course()
    {
        return $this->belongsTo('App\Models\School\SchoolCourse', 'school_course_id');
    }

    /**
     * One to Many relationship between Batch and Coordinator(User)
     * Every batch will atleast have one coordinator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function coordinator()
    {
        return $this->belongsTo('App\Models\User');
    }

     //get coordinator
     public function coordinatorOnlyIdName()
     {        
         return $this->belongsTo('App\Models\User')->select(array('id', 'first_name'));
     }

    /**
     * One to Many relationship between Batch and Student
     * Batch will have many Students
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
    */

    public function students()
    {
        return $this->hasMany('App\Models\School\Student');
    }

    /**
     * One to Many relationship between Batch and Calendar
     * Each Batches will have calendars planned.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
    */

    public function calendars()
    {
        return $this->hasMany('App\Models\School\Calendar');
    }  

    /**
    * One to Many relationship between Batch and Attendence
    * Each Batches will have attendance.
    *
    * @return \Illuminate\Database\Eloquent\Relations\hasMany
    *
    */

    public function attendances()
    {
        return $this->hasMany('App\Models\School\Attendance');
    }

    /**
     * One to Many relationship between Batch and Course progress
     * Each Batches will have calendars planned.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
     */
    public function courseProgress()
    {
        return $this->hasMany('App\Models\School\CourseProgress');
    }

    // //get coordinator
    // public function coordinatorOnlyIdName()
    // {        
    //     return $this->belongsTo('App\Models\User')->select(array('id', 'first_name'));
    // }
}
