<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolInventory extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  SchoolInventory model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the school_inventories table.
    |  Also takes care of relations between school, inventories.
    |
    */
    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
       'school_id', 'inventory_id', 'entered_date', 'working', 'not_working', 'lost', 'updated_by'
   ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function inventory()
    {
        return $this->belongsTo('App\Models\School\Inventory','inventory_id' );
    }
}
