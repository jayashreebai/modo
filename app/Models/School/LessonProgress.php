<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonProgress extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   Lesson Progress model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the lesson_progress table.
    |   Also takes care of relations between lesson, school courses.
    |
    */

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'school_course_id', 'batch_id', 'entered_date', 'updated_by', 'course_content_id', 'module_id', 'lesson_hash_id'
    ];

    /**
     * One to Many relationship between course progress and Batch
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */
    public function batch()
    {
        return $this->belongsTo('App\Models\School\Batch', 'batch_id');
    }

    /**
     * One to Many relationship between User and course batch
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function completedBy()
    {
        return $this->belongsTo('App\Models\User', 'completed_by');
    }

    /**
     * One to Many relationship between User and course batch
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function courseContent()
    {
        return $this->belongsTo('App\Models\School\LessonProgress', 'course_content_id');
    }
}
