<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   Gallery model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the galleries table.
    |   Also takes care of relations between Gallery and school.
    |
    */

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     */
    protected $fillable = [
        'description', 'url', 'school_id', 'added_by', 'updated_by'
    ];

    /**
        * The attributes that should be mutated to dates.
        *
        * @var array
        */
    protected $dates = ['deleted_at'];

    /***********relationship *********************/

    /**
     * One to Many relationship between Gallery and School
     * Each Gallery belongs to one School
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */
    public function school()
    {
        return $this->belongsTo('App\Models\School\School');
    }
}
