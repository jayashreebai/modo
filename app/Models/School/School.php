<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   School model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the schools table.
    |   Also takes care of relations between Course, Coordinator, EngagementType.
    |
    */
    use SoftDeletes, Sluggable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'school', 'contact_number', 'logo', 'address', 'website_url', 'school_admin_id', 'engagement_type_id'
    ];

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'school',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between School and Contacts
     * School can have more than one contacts
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
     */

    public function contacts()
    {
        return $this->hasMany('App\Models\School\Contact');
    }

    /**
     * One to Many relationship between School and Course
     * School can have more than one course
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasMany
     */

    public function courses()
    {
        return $this->hasMany('App\Models\School\SchoolCourse');
    }

    /**
     * One to Many relationship between School and EngagementType
     * School will belongs to atleast one engagement_types
     *
     *  @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function engagementType()
    {
        return $this->belongsTo('App\Models\EngagementType');
    }

    /**
     * Many to Many relationship between School and Coordinators(user)
     * School will have many coordinators
     *
     *  @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function coordinators()
    {
        return $this->belongsToMany('App\Models\User', 'school_coordinators', 'school_id', 'coordinator_id');
    }

    /**
     * Many to Many relationship between School and Announcement
     * School will have many announcements
     *
     *  @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

    public function announcements()
    {
        return $this->belongsToMany('App\Models\Announcement', 'school_announcements', 'school_id', 'announcement_id')->withPivot('date');
    }
    /**
     * Many to Many relationship between School and Coordinators(user)
     * School will have many coordinators
     */

    public function schoolAdmin()
    {
        return $this->belongsTo('App\Models\User', 'school_admin_id');
    }

    /**
    * One To Many relationship between Inventory and School
    * School has many inventories
    *
    * @return \Illuminate\Database\Eloquent\Relations\hasMany
    */

    public function inventories()
    {
        return $this->hasMany('App\Models\School\Inventory','school_id');
    }

    /**
     * One To Many relationship between Gallery and School
     * School has many galleries
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */

    public function galleries()
    {
        return $this->hasMany('App\Models\School\Gallery', 'school_id'); 
    }
}
