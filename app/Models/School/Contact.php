<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Contact model for school_contacts table
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the school_contacts table.
    |  Also takes care of relations between Contact and School.
    |
    |
    */
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'school_contacts';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = [
        'name', 'phone', 'designation', 'school_id', 'email'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between Contact and School
     * Each Contact belongs to atleast one School
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function school()
    {
        return $this->belongsTo('App\Models\School\School');
    }
}
