<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   Calendar model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the calendars table.
    |   Also takes care of relations between Course, coordinator.
    */

    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = [
        'coordinator_id', 'batch_id', 'scheduled_date', 'scheduled_time', 'created_by', 'updated_by'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */

    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between Calendar and Batch
     * Calendar will belongs to atleast batch
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function batch()
    {
        return $this->belongsTo('App\Models\School\Batch');
    }

    /**
     * One to Many relationship between Calendar and Coordinator
     * calendar belongs to each coordinator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function coordinator()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * One to Many relationship between User and Calendar
     * calendar will be created by one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function createdBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by');
    }

    /**
     * One to Many relationship between User and Calendar
     * calendar will be updated by one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\User', 'updated_by');
    }
}
