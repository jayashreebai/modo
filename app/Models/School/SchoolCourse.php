<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SchoolCourse extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  SchoolCourse model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the school_courses table.
    |  Also takes care of relations between CourseContent, Batch.
    |
    */
    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'school_id', 'course_id', 'expiry_date'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between SchoolCourse and Content
     * Each SchoolCourse will have many CourseContents
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */

    public function contents()
    {
        return $this->hasMany('App\Models\School\CourseContent');
    }

    /**
     * One to Many relationship between SchoolCourse and Batch
     * Each SchoolCourse will have many batches
     *
     *  @return \Illuminate\Database\Eloquent\Relations\hasMany
     */

    public function batches()
    {
        return $this->hasMany('App\Models\School\Batch', 'school_course_id');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course\Course', 'course_id');
    }

    public function school()
    {
        return $this->belongsTo('App\Models\School\School');
    }
}
