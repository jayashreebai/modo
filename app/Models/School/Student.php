<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Student model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the students table.
    |  Also takes care of relations between Batch and Student.
    |
    */
    use SoftDeletes, Sluggable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name', 'batch_id', 'gender', 'dob'
    ];

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between Student and Batch
     * Student belongs to atleast one Batch
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function batch()
    {
        return $this->belongsTo('App\Models\School\Batch');
    }

    /**
    * One To Many relationship between Student and Attendance
    * student can have many attendances
    *
    *  @return \Illuminate\Database\Eloquent\Relations\hasMany
    *
    */

    public function attendances()
    {
        return $this->hasMany('App\Models\School\Attendance', 'student_id');
    }
}
