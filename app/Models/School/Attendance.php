<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Attendance model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the attendance table.
    |  Also takes care of relations between attendance of each student and batch.
    |
    */
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entered_date', 'student_id', 'batch_id', 'created_by', 'present'
    ];

    /**
        * The attributes that should be mutated to dates.
        *
        * @var array
        */
    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between Attendance and Coordinator
     * attemdence belongs to each coordinator
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function coordinator()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
    * One to Many relationship between Attendance and Student
    * attendance belongs to each student
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    *
    */

    public function student()
    {
        return $this->belongsTo('App\Models\School\Student');
    }

    /**
     * One to Many relationship between Attendance and Batch
     * attendance belongs to each batch
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function batch()
    {
        return $this->belongsTo('App\Models\School\Batch');
    }
}
