<?php

namespace App\Models\School;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseContent extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  CourseContent model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the course_contents table.
    |  Also takes care of relations between CourseContent, school.
    |
    */
    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'school_course_id', 'module_id', 'module_content', 'order', 'active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between SchoolCourse and CourseContent
     * Course Content belongs to every Schoolcourse
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     */

    public function school()
    {
        return $this->belongsTo('App\Models\School\CourseContent');
    }

    public function module()
    {
        return $this->belongsTo('App\Models\Course\Module', 'module_id');
    }
}
