<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class InventoryCategory extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   InventoryCategory model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the inventory_categories table.
    |   Also takes care of relations between category and inventory.
    |
    */

    use SoftDeletes, Sluggable;
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'item_name'
    ];

    /**
     * hidden fields
     */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'item_name',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /***********relationships ************/

    /**
    * One To Many relationship between InventoryCategory and Inventory
    * Inventorycategory has many inventories
    *
    * @return \Illuminate\Database\Eloquent\Relations\hasMany
    */

    public function inventories()
    {
        return $this->hasMany('App\Models\School\Inventory','category_id');
    }
}
