<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Module model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the modules table.
    |  Also takes care of relations between Module and Course.
    |
    */
    use SoftDeletes, Sluggable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = [
        'module', 'order', 'lessons', 'course_id', 'active', 'slug'
    ];

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'module',
            ],
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * One To Many relationship between Course and Module
     * Module will atleast belong under one course
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function course()
    {
        return $this->belongsTo('App\Models\Course\Course');
    }

    public function courseContent()
    {
        return $this->hasMany('App\Models\School\CourseContent', 'module_id');
    }
}
