<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Level extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   Level model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the course_levels table.
    |   Also takes care of relations between level and Course
    |
    |
    */

    use SoftDeletes, Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'course_levels';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'level'
    ];

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'level',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between Level and Course
     * Level has many courses
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
    */

    public function courses()
    {
        return $this->hasMany('App\Models\Course\Course');
    }
}
