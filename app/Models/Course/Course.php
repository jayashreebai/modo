<?php

namespace App\Models\Course;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    /*
    |--------------------------------------------------------------------------
    | Course model
    |--------------------------------------------------------------------------
    | Through this file services interacts with the course table.
    | Also takes care of relations between module, level, school.
    |
    */

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    use SoftDeletes, Sluggable;
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'course', 'description', 'video_url', 'course_duration', 'level_id', 'slug'
    ];


    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'course',
            ],
        ];
    }
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * One to Many relationship between Course and Module
     * Course will have many modules
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */

    public function modules()
    {
        return $this->hasMany('App\Models\Course\Module')->get();
    }

    public function module()
    {
        return $this->hasMany('App\Models\Course\Module');
    }

    /**
     * One to Many relationship between Course and Level
     * Course will belong to atleast one level
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */
    public function level()
    {
        return $this->belongsTo('App\Models\Course\Level');
    }

    /**
     * One to Many relationship between Course and School
     * One course belongs to many school
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
    */

    public function schools()
    {
        return $this->belongsToMany('App\Models\School\School');
    }
}
