<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Role model
    |--------------------------------------------------------------------------
    |  Through this file services interacts with the roles table.
    |  Also takes care of relations between User and Role.
    |
    */

    use SoftDeletes, Sluggable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable=[
        'role'
    ];

    /**
    * Return the sluggable configuration array for this model.
    *
    * @return array
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'role',
            ],
        ];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * One To Many relationship between User and Role
     * Role as many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
