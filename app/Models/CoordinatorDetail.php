<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoordinatorDetail extends Model
{
    /*
    |--------------------------------------------------------------------------
    |   CoordinatorDetail model
    |--------------------------------------------------------------------------
    |   Through this file services interacts with the coordinator_details table.
    |   Also takes care of relations between User and CoordinatorDetail.
    |
    */

    protected $table = 'coordinator_details';

    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = [
        'coordinator_id', 'qualification', 'occupation', 'address'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */

    protected $dates = ['deleted_at'];

    /**
     * One to one relationship between User and CoordinatorDetail
     * One User will have one coordinator details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
    */

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'coordinator_id');
    }
}
