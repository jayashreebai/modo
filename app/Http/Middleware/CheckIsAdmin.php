<?php

namespace App\Http\Middleware;

use Closure;

class CheckIsAdmin
{
    /*
    |--------------------------------------------------------------------------
    | Check User Is Admin Middleware
    |--------------------------------------------------------------------------
    |
    | This middleware checks whether user is Modo admin or not.
    |
     */
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_null($request->user()) && $request->user()->role_id != 1) {
            return response()->json(['error' => 'Only admin have access to this']);
        }
        return $next($request);
    }
}
