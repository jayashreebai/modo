<?php

namespace App\Http\Requests\Coordinator;

use Illuminate\Foundation\Http\FormRequest;

class LessonCompleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'course_content_id' => 'required|exists:course_contents,id',
            'school_course_id' => 'required|exists:school_courses,id',
            'batch_id' => 'nullable',
            'module_id' => 'required|exists:modules,id',
        ];
    }
}
