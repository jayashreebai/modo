<?php

namespace App\Http\Requests\Coordinator;

use Illuminate\Foundation\Http\FormRequest;

class AddCoordinatorRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding coordinator.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:50',
            'last_name' => 'string|max:20',
            'email' => 'required|email',
            'phone' => 'required|digits:10|regex:/^[6-9]\d{9}$/',
            'qualification' => 'required|string|max:50',
            'occupation' => 'required|string|max:50',
            'address' => 'required|string|max:50',
        ];
    }
}
