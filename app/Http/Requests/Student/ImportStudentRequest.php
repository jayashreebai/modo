<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class ImportStudentRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for import students.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:csv,txt',
        ];
    }

    /**
     *
     * Customized messages for the validations
     *
     * @return array
     */

    public function messages()
    {
        return [
            'file.required' => 'File is required.',
            'file.mimes' => 'File type should be (.csv) only.',

        ];
    }
}
