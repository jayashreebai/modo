<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class SchoolEngagementTypeRequest extends FormRequest
{
    /**
     * This request file is to update a engagement-type for the school
     * 
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'engagement_type_id' => 'required|exists:engagement_types,id'
        ];
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'engagement_type_id.exists' => 'engagement_type_id does not exists.',
            'engagement_type_id.required' => 'Please enter engagement type!'
        ];
    }
}
