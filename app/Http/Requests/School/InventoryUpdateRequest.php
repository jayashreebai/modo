<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class InventoryUpdateRequest extends FormRequest
{
    /**
     * Form request validation for updating iventories.
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inventory_id' => 'required|exists:inventories,id',
            'entered_date' => 'required|date_format:Y-m-d',
            'working' => 'required|integer',
            'not_working' => 'required|integer',
            'lost' => 'required|integer'
        ];
    }
    /**
    *
    * Customized messages for the validations
    *
    * @return array
    */

    public function messages()
    {
        return [
            'inventory_id.required' => 'Please Enter inventory id',
            'inventory_id.integer' => 'Inventory id should be integer',
        ];
    }
}
