<?php

namespace App\Http\Requests\School;

use App\Models\Course\Course;
use App\Models\School\SchoolCourse;
use Illuminate\Foundation\Http\FormRequest;

class AddBatchRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding batches.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $school_course_id = $this->request->get('course_id');
        $course_id = SchoolCourse::whereId($school_course_id)->value('course_id');
        $modules = Course::whereId($course_id)->value('course_duration');
        return [
            'cluster_name' => 'required|string',
            'batch_name' => 'required|string',
            'coordinator_id' => 'required|integer|exists:users,id',
            'course_id' => 'required|integer|exists:school_courses,id',
            'modules_per_day' => 'required|integer|gt:0|lte:' .$modules
        ];
    }
    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'cluster_name.required' => 'Please enter cluster name',
            'cluster_name.string' => 'Cluster name should be string',

            'batch_name.required' => 'Please enter batch name',
            'batch_name.string' => 'Batch name should be string',
        ];
    }
}
