<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class AddSchoolAdminRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding school-admin.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:50',
            'last_name' => 'string|max:20',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|digits:10|regex:/^[6-9]\d{9}$/|unique:users,phone',
            'school_id' => 'required|exists:schools,id'
        ];
    }

    /**
     *
     * Customized messages for the validations
     *
     * @return array
     */

    public function messages()
    {
        return [
            'first_name.required' => 'Please Enter first name',
            'first_name.string' => 'First name should be string',

            'email.required' => 'Please Enter email address',
        ];
    }
}
