<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCoordinatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:50',
            'last_name' => 'max:20',
            'email' => 'required|email|unique:users,email,' .$this->user->id,
            'phone' => 'required|digits:10|regex:/^[6-9]\d{9}$/|unique:users,phone,' .$this->user->id,
            'qualification' => 'required|string|max:50',
            'occupation' => 'required|string|max:50',
            'address' => 'required|string|max:50',
        ];
    }
}
