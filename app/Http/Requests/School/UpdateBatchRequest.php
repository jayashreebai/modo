<?php

namespace App\Http\Requests\School;

use App\Models\Course\Course;
use App\Models\School\SchoolCourse;
use Illuminate\Foundation\Http\FormRequest;
 
class UpdateBatchRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for updating batches.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $schoolCourseId = $this->request->get('course_id');
//        $course_id = SchoolCourse::whereId($schoolCourseId)->value('course_id');
        $modules = Course::whereId($schoolCourseId)->value('course_duration');
        
        return [
            'coordinator_id' => 'required|integer|exists:users,id',
            'school_course_id' => 'required|integer|exists:school_courses,id',
            'modules_per_day' => 'required|integer|lte:' . $modules
        ];
    }
}
