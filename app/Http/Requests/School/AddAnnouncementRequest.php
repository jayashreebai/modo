<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class AddAnnouncementRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding announcements.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'date' => 'required|date_format:Y-m-d',
//            'file' => 'required',
        ];
    }

    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'file.required' => 'Please select file',

            'title.required' => 'Please enter title',
            'title.string' => 'Title should be string',

            'description.required' => 'Please enter description',
            'description.string' => 'Description should be string',
        ];
    }
}
