<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class AddStudentRequest extends FormRequest
{

    /**
     * This request file is for validating
     * post request for adding students.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'dob' => 'required|date_format:Y-m-d',
            'gender' => 'required',
        ];
    }

    /**
     *
     * Customized messages for the validations
     *
     * @return array
     */

    public function messages()
    {
        return [
            'name.required' => 'Please Enter student name',
            'name.string' => 'Student name should be string',
        ];
    }
}
