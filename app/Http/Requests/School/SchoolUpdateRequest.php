<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class SchoolUpdateRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for updating schools.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [
        //     'field_name' => 'required|string|unique:schools,school',
        //     'value' => 'required_if:field_name,engagement_type_id,website_url,school,address,school_admin_id,active,contact_number',
        // ];

        return [
            'name' => 'required|string|regex:^[A-Za-z0-9- ]+$^',
            'address' => 'required|string',
            'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'contact' => 'required|digits:10|regex:/^[6-9]\d{9}$/|unique:schools,contact_number,' . $this->school->id,
            'website' => 'url',
        ];
    }

    /**
    *
    * Customized messages for the validations
    *
    * @return array
    */

    public function messages()
    {
        return [
            'name.required' => 'Please Enter school name',
            'name.string' => 'School name should be string',
            'name.unique' => 'School name should be unique',
            'name.regex' => 'Special characters are not allowed at end of the name.',

            'address.required' => 'Please enter school address',
            'address.string' => 'School address should be string',

            'website.url' => 'Please enter valid website',
            
            'contact.regex' => 'Please enter valid contact number'
        ];
    }
}
