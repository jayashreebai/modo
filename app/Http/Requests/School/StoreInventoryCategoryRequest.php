<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class StoreInventoryCategoryRequest extends FormRequest
{

    /**
     *  Form request file to validate for the given fields.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name' => 'required|string'
        ];
    }
    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'item_name.required' => 'Please enter item name',
            'item_name.string' => 'Item name should be string',
        ];
    }
}
