<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInventoryManagement extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entered_date' => 'required|date_format:Y-m-d',
            'inventory_id' => 'required',
            'working' => 'required_without_all:not_working,lost',
            'not_working' => 'required_without_all:lost,working',
            'lost' => 'required_without_all:not_working,working',

        ];
    }
}
