<?php

namespace App\Http\Requests\School;

use App\Models\School\School;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSchoolAdminRequest extends FormRequest
{ 
    /**
     * This request file is for validating
     * post request for updating school admin.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $school = \Route::current()->parameter('school');
        $user = School::whereId($school->id)->value('school_admin_id');
        
        return [
            'first_name' => 'required|string|max:50',
            'last_name' => 'max:20',
            'email' => 'required|email|unique:users,email,'. $user,
            'phone' => 'required|digits:10|regex:/^[6-9]\d{9}$/|unique:users,phone,'. $user,
        ];
    }

    /**
     *
     * Customized messages for the validations
     *
     * @return array
     */

    public function messages()
    {
        return [
            'first_name.required' => 'Please Enter First name',
            'first_name.string' => 'First name should be string',

            'email.required' => 'Please Enter email address',
        ];
    }
}
