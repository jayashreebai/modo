<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class AssignInventoryRequest extends FormRequest
{
    /**
     * Form request file for validating assign inventories.
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|exists:inventory_categories,id',
            'serial_number' => 'nullable|string',
            'item_name' => 'required',
            'quantity' => 'required|integer'
        ];
    }
    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'category_id.required' => 'Please enter inventory category',
            'category_id.integer' => 'Inventory category should be integer',

            'serial_number.required' => 'Please enter serial number',
            'serial_number.string' => 'Serial number should be string',

            'quantity.required' => 'Please enter quantity',
            'quantity.integer' => 'Quantity should be integer',
        ];
    }
}
