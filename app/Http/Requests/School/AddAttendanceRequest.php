<?php

namespace App\Http\Requests\School;

use Validator;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AddAttendanceRequest extends FormRequest
{
    /**
      * This request file is for validating
      * post request for adding attendances.
      *
      */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /*
    |--------------------------------------------------------------------------
    | Add Attendance Request
    |--------------------------------------------------------------------------
    |
    | This FormRequest handles Add Attendance related validations
    | for the application.
    |
        */

    /**
     * Validation fo Add Attendance
     *
     * @param $request
     * @return mixed
     */
    public function attendanceValidation($request)
    {
        $request->request->add(['absentees' => json_decode($request->absentees, true)]);

        // Validation messages for input fields
        $message = [
            'date.required' => 'Please enter date to take attendance',
            'coordinator_id.required' => 'Please enter coordinator-id to take attendance',
            // 'absentees.student_id.integer' => 'Student id should be integer',
        ];

        // Validation rules for input fields
        $validator = Validator::make($request->all(), [
                'date' => 'required|date:Y/m/d',
                // 'absentees.student_id' => 'required|integer|exists:students,id'
            ], $message);

        // If validation fails then return errors with the customized messages
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
    }

    /**
    * Validation fo Update Attendance
    *
    * @param $request
    * @return mixed
    */
    public function updateAttendanceValidation($request)
    {
        // Validation messages for input fields
        $message = [
            'present.required' => 'Please enter data to update attendance',
        ];

        // Validation rules for input fields
        $validator = Validator::make($request->all(), [
                'present' => 'required'
            ], $message);

        // If validation fails then return errors with the customized messages
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
    }
}
