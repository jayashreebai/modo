<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class UnassignCourseRequest extends FormRequest
{
    /**
     * Form request file for validating post data for course_id details
     * for unassigning course.
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_id' => 'required|exists:courses,id'
        ];
    }
}
