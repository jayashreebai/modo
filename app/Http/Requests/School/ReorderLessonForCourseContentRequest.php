<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class ReorderLessonForCourseContentRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for reorder lessons.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lesson_hash_id' => 'required|string',
            'order' => 'required|integer',
        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'order.integer' => 'Order should be of integer.',
            'order.required' => 'Order should be required.',
            'lesson_hash_id.required' => 'Lesson id should be required.'
        ];
    }
}
