<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContactDetailRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for updating contact-detail.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        $contact = \Route::current()->parameter('contact');
        // $user = Contact::whereId($contact)->value('school_admin_id');

        return [
            'name' => 'required|string',
            'designation' => 'required|string',
            'phone' => 'required|digits:10|regex:/^[6-9]\d{9}$/|unique:school_contacts,phone,'.$contact,
            'school_id' => 'required',
            'email' => 'email'
        ];
    }

    /**
     *
     * Customized messages for the validations
     *
     * @return array
     */

    public function messages()
    {
        return [
            'name.required' => 'Please Enter name.',
            'name.string' => 'Name should be string.',
            'designation.required' => 'Please Enter designation.',
            'phone.required' => 'Please Enter phone.',
            'phone.regex' => 'Phone number should be start with 6, 7, 8 or 9.',
        ];
    }
}
