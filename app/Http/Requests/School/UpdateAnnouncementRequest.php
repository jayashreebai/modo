<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAnnouncementRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for updating announcement.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'date' => 'required|date_format:Y-m-d',
//            'file' => 'required',
        ];
    }
    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'field_name.required' => 'Please enter field name',
            'value.required' => 'Please enter value',
        ];
    }
}
