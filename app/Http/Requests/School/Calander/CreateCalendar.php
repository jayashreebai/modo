<?php

namespace App\Http\Requests\School\Calander;

use Illuminate\Foundation\Http\FormRequest;

class CreateCalendar extends FormRequest
{ 
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'coordinator_id' => 'required|integer|exists:users,id',
            'date' => 'required|after:yesterday',
            'time' => 'required|json',

        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'date.required' => 'Please enter Schedule date.',
            'date.date_format' => 'Date format should be YYYY-MM-DD.',
            'date.after' => 'Date should not be previous date.',
            'time.required' => 'Please enter schedule time.',
        ];
    }
}
