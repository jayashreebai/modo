<?php

namespace App\Http\Requests\School\Calander;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCalendar extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|json'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'date.required' => 'Please enter Schedule date.',
            'date.date_format' => 'Date format should be YYYY-MM-DD',
            'time.required' => 'Please enter schedule time',
        ];
    }
}
