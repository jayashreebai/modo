<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class DeleteCoordinatorRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for deleting courses.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coordinator_id' => 'required|exists:users,id|integer',
            'school_id' => 'required|exists:schools,id|integer'
        ];
    }

    /**
    *
    * Customized messages for the validations
    *
    * @return array
    */

    public function messages()
    {
        return [
            'coordinator_id.required' => 'Please Enter coordinator id',
            'coordinator_id.integer' => 'Coordinator id should be integer',

            'school_id.required' => 'Please Enter School id',
            'school_id.integer' => 'School id should be integer',
        ];
    }
}
