<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class ReorderModuleForCourseContentRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for reorder modules.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order' => 'required|integer',
        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'order.integer' => 'Order should be of integer.',
            'order.required' => 'Order should be required.',
        ];
    }
}
