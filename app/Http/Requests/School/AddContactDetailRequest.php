<?php

namespace App\Http\Requests\School;

use Illuminate\Foundation\Http\FormRequest;

class AddContactDetailRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding contact details.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'designation' => 'required|string',
            'phone' => 'required|digits:10|regex:/^[6-9]\d{9}$/|unique:school_contacts,phone',
            'school_id' => 'required|exists:schools,id',
            'email' => 'email'
        ];
    }
    /**
     *
     * Customized messages for the validations
     *
     * @return array
     */

    public function messages()
    {
        return [
            'name.required' => 'Please enter the name.',
            'name.string' => 'Name should be string.',
            'designation.required' => 'Please enter the designation.',
            'phone.required' => 'Please enter the phone.',
            'school_id.required' => 'School id is required.',
            'school_id.exists' => 'School id does not exists.',
            'phone.regex' => 'Phone number should be start with 6, 7, 8 or 9.',

        ];
    }
}
