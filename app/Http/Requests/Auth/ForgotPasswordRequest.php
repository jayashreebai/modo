<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /*
    |--------------------------------------------------------------------------
    | Forgot Password Request
    |--------------------------------------------------------------------------
    |
    | This FormRequest handles forgot password related validations
    | for the application.
    |
     */

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|max:50|regex:/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,3})$/',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Please enter your email id',
            'email.regex' => 'Should accepts only abc@efg.klm format',
            'email.max' => 'Not a valid email id',
        ];
    }
}
