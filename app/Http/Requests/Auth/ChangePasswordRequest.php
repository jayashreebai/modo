<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /*
    |--------------------------------------------------------------------------
    | Change Password Request
    |--------------------------------------------------------------------------
    |
    | This FormRequest handles change password related validations
    | for the application.
    |
     */

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required|min:6|max:8',
            'password' => 'required|confirmed|min:6|max:8',
            'password_confirmation' => 'same:password|required|min:6|max:8',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'current_password.required' => 'Please enter old password',
            'current_password.min' => 'Password must be 6 character long',
            'current_password.max' => 'Password should allow only 8 characters',

            'password.required' => 'Please enter your password',
            'password.confirmed' => "Please enter correct password",
            'password.min' => 'Password must be 6 character long',
            'password.max' => 'Password should allow only 8 characters',

            'password_confirmation.required' => 'Please confirm your password',
            'password_confirmation.same' => "Password didn't match with confirm password",
            'password_confirmation.min' => 'Password must be 6 character long',
            'password_confirmation.max' => 'Password should allow only 8 characters',
        ];
    }
}
