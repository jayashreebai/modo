<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /*
    |--------------------------------------------------------------------------
    | Login Request
    |--------------------------------------------------------------------------
    |
    | This FormRequest handles login related validations
    | for the application.
    |
     */

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|max:50|exists:users,email',
            'password' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Please enter your email id',
            'email.max' => 'Not a valid email id',
            'email.exists' => "Email id doesn't exist",
            'password.required' => 'Please enter your password',
        ];
    }
}
