<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /*
    |--------------------------------------------------------------------------
    | Reset Password Request
    |--------------------------------------------------------------------------
    |
    | This FormRequest handles reset password related validations
    | for the application.
    |
     */

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'password' => 'required|confirmed|between:6,8',
            'password_confirmation' => 'same:password|required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => ':attribute is required',
            'between' => 'Password must contain minimum of 6 and maximum 8 characters'
        ];
    }
}
