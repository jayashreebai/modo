<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class ModuleStoreRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding modules.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'course_id' => 'required|exists:courses,id',
        ];
    }

    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'name.required' => 'Please enter module name',
            'name.string' => 'Module name should be of string',
            'name.regex' => 'Name title should be of alphanumric only',

            'course_id.required' => 'Please enter course for which module belongs',
        ];
    }
}
