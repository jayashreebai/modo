<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class ReorderLessonRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for reorder lessons.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module_id' => 'required|integer|exists:modules,id',
            'lesson_hash_id' => 'required|string',
            'order' => 'required|integer',
        ];
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'module_id.required' => 'Module id is required.',
            'module_id.integer' => 'Module id should be integer.',
            'module_id.exists' => 'Module id does not exists.',
            'order.integer' => 'Order should be of integer.',
            'order.required' => 'Order should be required.',
            'lesson_hash_id.required' => 'Lesson id should be required.'
        ];
    }
}
