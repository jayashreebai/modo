<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class LessonUpdateRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for updating lessons.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module_id' => 'required|integer|exists:modules,id',
            'lesson_hash_id' => 'required|string',
            'field_name' => 'required|string',
            'value' => 'required|string',
        ];
    }
    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'module_id.required' => 'Module id is required.',
            'module_id.integer' => 'Module id should be integer.',
            'module_id.exists' => 'Module id does not exists.',

            'lesson_hash_id.required' => 'Lesson hash id is required',
            'lesson_hash_id.string' => 'Lesson hash id is string',

            'field_name.string' => 'Field name should be of string.',
            'field_name.required' => 'Field name should be required.',

            'value.string' => 'value should be of string.',
            'value.required' => 'value should be required.',

        ];
    }
}
