<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class CourseStoreRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding courses.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
    */
    public function authorize()
    {
        return true; 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|regex:/^[a-z\d\-_\s]+$/i',
            'description' => 'required',
            'video' => 'required|url',
            'course_duration' => 'required|integer|gt:0',
            'level' => 'required|exists:course_levels,id',
        ];
    }

    /**
    * Custom message for validation
    *
    * @return array
    */
    public function messages()
    {
        return [
            'title.required' => 'Please enter Course title',
            'title.string' => 'Course title should be of string',
            'title.regex' => 'Course title should be of alphanumric only',

            'description.required' => 'Please enter course description',

            'video.required' => 'Please enter video url',
            'video.url' => 'Please enter valid video url',

            'course_duration.required' => 'Please enter course duration or number of modules',
            'course_duration.integer' => 'Please enter valid number of modules',

            'level.required' => 'Please enter level for which course belongs',
        ];
    }
}
