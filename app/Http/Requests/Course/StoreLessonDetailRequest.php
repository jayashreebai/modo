<?php

namespace App\Http\Requests\Course;

use Illuminate\Foundation\Http\FormRequest;

class StoreLessonDetailRequest extends FormRequest
{
    /**
     * This request file is for validating
     * post request for adding lesson-details.
     *
     */
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module_id' => 'required|integer|exists:modules,id',
            'lesson_hash_id' => 'required|string',
            'lesson_detail' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'module_id.required' => 'Module id is required.',
            'lesson_detail.required' => 'Lesson detail is required.',
            'module_id.integer' => 'Module id should be integer.',
            'module_id.exists' => 'Module id does not exists.',
            'lesson_hash_id.string' => 'Lesson id should be of string.',
            'lesson_hash_id.required' => 'Lesson id should be required.'
        ];
    }
}
