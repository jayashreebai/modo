<?php

namespace App\Http\Requests\SchoolManagement;

use Illuminate\Foundation\Http\FormRequest;

class QuizDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'course_content_id' => 'required|exists:course_contents,id',
            'lesson_id' => 'required',
            'quiz' => 'required|json'
        ];
    }
}
