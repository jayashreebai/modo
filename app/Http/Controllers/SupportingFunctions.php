<?php

namespace App\Http\Controllers;

use Image;
use Carbon\Carbon;
use App\DataSerializer;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Storage;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class SupportingFunctions extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Supporting Functions
    |--------------------------------------------------------------------------
    |
    | This controller handles all the frequently used services
    | for the application.
    |
     */

    /**
     * Save token to password_reset table.
     *
     * @param $email
     * @return string
     */
    public static function saveToken($email)
    {
        // Generate random 25 character along with current time attached with it
        $token = str_random(25) . Carbon::now()->timestamp;

        // Storing the random string which will act like password reset token
        PasswordReset::create(['email' => $email, 'token' => $token]);

        return $token;
    }

    /**
     * Check if token exists in password_reset table
     *
     * @param $token
     * @return mixed
     */
    public static function getUserIfTokenExists($token)
    {
        return PasswordReset::where('token', $token)->first();
    }

    /**
     * Expire Password Reset token after 24 hours
     *
     * @param $token
     * @return mixed
     */
    public static function expireToken($token)
    {
        $tokenData = PasswordReset::where('token', $token)->first();
        if ($tokenData) {
            $timeDifference = Carbon::now()->diffInHours($tokenData->created_at);
            if ($timeDifference > 24) {
                return PasswordReset::where('token', $token)->delete();
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Delete verification token from password_reset table
     *
     * @param $token
     */
    public static function deleteVerificationToken($token)
    {
        PasswordReset::where('token', $token)->delete();
    }

    /**
     * Active and Deactive Function
     *
     * @param $model, $id, $flag
     */
    public static function deactivateModel($model, $id, $flag)
    {
        return $model::where('id', $id)->update(['active' => $flag]);
    }

    /**
     * Add image
     *
     * @param $request
     * @return string
    */
    public static function addImage($request, $folder, $filename)
    {
        // Create Image instance
        $resizeImage = Image::make($request->file($filename)->getRealPath());

        // Resize the image to width of 300 and constraint aspect ratio (auto height)
        $resizeImage->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');

        // Generates a unique alphanumeric string
        $imageName = (string)md5(time() . rand());

        // Storing the image in storage/app/public/user-profile
        Storage::disk('public')
            ->put($folder . $imageName . '.jpg', $resizeImage, 'public');

        // Saving the path in our DB
        return $folder . $imageName . '.jpg';
    }


    public static function addGalleryImage($files, $folder, $filename)
    {
        // Create Image instance
        $resizeImage = Image::make($files->file($filename)->getRealPath());

        // Resize the image to width of 300 and constraint aspect ratio (auto height)
        $resizeImage->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');

        // Generates a unique alphanumeric string
        $imageName = (string)md5(time() . rand());

        // Storing the image in storage/app/public/user-profile
        Storage::disk('public')
            ->put($folder . $imageName . '.jpg', $resizeImage, 'public');

        // Saving the path in our DB
        return $folder . $imageName . '.jpg';
    }

    /**
     * This function is used for returning success json response
     *
     * @param message
     * @return jsonResponse
    */

    public static function success($message)
    {
        return response()->json(['data' => ['message' => $message], 'status_code' => 200]);
    }

    /**
    * This function is used for returning error json response
    *
    * @param message
    * @return jsonResponse
    */

    public static function error($message)
    {
        return response()->json(['error' => ['message' => $message], 'status_code' => 500]);
    }

    /**
    * This function is used for returning general error json response
    *
    * @param message
    * @return jsonResponse
    */

    public static function errors($message, $statusCode)
    {
        return response()->json(['error' => ['message' => $message], 'status_code' => $statusCode]);
    }

    /**
    * This function is used for returning not found json response
    *
    * @param message
    * @return jsonResponse
    */

    public static function notFound($message)
    {
        return response()->json(['error' => ['message' => $message], 'status_code' => 404]);
    }

    /**
     * Transform function
     */
    public static function transform($value, $transformer)
    {
        $manager = new \League\Fractal\Manager();
        $manager->setSerializer(new DataSerializer());
        $result = new Collection($value, new $transformer);
        $data = $manager->createData($result)->toArray();
        return response()->json(['data' => $data, 'status_code' => 200]);
    }

    /**
     * Transform function
     */
    public static function transformItem($value, $transformer)
    {
        $manager = new \League\Fractal\Manager();
        $manager->setSerializer(new DataSerializer());
        $result = new Item($value, new $transformer);
        $data = $manager->createData($result)->toArray();
        return response()->json(['data' => $data, 'status_code' => 200]);
    }

    /**
     * @param $lessonOrModuleContent
     * @param $lesson_id
     * @param $order
     * @return array
     *
     * This is reusable function for reordering the lesson or Module.
     */
    public function reorderLessonOrModule($lessonOrModuleContent, $lesson_id, $order)
    {
        $lessonsOrModules = json_decode($lessonOrModuleContent, true);
        $flag = true;
        $newUpdatedLessonOrModule = [];

        //===Below foreach is used for fetching the current lesson with hashId for taking its order===
        foreach ($lessonsOrModules as $lesson) {
            if ($lesson['id'] == $lesson_id) {
                $currentLesson = $lesson;
            }
        }
        //======================================END===================================================

        if ($currentLesson['order'] > $order) {

            //====Using USORT php function for sorting the lesson based on ascending order=======
            usort($lessonsOrModules, function ($a, $b) {
                return strcmp($a['order'], $b['order']);
            });
            //=====================================END===========================================

            foreach ($lessonsOrModules as $lesson) {
                if ($flag) {
                    if (($lesson['order'] >= $order) && ($lesson['id'] != $lesson_id)) {
                        $lesson['order'] += 1;
                        array_push($newUpdatedLessonOrModule, $lesson);
                    } else {
                        if ($lesson['id'] == $lesson_id) {
                            $lesson['order'] = $order;
                            $flag = false;
                            array_push($newUpdatedLessonOrModule, $lesson);
                        } else {
                            array_push($newUpdatedLessonOrModule, $lesson);
                        }
                    }
                } else {
                    array_push($newUpdatedLessonOrModule, $lesson);
                }
            }
        } else {
            //====Using USORT php function for sorting the lesson based on desceding order=======
            usort($lessonsOrModules, function ($a, $b) {
                return strcmp($b['order'], $a['order']);
            });
            //=====================================END===========================================

            foreach ($lessonsOrModules as $lesson) {
                if ($flag) {
                    if (($lesson['order'] <= $order) && ($lesson['id'] != $lesson_id)) {
                        $lesson['order'] -= 1;
                        array_push($newUpdatedLessonOrModule, $lesson);
                    } else {
                        if ($lesson['id'] == $lesson_id) {
                            $lesson['order'] = $order;
                            $flag = false;
                            array_push($newUpdatedLessonOrModule, $lesson);
                        } else {
                            array_push($newUpdatedLessonOrModule, $lesson);
                        }
                    }
                } else {
                    array_push($newUpdatedLessonOrModule, $lesson);
                }
            }
        }
        usort($newUpdatedLessonOrModule, function ($a, $b) {
            return strcmp($a['order'], $b['order']);
        });
        return $newUpdatedLessonOrModule;
    }

    /**
     * @param $key
     * @param $value
     * @param $jsonObjects
     * @return array
     * This function is used for finding the single object from the json , in this u have to pass for
     * key u want to search and for which value, if its find the it will return the object else
     * it will return false.
     */
    public static function singleValue($key,$value,$jsonObjects)
    {
        foreach ($jsonObjects as $item)
        {
            if($item[$key] == $value){
                return $item;

            }
        }
        return false;

    }

    /**
     * @param $key
     * @param $value
     * @param $jsonObjects
     * @return array
     * This function is used for finding the multiple object from the json , in this u have to pass for
     * key u want to search and for which value, if its find the it will return the object else
     * it will return false.
     */
    public static function multivalue($key,$value,$jsonObjects)
    {
//        return $jsonObjects;
//        $response = [];
        return collect($jsonObjects)->filter(function ($item) use ($key,$value){
            return $item[$key] == $value;
        })->values();
//        foreach ($jsonObjects as $item)
//        {
//            if($item['active'] == 1){
//                array_push($item,$response);
//            }
//        }
//        return $response;

    }

}
