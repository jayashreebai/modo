<?php

namespace App\Http\Controllers\Course;

use Illuminate\Http\Request;
use App\Models\Course\Course;
use App\Models\Course\Module;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\Course\ModuleStoreRequest;
use App\Http\Requests\Course\ModuleUpdateRequest;
use App\Http\Requests\Course\ReorderModuleRequest;
use App\Http\Requests\School\UpdateActiveRequest;
use App\Transformers\CourseManagement\ModuleTransformer;

class ModuleController extends Controller
{
    /**
     * Module controller will have services related to modules.
     *
     * @return \Illuminate\Http\Response
     *
     */

    /**
     * ModuleController constructor.
    */
    public function __construct()
    {
        $this->middleware(['jwt.auth']);
    }

    /**
     * Store Module API
     * 1. Fetch course_duration to keep track of order in modules.
     * 2. Increment course_duration
     * 3. Populate modules table with given request data.
     *
     * @param  \Illuminate\Http\ModuleStoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ModuleStoreRequest $request)
    {
        $course = Course::find($request->course_id);
//        $course->increment('course_duration');

        $moduleData = Module::create([
            'module' => $request->name,
            'order' => $course->course_duration,
            'lessons' => '[]',
            'course_id' => $request->course_id
        ]);
        $moduleCount = count($course->module->where('active',1));
//      course content is not fulfill for assigning to school thats y doing course active to 2
        Course::where('id',$request->course_id)->update(['active'=>2, 'course_duration' => $moduleCount]);
        return SupportingFunctions::success($moduleData->module. " added successfully!");
    }

    /**
     * Update the module API
     *
     * @param  \Illuminate\Http\ModuleUpdateRequest $request
     * @param  string module
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ModuleUpdateRequest $request, Module $module)
    {
        $module->update(['module' => $request->get('name')]);
        return SupportingFunctions::success($request->get('name')." updated successfully!");
    }

    /**
     * Delete module API
     * 1. Update active to zero using deactivateModel static function
     * 2. Softdelete the given module
     *
     * @param  string module
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function destroy(Module $module)
    {
        SupportingFunctions::deactivateModel(Module::class, $module->id, 0);
        $module->delete();

        $moduleCount = Module::whereCourseId($module->course_id)->where('active',1)->count();
//      course content is not fulfill for assigning to school thats y doing course active to 2
        Course::where('id',$module->course_id)->update(['course_duration' => $moduleCount]);

//      --------------Checking for proper lesson content is present in each module------------
        $activeFlag = LessonController::checkingCourseHasProperLessonWithContent($module->course_id);
        if($activeFlag){
            Course::where('id', $module->course_id)->update(['active' => 1]);
        }
        else{
            Course::where('id', $module->course_id)->update(['active' => 2]);
        }
//      ------------------------------------END CHECKING--------------------------------------


        return SupportingFunctions::success($module->module." deleted successfully!");
    }

    /**
     *
     * API List of all modules for given course
     *
     * @param  string course
     * @return \Illuminate\Http\JsonResponse
    */

    public function index(Course $course)
    {  
        $courses = Course::whereId($course->id)->first(); 
        return SupportingFunctions::transformItem($courses, new ModuleTransformer);
    }

    /**
     * Reorder Module API
     * 1. Reorder of modules
     * 2. Post request
     * 3. One module_id and its order is given at a time
     * 4. Next modules should be incremented by one
     */
    public function reorder(ReorderModuleRequest $request, Course $course)
    {
        $modules = Module::where('course_id', $course->id)->orderBy('order')->get();

        $supportingFunction = new SupportingFunctions;
        $reorderedModules = $supportingFunction->reorderLessonOrModule($modules, $request->get('module_id'), $request->get('order'));

        //below line of code is use for updating the course with the given ordered value.
        foreach ($reorderedModules as $module) {
            Module::whereId($module['id'])->update(['order' => $module['order']]);
        }
        return SupportingFunctions::success("Order changed successfully!");
    }

    /**
     * updateActive
     */
    public function updateActive(UpdateActiveRequest $request)
    {
        $module = Module::find($request->module_id);
        $module->active = $request->active;
        $module->save();
        $moduleCount = Module::whereCourseId($module->course_id)->where('active',1)->count();
        Course::where('id',$module->course_id)->update(['course_duration' => $moduleCount]);
//      --------------Checking for proper lesson content is present in each module------------
        $activeFlag = LessonController::checkingCourseHasProperLessonWithContent($module->course_id);
        if($activeFlag){
            Course::where('id', $module->course_id)->update(['active' => 1]);
        }
        else{
            Course::where('id', $module->course_id)->update(['active' => 2]);
        }
//      ------------------------------------END CHECKING--------------------------------------
        return SupportingFunctions::success('Module updated successfully!');
    }
}
