<?php

namespace App\Http\Controllers\Course;

use App\DataSerializer;
use App\Models\School\Batch;
use App\Models\School\SchoolCourse;
use App\Transformers\SchoolManagement\CourseNameTransformer;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Models\Course\Course;
use App\Models\Course\level;
use App\Models\Course\Module;
use App\Http\Controllers\Controller;
use League\Fractal\Resource\Collection;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\Course\CourseStoreRequest;
use App\Http\Requests\Course\CourseUpdateRequest;
use App\Transformers\CourseManagement\CourseTransformer;
use App\Transformers\CourseManagement\CourseIdNameTransformer;
use App\Transformers\CourseManagement\LevelTransformer;

class CourseController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Course Controller(Resource Controller)
    |--------------------------------------------------------------------------
    |
    | This controller is used for Course related operation.
    | Course creation(module and lesson), course update, course read, course delete
    |
    */
    /**
     * CourseController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**testing git integration */
    public function example(){
        return 1;
    }

    /**
     * Course list API
     *
     * Fetches all active or inactive courses.
     * according to give flag
     *
     * @return \Illuminate\Http\Response
     */
    public function activeCourses($flag)
    {
        $courses = Course::whereActive($flag)->get();
        if (count($courses)) {
            return SupportingFunctions::transform($courses, new CourseTransformer);
        }
        return SupportingFunctions::success('Course doesnot exists!');
    }
    public function activeCourses2($flag)
    {
        $courses = Course::whereActive($flag)->get();
        if (count($courses)) {
            return SupportingFunctions::transform($courses, new CourseTransformer);
        }
        return SupportingFunctions::success('Course doesnot exists!');
    }

     /**
     * Course list API
     *
     * Fetches all active or inactive courses.
     * according to give flag
     *
     * @return \Illuminate\Http\Response
     */
    public function activeCourses1($flag)
    {
        $courses = Course::whereActive($flag)->get();
        if (count($courses)) {
            return SupportingFunctions::transform($courses, new CourseTransformer);
        }
        return SupportingFunctions::success('Course doesnot exists!');
    }

    /**
     * 1. Create a course with fields title, description, course_duration, video_link and level (course table)
     * 2. Populate modules table with number of course_duration
     * 3. Let module name start with Module1, Module2... and lessons be empty json.
     *
     * @param  \Illuminate\Http\CourseStoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CourseStoreRequest $request)
    {
        /**
         * 1. Populate Course.
         * 2. Populate Module
         */
        $course = Course::create([
            'course' => $request->title,
            'description' => $request->description,
            'course_duration' => $request->course_duration,
            'level_id' => $request->level,
            'video_url' => $request->video,

        ]);
        $course->active = 2;
        $course->save();

        //populate modules with number of course_duration value
        for ($i=1; $i <= $request->course_duration; $i++) {
            Module::create([
                'module' => 'Module '.$i,
                'order' => $i,
                'lessons' => '[]',
                'course_id' => $course->id
            ]);
        }
        return SupportingFunctions::success('Course successfully created!');
    }

    /**
     * Update Course values for given fieldname and its value
     *
     * @param  \Illuminate\Http\CourseUpdateRequest $request
     * @param  string $course
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CourseUpdateRequest $request, Course $course)
    {
        $totalModule = (int)$request->course_duration;
        if($course->course_duration != $totalModule){
            return response()->json(['error' => 'Sorry no. of module is not less then or greater then actual module count.'] ,433);
        }
        $course->update(['course' => $request->title,
                        'description' => $request->description,
                        'video_url' => $request->video,
                        'course_duration' => $request->course_duration,
                        'level_id' => $request->level]);

        return SupportingFunctions::success($request->title." updated successfully!");
    }

    /**
     * Course List Api 
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $courses = Course::all();

//        if ($courses->isNotEmpty()) {
           return SupportingFunctions::transform($courses, new CourseTransformer());
//        }
//        return SupportingFunctions::success("Courses not found!");
    }

    /**
     * Levels master API
     * @return \Illuminate\Http\JsonResponse
     * 
     */
    public function level() 
    {
        $level = Level::all();
        if ($level->isNotEmpty()) {
           return SupportingFunctions::transform($level, new LevelTransformer());
        } 
        return SupportingFunctions::success('Level not found!');
    }

    /**
     * Course delete API
     * 
     * @param  string $course
     * @return \Illuminate\Http\JsonResponse
     * 
     */
    public function destroy(Request $request, Course $course)
    {
        $schoolCourse = SchoolCourse::where(['course_id' => $course->id])->first();
        if (!empty($schoolCourse)) {
            if (Batch::where('school_course_id', $schoolCourse->id)->exists()) {
                return response()->json(['error' => 'Cannot dactivate the course because batch already started with this course'] ,433);

            }
        }
        $flag = $request->get('flag');
        SupportingFunctions::deactivateModel(Course::class, $course->id, $flag);

        if($flag == 1){
            $activeFlag = LessonController::checkingCourseHasProperLessonWithContent($course->id);
            if(!$activeFlag){
                Course::where('id', $course->id)->update(['active' => 2]);
            }
        }
//        $course->delete();
        $flag == 1 ? $msg = 'activated' : $msg = 'deactivated';
        return SupportingFunctions::success($course->course.' '.$msg .' successfully!');
    }

    /**
     * course-details for given course slug
     * 
     */
    public function show(Course $course)
    {
        return SupportingFunctions::transformItem($course, new CourseNameTransformer());
    } 


}
