<?php

namespace App\Http\Controllers\Course;

use App\DataSerializer;
use App\Jobs\PopulateCourseContent;
use App\Models\Course\Course;
use App\Models\School\CourseProgress;
use App\Models\School\SchoolCourse;
use Carbon\Carbon;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Models\Course\Module;
use App\Models\School\CourseContent;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\Course\LessonStoreRequest;
use App\Http\Requests\Course\ReorderLessonRequest;
use App\Http\Requests\Course\LessonUpdateRequest;
use App\Http\Requests\Course\StoreLessonDetailRequest;
use App\Transformers\CourseManagement\LessonTransformer;
use App\Transformers\CourseManagement\LessonDetailTransformer;
use App\Transformers\SchoolManagement\SchoolLevelLessonTransformer;

class LessonController extends Controller
{
    /**
     * This controller is for lessons
     */

    /**
     * Lesson controller constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Store Lesson API
     *
     * @param  \Illuminate\Http\LessonStoreRequest  $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(LessonStoreRequest $request)
    {
        $module = Module::where('id', $request->get('module_id'))->first();
        $lesson_name = $request->get('lesson_name');

        $lessonContent = new \stdClass();
        $lessonContent->id = uniqid();
        $lessonContent->slug = 'lesson-'. $lessonContent->id ;
        $lessonContent->lesson_name = $lesson_name;
        $lessonContent->active = 1;
        $lessonContent->order = $request->get('order');

//      decoding the $module->lessons from json to array for inserting the new object at last.
        $lesson = json_decode($module->lessons, true);
        $lesson[] = $lessonContent;

//      converting the new updated lesson to json and store it in again.
        $module->lessons = json_encode($lesson);
        $module->save();

//      course content is not fulfill for assigning to school thats y doing course active to 2
        Course::where('id',$module->course_id)->update(['active'=>2]);

        return SupportingFunctions::success($lesson_name . ' added successfuly.');
    }

    /**
     * Update Lessons API
     *
     * @param  \Illuminate\Http\LessonUpdateRequest  $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function update(LessonUpdateRequest $request)
    {
        $module = Module::where('id', $request->get('module_id'))->first();
        $lesson_id = $request->get('lesson_hash_id');
        $field_name = $request->get('field_name');

        $lessons = json_decode($module->lessons, true);
        if (count($lessons)<0) {
            return SupportingFunctions::success('Lesson does not exists!');
        }
        $updatedLesson = [];
        $lessonContent = null ;
        foreach ($lessons as $lesson) {
            if ($lesson['id'] == $lesson_id) {
                $lesson[$field_name] = $request->get('value');
                array_push($updatedLesson, $lesson);
                $lessonContent = $lesson['lesson_name'];
            } else {
                array_push($updatedLesson, $lesson);
            }
        }
        $module->lessons = json_encode($updatedLesson);
        $module->save();

        return SupportingFunctions::success($lessonContent. ' updated successfully.');
    }

    /**
     * Delete Lesson in modules API
     * Given both module_id and lesson hash id
     *
     * 1. Find for Lesson hash id
     * 2. if its exists Delete or Already Deleted
     * 3. Display proper response
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function destroy(Request $request, $active)
    {
        if (Module::where('id', $request->get('module_id'))->whereActive(1)->exists()) {
            
            $module = Module::where('id', $request->get('module_id'))->first();
            $lessons = Module::where('id', $request->get('module_id'))->value('lessons');
            $lessonData = json_decode($lessons, true);
            $flag = 0;
            $updatedLesson = [];

            if (!empty($lessonData)) {
                foreach ($lessonData as $key => $lesson) {
                    if ($lesson['id'] == $request->get('lesson_hash_id')) {
                        $lesson['active'] = $active;
                        array_push($updatedLesson, $lesson);
                        $flag = 1;

                    } else {
                        array_push($updatedLesson, $lesson);
                    }                    
                }                
                $module->lessons = json_encode($updatedLesson);
                $module->save();
                if ($flag)
                {
                    $activeFlag = LessonController::checkingCourseHasProperLessonWithContent($module->course_id);
                    if($activeFlag){
                        Course::where('id', $module->course_id)->update(['active' => 1]);
                    }
                    else{
                        Course::where('id', $module->course_id)->update(['active' => 2]);
                    }
//                    $this->updateNewLesson($request->get('module_id'), $request->get('lesson_hash_id'));
                    return SupportingFunctions::success('Lesson updated successfully!');
                }
                else{
                    return SupportingFunctions::success('Lesson does not exists.');
                }
            }
        }
        else{
            return SupportingFunctions::success('Lesson does not exists!');
        }
    }



    /**
     * API for List of active lessons for given module slug
     * 1. Find whether lessons exists
     * 2. check whether active lesson exists or not
     *
     * @param  \Illuminate\Http\Request  $module
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function index(Module $module)
    {
        $lessons = $module->lessons;
        $lessonData = json_decode($lessons, true);

        if (!empty($lessonData)) {
            return SupportingFunctions::transform($lessonData, new LessonTransformer); 
        } else {
            return SupportingFunctions::success('No lessons exists for given module!');
        }
    }

    /**
     * List all lesson for school-level
     * 
     */
    public function allLesson($course_content_id)
    {
        $courseContent = CourseContent::whereId($course_content_id)->first();
        $lesson = json_decode($courseContent->module_content, true); 
        $updated_lesson_list = array();
        if (!empty($lesson)) {
            // foreach ($lesson as $key => $value) {
            //    if ($value['active'] == 1) {
            //       $updated_lesson_list[] = $value;
            //    }
            // } 
            // return response()->json(['data' => $lesson, 'status' => 200]);
            // print_r($lesson[0]['active']);die;
            if (!empty($lesson)) {
                return SupportingFunctions::transform($lesson, new SchoolLevelLessonTransformer());          
            } 
            else
            {
                return SupportingFunctions::success('No data found!');
            }
        } else {
            return SupportingFunctions::success('Lessons not found!');
        }        
    }

    /**
     * 1.Fetch Lesson Details for given module and lesson_id
     * 2. Check for lesson id exists or not
     * 3. And it is not deleted
     *
     * @param $module, lessonId
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function show(Module $module, $lesson_id)
    {
        //json_decode for data from db
        //check for exists of lesson_id and not deleted
        $lessons = json_decode($module->lessons, true);
        if (empty($lessons)) {
            return SupportingFunctions::error('Lesson does not exists for the given module!');
        }
        foreach ($lessons as $lesson) {
            if ($lesson_id == $lesson['id']) {
                $found = 1;
                $lessonDetail[] = $lesson;
            }
        }
        if (isset($found) > 0) {
            return SupportingFunctions::transform($lessonDetail, new LessonDetailTransformer);
        } else {
            return SupportingFunctions::success('Given Lesson does not exists!');
        }
    }

    /**
     * 
     * Lesson detail for school-level from CourseContent
     * 
     */
    public function lessonDetail($course_content_id, $lesson_id)
    {
        $courseContent = CourseContent::whereId($course_content_id)->first();
        $lessons = json_decode($courseContent->module_content, true);
        if (!empty($lessons)) {
            foreach ($lessons as $lesson) {
                if ($lesson_id == $lesson['id']) {
                    $found = 1;
                    $lessonDetail[] = $lesson;
                }
            }
            if (isset($found) > 0) {
                return SupportingFunctions::transform($lessonDetail, new LessonDetailTransformer);
            } else {
                return SupportingFunctions::success('Given Lesson does not exists!');
            }
        } else {
            return SupportingFunctions::success('Lessons not found!');
        }        
    }

    /**
     * Function for reording the lesson
     */
    public function reorderLesson(ReorderLessonRequest $request)
    {
        $module = Module::where('id', $request->get('module_id'))->first();
        $lesson_id = $request->get('lesson_hash_id');
        $order = $request->get('order');
//      Calling the function from supporting function for reordering the lessons
        $supportingFunction = new SupportingFunctions();
        $reorderedLesson = $supportingFunction->reorderLessonOrModule($module->lessons, $lesson_id, $order);

        $module->update(['lessons' => json_encode($reorderedLesson)]);
        return SupportingFunctions::success('Order changed successfully');
    }

    /**
     * This function is used for adding lesson detail
     * @param LessonStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     * request parameter {
            module_id
     *      lesson_hash_id
     *      description
     *      video_url
     *      order
     *      quiz (should be in json)
     * }
     *
     */
    public function storeLessonDetail(StoreLessonDetailRequest $request)
    {
        $module = Module::where('id', $request->get('module_id'))->first();
        $lesson_id = $request->get('lesson_hash_id');
        $new_updated_lesson = [];
        $lessons = json_decode($module->lessons, true);
        $lesson_name = null;
        foreach ($lessons as $lesson) {
            if ($lesson['id'] == $lesson_id) {
                $lesson_name = $lesson['lesson_name'];
                array_push($new_updated_lesson, $request->get('lesson_detail'));
            } else {
                array_push($new_updated_lesson, $lesson);
            }
        }
        $module->lessons = json_encode($new_updated_lesson);
        $module->save();

//      --------------Checking for proper lesson content is present in each module------------
        $flag = $this->checkingCourseHasProperLessonWithContent($module->course_id);

        if($flag)
        {
            Course::where('id',$module->course_id)->update(['active'=>1]);
            $this->updateCourseContent($module->course_id);
        }

//      ------------------------------------END CHECKING--------------------------------------
        if ($lesson_name != null) {
            return SupportingFunctions::success($lesson_name. ' added successfuly.');
        } else {
            return SupportingFunctions::success('Lesson with given id not found.');
        }
    }

    public static function checkingCourseHasProperLessonWithContent($courseId)
    {
        $modules = Module::where(['course_id' => $courseId, 'active' => 1]);
        $lesson_flag = True;
        foreach ($modules->get() as $module)
        {
            $lessons = json_decode($module->lessons, true);
            if(!empty($lessons)){
                foreach ($lessons as $lesson)
                {
                    if(!isset($lesson['video']) && $lesson['active'] == 1){
                        $lesson_flag=False;
                    }
                }
            }
            else{
                $lesson_flag = False;
            }
        }
        return $lesson_flag;
    }

    /**
     * Updating course content when ever new module is added or new lesson is added to the list
     */
    public function updateCourseContent($courseId)
    {
       $schoolCoursesId = SchoolCourse::whereCourseId($courseId)->where('expiry_date->to', '>=', Carbon::now()->toDateString())->pluck('id');

       $module = Module::whereCourseId($courseId)->whereActive(1)->get();

       $module->map(function ($item) use($schoolCoursesId){
           foreach ($schoolCoursesId as $scid)
           {
               //checking if the passed module is exist in course content table or not.
               $cc = CourseContent::whereSchoolCourseId($scid)->whereModuleId($item->id)->first();
               if($cc){
                // so module is exists in course content, now we check for lessons.
                   if(!CourseProgress::whereSchoolCourseId($scid)->whereModuleId($item->id)->exists())
                   {
                       $ccLessonDetails = array_map(create_function('$o', 'return $o->id;'), json_decode($cc->module_content));

                       $masterLessonDetails = json_decode($item->lessons, true);
                       $orderCount = count($ccLessonDetails);
                       $updatedNewLesson = [];
                       foreach ($masterLessonDetails as $mlesson)
                       {
                          if(!in_array($mlesson['id'], $ccLessonDetails )){
                              $orderCount +=  1;
                              $mlesson['order'] = $orderCount;

                              array_push($updatedNewLesson,$mlesson);
                          }

                       }
                       $cc->module_content = json_encode(array_merge(json_decode($cc->module_content), $updatedNewLesson ));
                       $cc->save();
                   }

               }else{
                   // so module does not exists in course content, so we will create the new content.
                   $lessons = json_decode($item->lessons, true);
                   $activeLesson = SupportingFunctions::multivalue('active', 1, $lessons)->toArray();

                   usort($activeLesson, function ($a, $b) {
                       return strcmp($a['order'], $b['order']);
                   });

                   CourseContent::create(['school_course_id' => $scid,
                       'module_id' => $item->id,
                       'module_content' => json_encode($activeLesson),
                       'order' => $item->order]);

               }
           }
       });
    }

//    public function updateNewLesson($moduleId, $lessonId)
//    {
//
//    }

}
