<?php

namespace App\Http\Controllers\School;

use App\Models\User;
use App\Models\School\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\Password\SendPasswordResetMail;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\AddSchoolAdminRequest;
use App\Http\Requests\School\UpdateSchoolAdminRequest;
use App\Transformers\SchoolManagement\SchoolAdminTransformer;

class SchoolAdminController extends Controller
{
    /***
    *
    * This controller will have functions related to
    * schoolAdmin.
    */

    /**
     * SchoolAdminController constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
    * Add school-admin to school API
    *
    * role id for school admin is 2.
    * send password link to set password to that email.
    *
    * @param \Illuminate\Http\AddSchoolAdminRequest $request
    * @return \Illuminate\Http\JsonResponse
    *
    */

    public function store(AddSchoolAdminRequest $request)
    {
        $schoolAdmin = User::create(array_merge($request->all(), ['role_id' => '2']));
        //add id to schools table as school-admin
        School::whereId($request->school_id)->update(['school_admin_id' => $schoolAdmin->id]);
        //Password
        // Generate Email verification token
        $token = SupportingFunctions::saveToken($request->email);

        // Process the mail in queue
        dispatch(new SendPasswordResetMail($schoolAdmin, $token, $flag=3));

        return SupportingFunctions::success('School admin added successfully! Please ask the admin to login in to their email Id and create a new password to start using the application.');
    }

    /**
     * Update school admin details API
     *
     * 1. Fetch schooladmin email
     * 2. If its not matching with given email, delete old and create new
     * 3. Repeat steps
     *
     * @param \Illuminate\Http\UpdateSchoolAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
    */

    public function update(UpdateSchoolAdminRequest $request, School $school)
    {
        $schoolAdmin = User::where('id', $school->school_admin_id)->first();

        if ($schoolAdmin['email'] != $request->email) {
            //delete and create a new
            SupportingFunctions::deactivateModel(User::class, $school->school_admin_id, 0);
            $schoolAdmin->delete();

            $newschoolAdmin = User::create(array_merge($request->all(), ['role_id' => '2']));
            $school->update(['school_admin_id' => $newschoolAdmin->id]);
            //Password
            // Generate Email verification token
            $token = SupportingFunctions::saveToken($request->email);

            // Process the mail in queue
            dispatch(new SendPasswordResetMail($schoolAdmin, $token));
            return SupportingFunctions::success('School admin added successfully! Please ask the admin to login in to their email Id and create a new password to start using the application.');
        }
        $schoolAdmin->update(['first_name' => $request->first_name, 'last_name' => $request->last_name, 'phone'=> $request->phone]);
        return SupportingFunctions::success('School admin details updated successfully!');
    }

    /**
     * School admin details for given school
     * 
     */
    public function index(School $school)
    {
       $schoolAdmin = $school->schoolAdmin;
       if ($schoolAdmin) {
        return SupportingFunctions::transformItem($schoolAdmin, new SchoolAdminTransformer());
        } 
       return SupportingFunctions::success('School-admin is not assigned to this school!');
       
    }
}
