<?php

namespace App\Http\Controllers\School;

use App\Jobs\AssignCoordinator;
use App\Mail\Password\PasswordReset;
use DB;
use App\DataSerializer;
use App\Models\User;
use App\Models\School\Batch;
use App\Models\School\School;
use App\Models\School\SchoolCourse;
use Illuminate\Support\Carbon;
use App\Models\Course\Course;
use App\Models\School\Calendar;
use App\UserQuizData;
use App\Models\CoordinatorDetail;
use League\Fractal\Resource\Item; 
use League\Fractal\Resource\Collection;
use App\Models\School\CourseContent;
use App\Models\School\CourseProgress;
use App\Models\School\LessonProgress;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Mail\Coordinator\CoordinatorLogin;
use App\Jobs\Password\SendPasswordResetMail;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\Coordinator\LessonCompleteRequest;
use App\Http\Requests\School\DeleteCoordinatorRequest;
use App\Http\Requests\School\UpdateCoordinatorRequest;
use App\Http\Requests\SchoolManagement\QuizDataRequest;
use App\Http\Requests\Coordinator\CompleteModuleRequest;
use App\Http\Requests\Coordinator\AddCoordinatorRequest;
use App\Transformers\CourseManagement\CourseTransformer;
use App\Transformers\SchoolManagement\BatchForCoordinatorTransformer;
use App\Transformers\SchoolManagement\LessonQuizTransformer;
use App\Transformers\SchoolManagement\UnlimitedBatchListTransformer;
use App\Transformers\SchoolManagement\CoordinatorModuleListTransformer;
use App\Transformers\SchoolManagement\ModuleListUnlimitedTransformer;
use App\Transformers\SchoolManagement\CoordinatorOrSchoolAdminTransformer;

class CoordinatorController extends Controller
{
    /**
     *
     * This Controller will have functions related to
     * coordinator corresponding to batch and schools.
     *
     */
    /**
     * Coordinator Controller constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Delete a coordinator from school
     *
     * 1. Active course for school
     * 2. If present and not completed by coordinator then message
     * 3. Else Delete it from school
     *
     * @param \Illuminate\Http\DeleteCoordinatorRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function destroy(DeleteCoordinatorRequest $request)
    {
        $school = School::whereId($request->school_id)->first();
        if ($school->active == 1) {
            $courses = $school->courses;
            $activeCourse = [];
            foreach ($courses as $key => $course) {
                $data = json_decode($course->expiry_date);
                if (($data->to >= carbon::now()->format('Y-m-d'))) {
                    array_push($activeCourse,$course->id);
                }
            }
            //active courses
            //delete coordinator if he is not assigned or already completed batches
            if (!empty(isset($activeCourse))) {
                if (Batch::whereIn('school_course_id', $activeCourse)->where('coordinator_id', $request->coordinator_id)->where('completed', 0)->exists()) {
                        return SupportingFunctions::success('Sorry you can not delete the cordinator because he has assign to batch which has to be complete.');
                }
            }
            DB::table('school_coordinators')->where(['coordinator_id' => $request->coordinator_id, 'school_id' => $request->school_id])->delete();
            return SupportingFunctions::success('Coordinator deleted successfully!');
        } else {
            return SupportingFunctions::error('School is not active');
        }
    }

    /**
     * Add coordinators to school API
     *
     * 1.Check if user is already registerd , if so add them as coordinator
     * 2. Send mail if coordinator is assigned to school
     *
     * @param \Illuminate\Http\AddCoordinatorRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(AddCoordinatorRequest $request, School $school)
    {
        if (User::where('email', $request->email)->exists()) {
            $coordinatorData = User::where('email', $request->email)->first();

            if (DB::table('school_coordinators')->where(['school_id' => $school->id, 'coordinator_id' => $coordinatorData->id])->exists()) {
                return SupportingFunctions::error('Coordinator already added to this school');
            }
            //add to school
            DB::table('school_coordinators')->insert(['school_id' => $school->id, 'coordinator_id' => $coordinatorData->id]);
            //send email to coordinator saying you have been added as coordinator to this school.
            dispatch(new AssignCoordinator($coordinatorData));

            return SupportingFunctions::success('Coordinator is assigned to school, kindly ask coordinator to login to see the details');
        }
        $coordinator = User::create(array_merge($request->all(), ['role_id' => '3']));
        CoordinatorDetail::create(array_merge($request->all(), ['coordinator_id' => $coordinator->id]));
        DB::table('school_coordinators')->insert(['school_id' => $school->id, 'coordinator_id' => $coordinator->id]);

        //Password
        // Generate Email verification token
        $token = SupportingFunctions::saveToken($request->email);
        // Process the mail in queue
//        Mail::to($coordinator->email)->send(new PasswordReset($token, $coordinator));
        dispatch(new SendPasswordResetMail($coordinator, $token, $flag=1));

        return SupportingFunctions::success('Coordinator added successfully! Please ask the coordinator to login in to their email Id and create a new password to start using the application.');
    }

    /**
     * Batches List for coordinator API
     *
     *  1. get all batches for the given coordinator from calendar
     *  2. sort in the date and time ascending order
     *  3. show completed key to true.
     *
     *  @param \Illuminate\Http\User $user 
     *  @return \Illuminate\Http\JsonResponse
     */

    public function batches()
    {
        $user = auth()->user();
        $batchData = array();
        //coordinator dashboard
        if ($user->role->id == 3) {
            $batches = $user->batches->pluck('id');
            $unlimited = $user->school;
            $unlimitedData = array();
            foreach ($unlimited as $key => $value) {
                if ($value['engagement_type_id'] == 1) {
                    //school courses should not be expired
                    $courses = $value->courses->pluck('id');
                    $activeCourses = SchoolCourse::whereIn('id', $courses)->where('expiry_date->to', '>=', Carbon::now()->format('Y-m-d'))->pluck('id');
                    if (count($activeCourses)) {                        
                        $content =  CourseContent::whereIn('school_course_id', $activeCourses)->orderBy('order')->first();
                    }
                    $manager = new \League\Fractal\Manager();
                    $manager->setSerializer(new DataSerializer());
                    $result = new Item($content, new UnlimitedBatchListTransformer);              
                    $unlimitedData = $manager->createData($result)->toArray();

                }
            }
            if (count($batches)) {
                  $batch =  Calendar::where(['coordinator_id' => $user->id])->whereIn('batch_id', $batches)->where('scheduled_date', '>=', Carbon::now()->format('y-m-d'))->orderBy('scheduled_date')->latest()->first();
                $manager = new \League\Fractal\Manager();
                $manager->setSerializer(new DataSerializer());
                $result = new Item($batch, new BatchForCoordinatorTransformer);              
                $batchData = $manager->createData($result)->toArray(); 
            } 
            $data =  array_merge($unlimitedData, $batchData);
            if (count($data)) {
                return response()->json(['data' => [$data], 'status_code' => 200]);
            }
            else
            {
                return SupportingFunctions::success('Data not found!');
            }
        }
        else
        { 
            //school-admin dashboard
            $user = auth()->user();
            $school = School::where('school_admin_id', $user->id)->first();
            $sc = $school->courses->pluck('course_id');
            if ($school->courses) {
                // $content = CourseContent::whereIn('school_course_id', $sc)->get();
                $course = Course::whereId($sc)->get();
                return SupportingFunctions::transform($course, new CourseTransformer);            
            }
            else{
                return SupportingFunctions::success('No courses found for school');
            }
        }       
    }

    /**
     ** @param CompleteModuleRequest $request
     *  @param $batchId
     *  @return \App\Http\Controllers\jsonResponse
     *
     * This function is used for complete the module which will populate course progress table.
     * 
     */

    // public function completeModule(CompleteModuleRequest $request, $batchId)
    // {
    //     $courseContentId = $request->get('courseContent');
    //     $courseContent = CourseContent::whereId($courseContentId)->first();
    //     if (!CourseProgress::where('course_content_id', $courseContentId)->exists()) 
    //     {
    //         $courseProgress = new \stdClass();
    //         $courseProgress->module_id = $courseContent->module_id;
    //         $courseProgress->lessons = $courseContent->module_content;

    //         CourseProgress::create([
    //             'module_completed' => json_encode($courseProgress),
    //             'batch_id' => $batchId, 
    //             'entered_date' => Carbon::now()->toDateString(),
    //             'completed_by' => auth()->user()->id,
    //             'course_content_id' => $courseContentId,
    //             'module_id' => $courseContent->module_id 
    //         ]);

    //         return SupportingFunctions::success('Module '.$courseContent->module->module.' completed successfully.');
    //     } 
    //     else 
    //     {
    //         return SupportingFunctions::error('Module '.$courseContent->module->module.' already completed.');
    //     }
    // }

    public function completeModule($courseContentId, $batchId)
    {
        // $courseContentId = $request->get('courseContent');
        $courseContent = CourseContent::whereId($courseContentId)->first(); 
        // if (!CourseProgress::where('course_content_id', $courseContentId)->exists()) 
        // {
            // return 1;
            $courseProgress = new \stdClass();
            $courseProgress->module_id = $courseContent->module_id;
            $courseProgress->lessons = $courseContent->module_content;

            $cp = CourseProgress::create([
                'module_completed' => json_encode($courseProgress),
                'batch_id' => $batchId, 
                'entered_date' => Carbon::now()->toDateString(),
                'completed_by' => auth()->user()->id,
                'course_content_id' => $courseContentId,
                'module_id' => $courseContent->module_id 
            ]);
            $cc = CourseContent::where(['school_course_id' => $courseContent->school_course_id, 'active' => 1])->pluck('id');
            // return count($cc);
            
            $cp = CourseProgress::whereIn('course_content_id', $cc)->where(['batch_id' => $batchId])->count();
            // var_dump($cc);die;

            // print_r($cc)
            if (count($cc) == $cp) {
                return 1;
            }             
            return 0;


            // return SupportingFunctions::success('Module '.$courseContent->module->module.' completed successfully.');
            
        // } 
        // else 
        // {
        //     return SupportingFunctions::error('Module '.$courseContent->module->module.' already completed.');
        // }
    }
    

    //lesson complete 
    public function lessonComplete(LessonCompleteRequest $request, $lesson_id)
    {  
        LessonProgress::create(['school_course_id' => $request->school_course_id,
                                'course_content_id' => $request->course_content_id,
                                'batch_id' => $request->batch_id,
                                'module_id' => $request->module_id,
                                'lesson_hash_id' => $lesson_id,
                                'entered_date' => Carbon::now()->format('y-m-d'),
                                'updated_by' => auth()->user()->id]);
        $lessons = CourseContent::where('id', $request->course_content_id)->value('module_content');
        $lessons = CourseContent::where('id', $request->course_content_id)->first();
        $lessonContent = json_decode($lessons->module_content, true);
        // $lessonContent = collect(json_decode($lessons->module_content, true));
        $count = count(json_decode($lessons->module_content, true));
        // return $count;
        foreach($lessonContent as $key => $value)
        {
            if ($value['id'] == $lesson_id) {
                $found = 1;
                $index = $key+1;
            }            
        }
        
        if(isset($found))
        {
            if ($index != $count) {
                // return 2222;
                
                return response(['batchCompleted' => false, 'message' => 'Lesson completed']);
            }

            // foreach($lessonContent as $key => $value)
            // {
            //     if($key == $index)
            //     {
            //        $nextLesson = $value;
            //         return response(['batchCompleted' => false, 'message' => 'Lesson completed']);
            //     }
            // }
        } 
        // return 1111;
        $result = $this->completeModule( $request->course_content_id, $request->batch_id);
        // return $result;
        if ($result) 
        {
            return response(['batchCompleted' => true, 'message' => 'Lesson completed']);
        }
        else
        {
            return response(['batchCompleted' => false, 'message' => 'Lesson completed']);

        }

    }

    /**
     *  Update Coordinator API
     *  Modoadmin will update coordinator's details.
     *
     */
    public function update(UpdateCoordinatorRequest $request, User $user)
    {
        $user->update($request->all());
        CoordinatorDetail::where('coordinator_id', $user->id)
                          ->update(['coordinator_id' => $user->id,
                                    'qualification' => $request->qualification,
                                    'occupation' => $request->occupation,
                                    'address' => $request->address
                                    ]);
        return SupportingFunctions::success('Coordinator updated successfully!');
    }

    /**
     * List of all coordinator for given school
     * 
     * @param $school
     * @return \App\Http\Controllers\jsonResponse
     * 
     */
    public function index(School $school)
    {
        $coordinators = $school->coordinators;
        if ($coordinators->isNotEmpty()) 
        {
            return SupportingFunctions::transform($coordinators, new CoordinatorOrSchoolAdminTransformer());            
        }
        return SupportingFunctions::success('There is no coordinators assigned for given school!');
    }

    /**
     * List of modules for given batch
     * 1. Fetch scid for a batch
     * 2. Fetch cc for given scid
     * 3. Fetch completed modules for ccid from above
     * 4. Fetch active modules from batch.
     * 
     */
    public function moduleList($batchId, $schoolCourseId)
    {
        if ($batchId != 'unlimited') 
        {
            $batch = Batch::whereId($batchId)->first();
            $array = array();
            $courseContent = CourseContent::where(['school_course_id' => $batch->school_course_id])->get();        
            foreach ($courseContent as $key => $value) 
            { 
                $manager = new \League\Fractal\Manager();
                $manager->setSerializer(new DataSerializer());
                $result = new Item($value->module, new CoordinatorModuleListTransformer($batch));
                $batchData[] = $manager->createData($result)->toArray();    
            }
            return response()->json(['data' => $batchData, 'status' => 200 ]);        
        }
        else
        { 
            $courseContent = CourseContent::where(['school_course_id' => $schoolCourseId])->get();
            $schoolCourse = SchoolCourse::whereId($schoolCourseId)->first();
            foreach ($courseContent as $key => $value) 
            { 
                $manager = new \League\Fractal\Manager();
                $manager->setSerializer(new DataSerializer());
                $result = new Item($value->module, new ModuleListUnlimitedTransformer($schoolCourse));
                $data[] = $manager->createData($result)->toArray(); 
            }
            return response()->json(['data' => $data, 'status' => 200 ]);        

        }
    }

    /**
     * Given lesson_hash_id, course_content_id
     * Fetch lesson details, quiz questions.
     * 
     *   */
    public function lessonDetails($lesson_id, $course_content_id, $batchId)
    {
        if(!LessonProgress::where(['lesson_hash_id' => $lesson_id, 'course_content_id' => $course_content_id, 'batch_id' => $batchId])->exists())   
        {   
            $completed = false;
        }
        else{
            $completed = true;
        }
        $lessons = CourseContent::whereId($course_content_id)->value('module_content');
        $content = CourseContent::whereId($course_content_id)->first();
        $data = json_decode($lessons,true);
        foreach ($data as $key => $value) {
            if ($value['id'] == $lesson_id) {
                $lessonData = [
                    'video_url' => $value['video'],
                        'quiz' => $value['quiz'],
                        'school_course_id' => $content->school_course_id,
                        'module_id' => $content->module_id,
                        'lesson_name' => $value['lesson_name'],
                        'completed' => $completed                   
                    ];
                    return response()->json(['data' => $lessonData, 'status' => 200]);
            }
        }
        return SupportingFunctions::success('No lesson found!');
        
    }

    /**
     * Take quiz data
     * 
     */
    public function quizData(QuizDataRequest $request)
    {
        UserQuizData::create(['user_id' => auth()->user()->id,
                          'course_content_id' => $request->course_content_id,
                          'lesson_id' => $request->lesson_id,
                          'quiz' => $request->quiz]);
        return SupportingFunctions::success('Anwser submitted!');
    }

    /**
     * List all lessons for lesson page
     * 
     */
    public function lessonList($courseContentId, $batchId)
    {         
        $courseContentDetails = CourseContent::where(['id' => $courseContentId])->first(); 
        $lessons = collect(json_decode($courseContentDetails->module_content, true));
        $completedLessons = LessonProgress::where(['course_content_id' => $courseContentDetails->id, 'batch_id' => $batchId])->pluck('lesson_hash_id')->toArray();

        //Making as collection and then filtering the lessons where active = 1 .
        $lessons = $lessons->filter(function ($item){
            return $item['active'] == 1;
        })->values();

        $data = $lessons->map(function ($item) use ($completedLessons) {
                    if (in_array($item['id'], $completedLessons)) {
                        $lessonCompleted = true;
                    }
                    else{
                        $lessonCompleted = false;
                    }       
                    return [
                        'id' => $item['id'],
                        'slug' => $item['slug'],
                        'name' => $item['lesson_name'],
                        'order' => $item['order'],
                        'completed' => $lessonCompleted
                    ];           
                }); 
                
        return response()->json(['data' => $data, 'status' => 200]);
    }
}
