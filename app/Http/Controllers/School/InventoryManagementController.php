<?php

namespace App\Http\Controllers\School;

use App\DataSerializer;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\UpdateInventoryManagement;
use App\Models\InventoryCategory;
use App\Models\School\Inventory;
use App\Models\School\SchoolInventory;
use App\Transformers\SchoolManagement\InventoryManagement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InventoryManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');

    }

    /**
     * @param $school_id
     * @return array
     * List of inventory accroding to category for particular school
     * As of now this function is not getting used.
     */
    public function listInventory($school_id)
    {
        $inventory = InventoryCategory::whereHas('inventories',function ($q) use($school_id){
            $q->where('school_id',$school_id);
        })->get();

        $manager = new \League\Fractal\Manager();
        $manager->setSerializer(new DataSerializer());
        $result = new \League\Fractal\Resource\Collection($inventory, new InventoryManagement($school_id));
        $data = $manager->createData($result)->toArray();
        return $data;

    }

    public function updateInventoryManagement(UpdateInventoryManagement $request)
    {
        $inventory = Inventory::find($request->get('inventory_id'));

        if((int)$inventory->quantity !== (int)($request->working + $request->not_working + $request->lost))
        {
            return response()->json(['error' => 'Data you have provided is not proper for the quantity of this inventory.'],433);
        }
        SchoolInventory::updateOrCreate([
            'inventory_id' => $request->get('inventory_id'),
            'entered_date' => $request->get('entered_date'),
        ],
         array_merge($request->all(), ['updated_by' => auth()->user()->id]));
        return SupportingFunctions::success('Inventory details updated successfully!');

    }

    public function listManageInventory(Request $request)
    {
       $date = $request->get('date');
       $schoolId = $request->get('schoolId');
       $schoolInventory = SchoolInventory::whereHas('inventory',function ($q) use($schoolId){
           $q->where('school_id',$schoolId);
       })->whereEnteredDate($date)->get();

        return SupportingFunctions::transform($schoolInventory, new \App\Transformers\SchoolManagement\SchoolInventory);

    }
}
