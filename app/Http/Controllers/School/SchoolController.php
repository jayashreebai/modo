<?php

namespace App\Http\Controllers\School;

use DB;
use Image;
use Carbon\Carbon;
use App\Models\User;
use App\Models\EngagementType;
use Illuminate\Http\Request;
use App\Models\School\School;
use App\Models\School\Batch;
use App\Models\Course\Course;
use App\Models\Course\Module;
use App\Models\School\SchoolCourse;
use App\Jobs\PopulateCourseContent;
use App\Models\School\CourseContent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\SchoolStoreRequest;
use App\Http\Controllers\Course\ModuleController;
use App\Http\Requests\School\SchoolUpdateRequest;
use App\Http\Requests\School\AssignCourseRequest;
use App\Http\Requests\School\UnassignCourseRequest;
use App\Transformers\SchoolManagement\CourseTransformer;
use App\Transformers\CourseManagement\ModuleTransformer;
use App\Transformers\SchoolManagement\SchoolTransformer;
use App\Transformers\SchoolManagement\SchoolAllDetailsTransformer;
use App\Http\Requests\School\ReorderLessonForCourseContentRequest;
use App\Http\Requests\SchoolManagement\UpdateCourseDateRequest;
use App\Http\Requests\School\ReorderModuleForCourseContentRequest;
use App\Transformers\SchoolManagement\SchoolCourseModuleTransformer;
use App\Transformers\SchoolManagement\SchoolListTransformer;
use App\Transformers\SchoolManagement\ForDropdownTransformer;
use App\Transformers\SchoolManagement\CourseContentTransformer;

class SchoolController extends Controller
{
    /***
     *
     * This controller will have functions related to
     * school.
     */

    /**
     * SchoolController constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * School creation API
     *
     * 1. Create a school with the data given by modo-admin
     * 2. Create a school contacts is different api
     * 3. Logo can be added.
     *
     * @param \Illuminate\Http\SchoolStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
    */

    public function store(SchoolStoreRequest $request)
    {
        if ($request->hasFile('logo')) {
            $folder = '/school/';
            $storagePath = SupportingFunctions::addImage($request, $folder, 'logo'); 
        }

        // store the school values
        $school = School::create([
            'school' => strtolower($request->name),
            'address' => $request->address,
            'website_url' => $request->website,
            'contact_number' => $request->contact,
            'logo' => isset($storagePath) ? $storagePath : null
        ]);

        return response()->json(['data' => ['id' => $school->id, 'slug' => $school->slug, 'logo' => $school->logo, 'message' => 'School '. $school->school .' created successfully!'], 'status_code' => 200]);

    }

    // /**
    //  * Update school details API
    //  * One field at a time
    //  *
    //  * @param \Illuminate\Http\SchoolUpdateRequest $request, $school slug
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function update(SchoolUpdateRequest $request, School $school)
    // {
    //     $fieldname = $request->field_name;

    //     //if field is logo
    //     if ($fieldname == 'logo') {
    //         $storagePath = null;
    //         if ($request->file('value')) {

    //             // If user already have a profile pic
    //             if (Storage::disk('public')->exists($school->logo)) {
    //                 Storage::disk('public')->delete($school->logo);
    //             }

    //             // Create Image instance
    //             $resizeImage = Image::make($request->file('value'));

    //             // Resize the image to width of 300 and constraint aspect ratio (auto height)
    //             $resizeImage->resize(800, null, function ($constraint) {
    //                 $constraint->aspectRatio();
    //             })->encode('jpg');

    //             // Generates a unique alphanumeric string
    //             $imageName = (string)md5(time() . rand());

    //             // Storing the image in storage/app/public/user-profile
    //             Storage::disk('public')
    //                 ->put('/school/' . $imageName . '.jpg', $resizeImage, 'public');
    //             // Saving the path in our DB

    //             $storagePath =  '/school/' . $imageName . '.jpg'; 
    //         }
    //         $school->update(['logo' => $storagePath]);
    //     } else {
    //         //other than logo
    //         $school->update([$fieldname =>$request->value]);
    //     }

    //     return SupportingFunctions::success($fieldname .' updated successfully!');
    // }

     /**
     * Update school details API - new jay
     * One field at a time
     *
     * @param \Illuminate\Http\SchoolUpdateRequest $request, $school slug
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SchoolUpdateRequest $request, School $school)
    {
        $school->update(['school' => $request->name,
                        'address' => $request->address,
                        'website_url' => $request->website,
                        'contact_number' => $request->contact]);
        $storagePath = null;
        if ($request->file('logo')) {

            // If user already have a profile pic
            if (Storage::disk('public')->exists($school->logo)) {
                Storage::disk('public')->delete($school->logo);
            }

            // Create Image instance
            $resizeImage = Image::make($request->file('logo'));

            // Resize the image to width of 300 and constraint aspect ratio (auto height)
            $resizeImage->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg');

            // Generates a unique alphanumeric string
            $imageName = (string)md5(time() . rand());

            // Storing the image in storage/app/public/user-profile
            Storage::disk('public')
                ->put('/school/' . $imageName . '.jpg', $resizeImage, 'public');
            // Saving the path in our DB

            $storagePath =  '/school/' . $imageName . '.jpg';
        }
        $school->update(['logo' => $storagePath]); 

        // return SupportingFunctions::success($school->school .' updated successfully!');
        return response()->json(['message' => $school->school .' updated successfully!', 'status' => 200, 'id' => $school->id, 'slug' => $school->slug]);
    }
    /**
     * Delete School Api
     * 1. Deactivate schooladmin
     * 2. Delete coordinators assigned for school
     * 3. Deactivate and delete school.
     * 
     * @param \Illuminate\Http\Request $request, string $school
     * @return \Illuminate\Http\JsonResponse
     *
    */

    public function destroy(School $school)
    {
        //Disable school-admin
        SupportingFunctions::deactivateModel(User::class, $school->school_admin_id, 0);

        //disable coordinators or delete school from school-coordinators table
        DB::table('school_coordinators')->where('school_id', $school->id)->delete();

        SupportingFunctions::deactivateModel(School::class, $school->id, 0);
        $school->delete();

        return SupportingFunctions::success('School deleted successfully!');
    }

    /**
     * Assign course API
     *
     * 1.Populate course content from master tables
     * 2.School course tables
     * 3.Queue for poplating table
     *
     *
     * @param \Illuminate\Http\AssignCourseRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     */

    public function assignCourse(AssignCourseRequest $request, School $school)
    {
        if (SchoolCourse::where(['school_id' => $school->id, 'course_id' => $request->course_id])->exists()) {
            return SupportingFunctions::error('Course is already assigned to'.$school->name);
        }

        //school_course
        $schoolCourse = SchoolCourse::create(['school_id' => $school->id, 'course_id' => $request->course_id, 'expiry_date' => $request->get('expiry')]);

        //Course content
        $modules = Module::where(['course_id' => $request->course_id, 'active' => 1])->get();

        //queue
        PopulateCourseContent::dispatch($modules, $schoolCourse->id);

        $course = Course::where('id', $request->course_id)->value('course');
        return SupportingFunctions::success($course.' assigned to '.$school->school. ' successfully!');
    }

    /**
     * Unassign course API
     *
     * 1.softdelete course for school given
     * 2. It shouldnot accessible to school-admin and coordinator.
     */
    public function unAssignCourse(UnassignCourseRequest $request, School $school)
    {
        $schoolCourse = SchoolCourse::where(['school_id' => $school->id, 'course_id' => $request->course_id])->first();
        if (!empty($schoolCourse)) {
            if (!Batch::where('school_course_id', $schoolCourse->id)->exists()) {
                $schoolCourse->delete();
                $courseName = Course::where('id', $request->course_id)->value('course');
                return SupportingFunctions::success($courseName . ' unassigned from '.$school->name .' successfully!');
            }
            return SupportingFunctions::error('Cannot unassign course because batch already started with this course');
        }
        return SupportingFunctions::success('Course is not assigned to school');
    }

    /**
     * Show school details API
     * 1. school fields
     * 2. All engagementtypes with selected engagement-type true.
     *
     * @param \Illuminate\Http\Request $school
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function show(School $school)
    {
        $schoolData = School::where('id', $school->id)->get();
        return SupportingFunctions::transform($schoolData, new SchoolTransformer);  
    }

    /**
     * jay - changed to active or deactivate module school level
     * @param Request $request
     * @param CourseContent $courseContent
     * @return \App\Http\Controllers\jsonResponse|CourseContent
     * This function is used for deleting module from course content(Course customisation)
     */
    public function deleteModule(Request $request, CourseContent $courseContent, $active)
    {
        if(Batch::whereCompleted(0)->whereSchoolCourseId($courseContent->school_course_id)->exists())
        {
            return response()->json(['error' => 'Sorry you can not delete module because batch has started with this course'],433);
        }
        $courseContent->update(['active'=>$active]);
        // $courseContent->delete();
        return SupportingFunctions::success($courseContent->module->module.' updated successfully.');
    }

    /**
     * added active flag to activate or deactivate flags - jay
     * @param Request $request
     * @param CourseContent $courseContent
     * @return \App\Http\Controllers\jsonResponse|CourseContent
     * This function is used for deleting lesson from course content for given course content id(Course customisation)
     */
    public function deleteLesson(Request $request,$courseContentId, $active)
    {
        $courseContent = CourseContent::whereId($courseContentId)->first();

        if(Batch::whereCompleted(0)->whereSchoolCourseId($courseContent->school_course_id)->exists())
        {
            return response()->json(['error' => 'Sorry you can not delete lesson because batch has started with this course'],433);
        }

        $lessonContents = json_decode($courseContent->module_content, true);
        $newUpdatedLesson = [];
        $lessonName = null;
        foreach ($lessonContents as $lessonContent) {
            if ($lessonContent['id'] == $request->get('lesson_hash_id')) {
                $lessonContent['active'] = $active;
                $lessonName = $lessonContent['lesson_name'];
                array_push($newUpdatedLesson, $lessonContent);
            } else {
                array_push($newUpdatedLesson, $lessonContent);
            }
        }
        $courseContent->update(['module_content' => json_encode($newUpdatedLesson)]);
        if (isset($lessonName)) {
            return SupportingFunctions::success($lessonName.' updated successfully.');
        } else {
            return SupportingFunctions::success('Lesson with given id not found.');
        }
    }

    /**
     * @param Request $request
     * @param CourseContent $courseContent
     * @return \App\Http\Controllers\jsonResponse
     * This function is used for reordering the lesson for course content
     * You jst have to use reusable function to achieve this.
     */
    public function reorderLessonForCourseContent(ReorderLessonForCourseContentRequest $request, CourseContent $courseContent)
    {

        if(Batch::whereCompleted(0)->whereSchoolCourseId($courseContent->school_course_id)->exists())
        {
            return response()->json(['error' => 'Sorry you can not reorder the lesson because batch has started with this course'],433);
        }

        $lesson_id = $request->get('lesson_hash_id');
        $order = $request->get('order');
        $lessonContent = $courseContent->module_content;
//      Calling the function from supporting function for reordering the lessons
        $supportingFunction = new SupportingFunctions();
        $reorderedLesson = $supportingFunction->reorderLessonOrModule($lessonContent, $lesson_id, $order);
//      Update the lesson latest lesson detail by order in course content
        $courseContent->update(['module_content' => json_encode($reorderedLesson)]);
        return SupportingFunctions::success('Lesson order changed successfully.');
    }

    /**
     * @param Request $request
     * @param CourseContent $courseContent
     * @return \App\Http\Controllers\jsonResponse
     * this function is used for reorerding the module for course content
     * You jst have to use reusable function to achieve this.
     */
    public function reorderModuleForCourseContent(ReorderModuleForCourseContentRequest $request, CourseContent $courseContent)
    {

        if(Batch::whereCompleted(0)->whereSchoolCourseId($courseContent->school_course_id)->exists())
        {
            return response()->json(['error' => 'Sorry you can not reorder the module because batch has started with this course'],433);
        }

        $courseContents = CourseContent::where('school_course_id', $courseContent->school_course_id)->get();
        $order = $request->get('order');

        $supportingFunction = new SupportingFunctions();
        $reorderedModules = $supportingFunction->reorderLessonOrModule($courseContents, $courseContent->id, $order);
//      below line of code is use for updating the course with the given ordered value.
        foreach ($reorderedModules as $module) {
            CourseContent::whereId($module['id'])->update(['order' => $module['order']]);
        }
        return SupportingFunctions::success('Module order changed successfully.');
    }

    /**
     * List of course for school API
     * // chnaged by jay
     * // list of course with selected key true if assigned for a school
     * @param \Illuminate\Http\Request $school
     * @return \Illuminate\Http\JsonResponse
     */
    public function course(School $school)
    {       
        return SupportingFunctions::transformItem($school, new CourseTransformer);
    } 

    /**
     * List of modules for given school-course-id
     * @param \Illuminate\Http\Request $schoolCourse
     * @return \Illuminate\Http\JsonResponse
     */
    public function modules($schoolCourse)
    {
        if (SchoolCourse::where('id', $schoolCourse)->where('expiry_date->to', '>=', Carbon::now()->format('Y-m-d'))->exists()) {
            $content = CourseContent::where('school_course_id', $schoolCourse)->orderBy('order')->get();
            return SupportingFunctions::transform($content, new SchoolCourseModuleTransformer);
        }
        return SupportingFunctions::error('Course as expired');
    }

    /**
     * Get details for school
     * @param \Illuminate\Http\Request $schoolId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDetailForSchool($schoolId)
    {
        $school = School::whereId($schoolId)->get();
        return SupportingFunctions::transform($school, new SchoolAllDetailsTransformer());
    }

    /**
     * Assigned course for school for dropdown
     * 
     * only id and name is fetched
     * 
     */
    public function courseForSchool(School $school) 
    {
        $schoolCourse = $school->courses->pluck('course_id')->toArray();
        $course = Course::get();
        
        $active_course = $course->map(function ($item) use ($schoolCourse, $school) {
            if (in_array($item['id'], $schoolCourse)) {
                $data = SchoolCourse::where(['school_id' => $school->id, 'course_id' => $item['id']])->first();
                $date = json_decode($data->expiry_date, true);
                $retVal = (Carbon::now()->format('Y-m-d') < $date['to']) ? false : true ;
                if ($retVal == false) {
                    
                    return [
                        "id" => $data->id,
                        "total_modules" => $item['course_duration'],
                        "course" => $item['course'],
                        "expired" => $retVal,
                    ];
                }                   
            }
        });
        if (!empty($active_course)) 
        {
            foreach ($active_course as $key => $value) {
               if($value != null)
               {
                   $assignedCourses[] = $value;
               }
            }
        } else {
            return SupportingFunctions::success(['School doesnot have a course assigned!']);
        }

        if (!empty($assignedCourses)) {
            return response()->json(['data' => $assignedCourses, 'status' => 200]);
        } else {
            return SupportingFunctions::success(['School doesnot have a course assigned!']);
        }       
    }

    /**
     * school - level customization 
     * 
     * 1.For given schoolcourse-id, fetch details by course-content
     * 
     */
    public function courseCustomization($schoolCourseId)
    {
        if (SchoolCourse::whereId($schoolCourseId)->exists()) {
             $c = CourseContent::where('school_course_id', $schoolCourseId)->get();
            return SupportingFunctions::transformItem($schoolCourseId, new CourseContentTransformer());
            
        } else {
            return SupportingFunctions::success('Course not found!');
        }
    }  
    
    /**
     * Update course date for given school_course_id
     * Basically when batch has no active_module_id update expiry_date.
     * 
     */
    public function updateCourseDate(UpdateCourseDateRequest $request)
    {
        $activeModule = Batch::where('school_course_id', $request->school_course_id)->where('active_module_id','!=', NULL)->pluck('active_module_id');
        if(count($activeModule))
        {
            return SupportingFunctions::success('Cannot update, course is in-progress');
        }
        SchoolCourse::whereId($request->school_course_id)->update(['expiry_date' => $request->expiry_date]);
        return SupportingFunctions::success('Date update successfully!');
    }
 
    /**
     * school list
     * 
     */
    public function index()
    {
        $school = School::whereActive(1)->get();
        if ($school) {
            return SupportingFunctions::transform($school, new SchoolListTransformer());         
        }
        else
        {
            return SupportingFunctions::success('No schools found!');
        }
    }

    /**
     * which step is completed while creating a school
     * 1. check for engagement-type-id [1]
     * 2. check for school-admin,coordinators,courses for school [1,2]
     */
    public function stages(School $school)
    {
        if($school->engagement_type_id == NULL)
        {
            return response()->json(['data' => [1] , 'status' => 200]);
        }
        elseif (count($school->courses))
        {
            if ($school->school_admin_id && count($school->coordinators)) 
            {
                return response()->json(['data' => [1,2,3] , 'status' => 200]);
                
            }
            return response()->json(['data' => [1,2] , 'status' => 200]);

        }
        else
        {
            return response()->json(['data' => [1,2] , 'status' => 200]);


        }

    }

    public function schoolListAccrodindToUser()
    {
        $authUser = auth()->user();
        $user = User::find($authUser->id);
        if ($user->role_id == 1){
           $school = School::all();
        }elseif ($user->role_id == 2){
            $school = collect([$user->schoolAdmin]);
        }else{
           $school = $user->school;
        }

        return $school->map(function($item){
            return [
              'id' => $item->id,
              'slug' => $item->slug,
              'schoolName' =>$item->school
            ];
        });
    }
}
