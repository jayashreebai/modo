<?php

namespace App\Http\Controllers\School;

use App\Http\Requests\School\GalleryUpdateRequest;
use App\Models\School\SchoolCourse;
use Illuminate\Support\Facades\URL;
use Image;
use Illuminate\Http\Request;
use App\Models\School\School;
use App\Models\School\Gallery;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\UploadGalleryRequest;
use App\Transformers\SchoolManagement\GalleryTransformer;

class GalleryController extends Controller
{
    /**
     * Gallery controller will have services for
     * uploading image or video and delete images or videos.
     *
     */

    /**
     * SchoolController constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Upload API
     *
     * @param  \Illuminate\Http\UploadGalleryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $schoolSlug = $request->get('school_course_id');
        $sid = School::whereSlug($schoolSlug)->first();
        $host = URL::to('/storage');

        $count = $request->get('count');
        for($i=1; $i <= $count; $i++)
        {
            $type = $request->get('type'.$i);
            if ($type === 'image') {
                $folder = '/schools/gallery/images';
                $imageName = (string)md5(time() . rand());
                $storagePath = SupportingFunctions::addImage($request, $folder, 'file'.$i);
                $url = $host.$storagePath;
                $tag = "<div style='height:200px;background-position: center;background-size: cover; background-image: url($url)'/>";
            } elseif($type === 'video') {
                $video=$request->file('file'.$i);
                $input = time().$video->getClientOriginalName();
                $destinationPath = 'storage/schools/gallery/videos';
                $video->move($destinationPath, $input);
                $url = $host.'/schools/gallery/videos/'.$input;
                $tag = "<video style='width: 100%; height: 200px;' id=\"video-player\" autoplay preload='none' controls><source src= $url type='video/mp4'/></video>";

            }
            // store the gallery values
            Gallery::create([
                'school_id' => $sid->id,
                'description' => 'Please add description.',
                'url' => $tag,
                'added_by' => auth()->user()->id,
                'updated_by' => auth()->user()->id
            ]);

        }

        return SupportingFunctions::success('File uploaded successfully!');
    }

    /**
     * Delete image/video API
     *
     * @param  \Illuminate\Http\galleryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($gallery_id)
    {
        Gallery::where('id', $gallery_id)->delete();
        return SupportingFunctions::success('Image/video deleted successfully!');
    }

    /**
     * Update image/video discription API
     *
     * @param  \Illuminate\Http\galleryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDescription(UploadGalleryRequest $request, $gallery_id)
    {
        Gallery::where('id', $gallery_id)->update([
            'description' => $request->get('description')
        ]);
        return SupportingFunctions::success('Description updated successfully!');
    }
    /**
     * List of gallery for given schoolId
     *
     *
     * @param  \Illuminate\Http\school $schoolId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(School $schoolcourse)
    {
        if (!empty($schoolcourse->galleries)) {
            return SupportingFunctions::transform($schoolcourse->galleries, new GalleryTransformer);
        }
        return SupportingFunctions::success('Galleries not found!');
    }
}
