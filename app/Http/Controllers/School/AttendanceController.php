<?php

namespace App\Http\Controllers\School;

use App\Models\School\Calendar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\School\Student;
use App\Models\School\Attendance;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\AddAttendanceRequest;
use App\Transformers\SchoolManagement\AttendanceTransformer;

class AttendanceController extends Controller
{
    /**
     * This controller will have services related to
     * attendances
     *
     */

    /**
     * SchoolController constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Store Attendance API
     *
     * 1. Fetch all students for given batch_id
     * 2. For given students for absentees mark 0 for a given date
     * 3. validate date should be today or yesterday, and if already entered.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(Request $request, $batch_id)
    {
        // Checking for validation
        $validate = new AddAttendanceRequest();

        $data = $validate->attendanceValidation($request);

        // If any validation error is there, then return that error
        if ($data) {
            return response()->json(['message' => 'The given data was invalid.', 'errors' => $data->original], 422);
        }

        if ($request->date != Carbon::now()->format('Y-m-d')) {
            return SupportingFunctions::error('Given date should be todays date');
        }

        $user_id = auth()->user()->id;

        if (!Attendance::where(['entered_date' =>  $request->date, 'batch_id' => $batch_id])->exists()) {
            $data = $request->absentees;
            // checking if coordinator is taking attendance on created event or not. bcoz he will not allowed to take the attendance other then event date.
            if(!Calendar::where(['batch_id' => $batch_id,'scheduled_date' => $request->date,'coordinator_id' => $user_id])->exists()){
                return SupportingFunctions::error('Sorry you cannot take attendance on the selected date.');
            }
            
            foreach ($data as $key => $student) {
                $studentList[] = $student['student_id'];
                Attendance::create(['student_id' => $student['student_id'],
                                    'batch_id' => $batch_id,
                                    'entered_date' => $request->date,
                                    'present' => 0,
                                    'created_by' => $user_id
                                    ]);
            }
            $presentStudents = Student::whereNotIn('id', $data)->where('batch_id', $batch_id)->pluck('id');
            if (!empty($presentStudents)) {
                foreach ($presentStudents as $student) {
                    Attendance::create(['student_id' => $student,
                                    'batch_id' => $batch_id,
                                    'entered_date' => $request->date,
                                    'present' => 1,
                                    'created_by' => $user_id
                                    ]);
                }
            }
            return SupportingFunctions::success('Attendance taken successfully!');
        }
        return SupportingFunctions::error('Attendance is already taken for the Date');
    }

    /**
    * Update Attendance API
    *
    * 1. Update attendance for given attendance_id
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\JsonResponse
    */
    public function update(Request $request, $attendance_id)
    {
        // Checking for validation
        $validate = new AddAttendanceRequest();

        $data = $validate->updateAttendanceValidation($request);

        // If any validation error is there, then return that error
        if ($data) {
            return response()->json(['message' => 'The given data was invalid.', 'errors' => $data->original], 422);
        }

        if (Attendance::where('id', $attendance_id)->exists()) {
            Attendance::where('id', $attendance_id)->update(['present' => $request->present]);
            return SupportingFunctions::success('Attendance updated successfully!');
        }
        return SupportingFunctions::success('Attendance not found!');
    }

    /**
     *
     * View attendance API
     * All roles should be able to see the attendance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function index($batch_id, $date)
    {
        $attendance = Attendance::where(['batch_id' => $batch_id, 'entered_date' => $date])->get();
        if (!empty($attendance)) {
            return SupportingFunctions::transform($attendance, new AttendanceTransformer);
        }
        return SupportingFunctions::success('No attendance data exists for given date');
    }
}
