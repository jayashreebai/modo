<?php

namespace App\Http\Controllers\School;

use App\Models\School\Attendance;
use App\Models\School\Calendar;
use Carbon\Carbon;
use Validator;
use App\Models\School\Batch;
use Illuminate\Http\Request;
use App\Models\School\Student;
use App\Imports\StudentsImport;
use App\Models\School\SchoolCourse;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\AddStudentRequest;
use App\Http\Requests\Student\ImportStudentRequest;

class StudentController extends Controller
{
    /**
     * This controller is used for student related services.
     */

    /**
     * Student Controller constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Store student API
     *
     * @param \Illuminate\Http\AddStudentRequest $school
     * @return \Illuminate\Http\JsonResponse
    */
    public function store(AddStudentRequest $request, $batch_id)
    {        
        Student::create(['name' => $request->name,
                        'dob' => $request->dob,
                        'gender' => $request->gender,
                        'batch_id' => $batch_id]);
        // Student::create(array_merge($request->all(), ['batch_id' => $batch_id]));
        return SupportingFunctions::success('Student added successfully!');
    }

    /**
     * Update student API
     *
     * @param \Illuminate\Http\AddStudentRequest $school
     * @return \Illuminate\Http\JsonResponse
    */
    public function update(AddStudentRequest $request, Student $student)
    {
        $student->update(['name' => $request->name, 'gender' => $request->gender, 'dob' => $request->dob]);
        return SupportingFunctions::success('Student updated successfully!');
    }

    /**
    * Delete student API
    *
    * @param \Illuminate\Http\Request $school
    * @return \Illuminate\Http\JsonResponse
    */
    public function destroy(Student $student)
    {
        $student->delete();
        return SupportingFunctions::success('Student deleted successfully!');
    }

    /**
     * @param ImportStudentRequest $request
     * @param $batchId
     * @return \App\Http\Controllers\jsonResponse|\Illuminate\Http\JsonResponse
     * This function is used for Student import for particular batch.
     * required parameter is batch_id
     */
    public function importStudent(ImportStudentRequest $request, $batchId)
    {
//      Checking batch id exists or not . not able to use in request file coz batch id getting through url.
        if (!Batch::whereId($batchId)->exists()) {
            return SupportingFunctions::error('Batch with given id does not exists.');
        }

        if ($request->has('file')) {
//          Using matweb Excel package for import
            $data = Excel::toCollection(new StudentsImport(), $request->file('file'));

//          Below foreach loop is used for validating each row before importing the file so here name is required               and dob and gender is nullable but having vaalidation for dob that should be date                                   gender should be alpha.
            foreach ($data[0] as $key => $value) {
                $validator = Validator::make($value->toArray(), [
                    'name' => 'required|string',
                    'dob' => 'nullable|date_format:Y-m-d',
                    'gender' => 'nullable|alpha',

                ]);
                if ($validator->fails()) {
                    return response()->json(['error'=>['message'=>$validator->errors()], 'status_code' => 500, 'extra_msg' => 'You have an error in row no '.($key+2) . ' in the sheet.']);
                }
            }

//          if no error came in during above foreach loop then data will import in student table.
            foreach ($data[0] as $value) {
                Student::create([
                    'name' => $value['name'],
                    'batch_id' => $batchId,
                    'dob' => $value['dob'],
                    'gender' => $value['gender'],
                ]);
            }
            return SupportingFunctions::success('Student details imported successfully.');
        }
        return SupportingFunctions::error('Error in file.');
    }

    /**
     * Student list for given batch
     * 1. Batch exists or not
     * 2. students exists or not
     * 3. send students list
     */
    public function index($batch_id)
    {
        if (!Batch::whereId($batch_id)->exists()) {
            return SupportingFunctions::success('Batch doesnot exists!');
        } else {
            $list = Student::where('batch_id', $batch_id)->get(['id', 'gender', 'name' , 'dob', 'slug']);
            $batch = Batch::whereId($batch_id)->get(['id','batch','cluster']);
            if (count($list)) {
                return response()->json(['data' => $list, 'status' => 200, 'batch' => $batch]);            
            } else {
                return response()->json(['data' => [], 'status' => 200, 'batch' => $batch, 'message' => 'No students found for given batch']);
            //    return SupportingFunctions::success('No students found for given batch');
            }            
        }
        
    }

    public function attendenceDate($batchId)
    {
        return $attendence = Attendance::where('batch_id',$batchId)->distinct('entered_date')->orderBy('entered_date','DESC')->limit(7)->pluck('entered_date');
    }

    //This function is used for taking all the event date from calendar for showing dropdown.
    public function attendenceCalendarDate($batchId)
    {
        return $attendence = Calendar::whereCoordinatorId(auth()->user()->id)
            ->where('scheduled_date','<=',Carbon::now()->toDateString())
            ->whereBatchId($batchId)
            ->orderBy('scheduled_date','DESC')
            ->limit(7)
            ->pluck('scheduled_date');
    }
}
