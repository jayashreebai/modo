<?php

namespace App\Http\Controllers\School;

use App\Models\School\SchoolCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Announcement;
use App\Models\School\School;
use App\Http\Controllers\Controller;
use App\Models\School\SchoolAnnouncement;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\AddAnnouncementRequest;
use App\Http\Requests\School\UpdateAnnouncementRequest;
use App\Transformers\SchoolManagement\AnnouncementTransformer;
use Illuminate\Support\Facades\URL;

class AnnouncementController extends Controller
{
    /**
     *
     * This AnnouncementController will have functions related to
     * announcements corresponding to announcement and schools.
     *
    */

    /**
     * Announcement Controller constructor.
    */

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Store Announcement API
     *
     * 1. Create announcement for school
     *
     * @param  \Illuminate\Http\AddAnnouncementRequest  $request, $school
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(AddAnnouncementRequest $request)
    {
        $schoolIds = json_decode($request->get('schoolIds'));
        $host = URL::to('/storage');
        $tag = '';
        if ($request->type == 'image') {
            $folder = '/schools/announcement/images';
            $imageName = (string)md5(time() . rand());
            $storagePath = SupportingFunctions::addImage($request, $folder, 'file');
            $url = $host.$storagePath;
            $tag = "<div style='height:200px;background-position: center;background-size: cover; background-image: url($url)'/>";
        } else if($request->type == 'video') {
            $video=$request->file('file');
            $input = time().$video->getClientOriginalName();
            $destinationPath = 'storage/schools/announcement/videos';
            $video->move($destinationPath, $input);
            $url = $host.'/schools/announcement/videos/'.$input;
            $tag = "<video style='width: 100%; height: 200px;' id=\"video-player\" autoplay preload='none' controls><source src= $url type='video/mp4'/></video>";

        }

        $announcement = Announcement::create([
          'title' => $request->get('title'),
          'description' => $request->get('description'),
          'url' => $tag
        ]);

        foreach ($schoolIds as $id){
            SchoolAnnouncement::create(['school_id' => $id,
                'announcement_id' => $announcement->id,
                'date' => $request->get('date')
            ]);

        }
        return SupportingFunctions::success('Announcement posted successfully!');
    }

    /**
    * Update Announcement API
    *
    * 1. Update announcement for given announcement_id
    *
    * @param  \Illuminate\Http\UpdateAnnouncementRequest  $request, $announcement
    * @return \Illuminate\Http\JsonResponse
    */
    public function update(UpdateAnnouncementRequest $request, Announcement $announcement)
    {
        $schoolIds = json_decode($request->get('schoolIds'));
//        $schoolIds = [1];
        if ($request->hasFile('file')){
            $host = URL::to('/storage');
            if ($request->type == 'image') {
                $folder = '/schools/announcement/images';
                $imageName = (string)md5(time() . rand());
                $storagePath = SupportingFunctions::addImage($request, $folder, 'file');
                $url = $host.$storagePath;
                $tag = "<div style='height:200px;background-position: center;background-size: cover; background-image: url($url)'/>";
            } elseif($request->type == 'video') {
                $video=$request->file('file');
                $input = time().$video->getClientOriginalName();
                $destinationPath = 'storage/schools/announcement/videos';
                $video->move($destinationPath, $input);
                $url = $host.'/schools/announcement/videos/'.$input;
                $tag = "<video style='width: 100%; height: 200px;' id=\"video-player\" autoplay preload='metadata' controls><source src= $url type='video/mp4'/></video>";

            }
        }
        else{
            $tag = $announcement->url;
        }
        $announcement->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'url' => $tag
        ]);
        SchoolAnnouncement::where('announcement_id',$announcement->id)->delete();
        foreach ($schoolIds as $id){
            SchoolAnnouncement::create([
                'school_id' => $id,
                'announcement_id' => $announcement->id,
                'date' => $request->get('date')
            ]);

        }
        return SupportingFunctions::success($announcement->title.' updated successfully!');
    }

    /**
    * Delete Announcement API
    *
    * 1. Delete announcement for given announcement_id
    *
    * @param  \Illuminate\Http\ $announcement
    * @return \Illuminate\Http\JsonResponse
    */
    public function destroy(Announcement $announcement)
    {
        $announcement->delete();
        return SupportingFunctions::success($announcement->title.' deleted successfully!');
    }

    /**
     * View announcement for modoAdmin API
     *
     * @param  \Illuminate\Http\ $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $announcements = Announcement::all();
            return SupportingFunctions::transform($announcements, new AnnouncementTransformer);
    }

    /**
     * View announcements for schoolAdmin API
     *
     * @param  \Illuminate\Http\ $request, $school
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function announcementsForSchool(School $schoolcourse)
    {
//        if (count($schoolcourse->announcements)>0) {
            $announcement = $schoolcourse->announcements->where('pivot.date','<=',Carbon::now()->toDateString());
            return SupportingFunctions::transform($announcement, new AnnouncementTransformer);
//        } else {
//            return SupportingFunctions::error('Announcements doesnot exists');
//        }
    }

    public function getAnnouncement(Announcement $announcement)
    {
       return [
            "id" => $announcement->id ,
            "title" => $announcement->title ,
            "slug" => $announcement->slug ,
            "description" => $announcement->description ,
            "url" => $announcement->url ,
            "school" => $announcement->school->pluck('id'),
            "date" => count($announcement->school) > 0 ? $announcement->school[0]->pivot->date : null
       ];
    }
}
