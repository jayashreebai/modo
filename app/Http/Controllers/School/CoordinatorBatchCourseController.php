<?php

namespace App\Http\Controllers\School;

use App\Http\Controllers\SupportingFunctions;
use App\Models\Course\Module;
use App\Models\School\Batch;
use App\Models\School\CourseContent;
use App\Models\School\CourseProgress;
use App\Models\School\LessonProgress;
use App\Models\School\SchoolCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoordinatorBatchCourseController extends Controller
{

    /**
     *
     * This Controller will have functions related to
     * coordinator login and taking the batch class.
     *
     */

    /**
     * Coordinator Controller constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * @param Request $request
     * @return array
     * this function is used for show the list of bach with unlimited course also.
     */
    public function coordinatorBatchlist(Request $request)
    {
         $user = auth()->user();
         //if school engagement type is by class or by student.
         $batches = Batch::whereHas('course',function ($q){
             $q->where('expiry_date->from' , '<=', Carbon::now()->addDay()->toDateString())
               ->where('expiry_date->to', '>=', Carbon::now()->toDateString())
               ->whereHas('school',function ($qu){
                   $qu->where('active',1);
               });
         })->where('coordinator_id', $user->id)->get();


        $batch_list_course = $batches->map(function ($item) use ($request){

            //Fetchng the next lesson for the coordinator to go to lesson page directly.
            $activated_module_id = json_decode(Batch::find($item->id)->active_module_id, true);
            $activatedModuleId = [];
            foreach ($activated_module_id as $key => $activated_module){
                $key_string = (string)$key+1;
                $activated_module['module_'.$key_string] = array_push($activatedModuleId, $activated_module['module_'.$key_string]);
            }
            $completed_course = CourseProgress::where('batch_id',$item->id)->pluck('module_id')->toArray();
            $module_id = null;
            foreach ($activatedModuleId as $moduleId){
                if(in_array($moduleId, $completed_course)){
                    continue;
                }else{
                    $module_id = $moduleId;
                    break;
                }
            }
            if(!empty($module_id)){
                $courseContent = CourseContent::whereModuleId($module_id)->whereSchoolCourseId($item->school_course_id)->first();
                $nextLessonContent = $this->fetchingNextLesson($courseContent, $item->school_course_id ,$item->id, $module_id);
                $nextLessonId = $nextLessonContent['id'];
                $nextLessonName = $nextLessonContent['lesson_name'];
                $cid = $courseContent->id;
            }
            else{
                $nextLessonId = null;
                $nextLessonName = null;
                $cid = null;
            }
            $lastLessonTakenDate = LessonProgress::whereBatchId($item->id)->orderBy('id','DESC')->first();
            return[
                'NextlessonId' => $nextLessonId ,
                'batchId' => $item->id ,
                'batchName' => $item->batch ,
                'clusterName' => $item->cluster ,
                'completed' => $item->completed ,
                'courseContentId' =>$cid,
                'courseName' => $item->course->course->course,
                'last_updated_time' => isset($lastLessonTakenDate) ? Carbon::parse($lastLessonTakenDate->created_at)->format('d-m-Y') : null ,
                'module_id' => $module_id ,
                'nextLessonName' => $nextLessonName ,
                'schoolCourseId' => $item->school_course_id ,
                'engagementType' => $item->course->school->engagement_type_id ,
                'schoolId' => $item->course->school->id ,
                'schoolName' => $item->course->school->school ,
                'schoolslug' => $item->course->school->slug ,
            ];
        });

        // if school engagement type is unlimited.

        $school = $user->school->where('engagement_type_id' ,1)->pluck('id');
        if(!empty($school)){
            $courses =  SchoolCourse::whereIn('school_id', $school)->get();
            $unlimited_course = $courses->map(function ($item) use ($user){
                return[
                    'NextlessonId' => null ,
                    'batchId' => null ,
                    'batchName' => null ,
                    'clusterName' => null ,
                    'completed' => null ,
                    'courseContentId' =>null,
                    'courseName' => $item->course->course,
                    'last_updated_time' => null ,
                    'module_id' => null ,
                    'nextLessonName' => null ,
                    'schoolCourseId' => $item->id ,
                    'engagementType' => 1 ,
                    'schoolId' => $user->school->where('engagement_type_id' ,1)->first()->id,
                    'schoolName' => $user->school->where('engagement_type_id' ,1)->first()->school,
                    'schoolslug' => $user->school->where('engagement_type_id' ,1)->first()->slug ,

                ];
            });
            return array_merge($batch_list_course->toArray(),$unlimited_course->toArray());
        }

        return $batch_list_course->toArray();
    }

    /**
     * @param Request $request
     * @return mixed
     * This function is used for show the list of module for a batch
     */
    public function schoolCourseContent(Request $request)
    {
        $schoolCourseId = $request->get('school_course_id');
        $batchId = $request->get('batch_id');
        $completionDateData = $this->lessonWithCompletionDate($batchId);
//      Getting all course content for the school course id
        $schoolCourseContent = CourseContent::whereActive(1)->whereSchoolCourseId($schoolCourseId)->orderBY('order')->get();

//      Taking the completed module id for the batch from course progress table
        $completed_module_id = CourseProgress::whereBatchId($batchId)->pluck('module_id')->toArray();

//      Taking the activated module id for the batch from batch table
        $activated_module_id = json_decode(Batch::find($batchId)->active_module_id, true);
        $activatedModuleId = [];
        foreach ($activated_module_id as $key => $activated_module){
            $key_string = (string)$key+1;
            $activated_module['module_'.$key_string] = array_push($activatedModuleId, $activated_module['module_'.$key_string]);
        }

//      Using map function on schoolCourseContent object to modified and validate the module content        and pass the desired key for frontend.
        return $schoolCourseContent->map(function ($item) use($completed_module_id, $activatedModuleId, $batchId,$completionDateData){

            //Calling the function for lesson modification.
            $lessonDetails = $this->gettingSortedLesson($item, $batchId);

            //Checking the module is completed or not by that updating the completed to true or false
            in_array($item->module_id, $completed_module_id) ? $completed = True : $completed = False;

            //Checking the module is unlock or not for today from batch table and updating the unlock to true or false.
            in_array($item->module_id, $activatedModuleId) ? $unlock = True : $unlock = False;

            //getting expire date of the module
            $date = SupportingFunctions::singleValue('module',$item->module_id, $completionDateData);
            return [
                    'id' => $item->module_id ,
                    'name' => $item->module->module ,
                    'courseContentId' => $item->id ,
                    'order' => $item->order ,
                    'unlock' => $unlock ,
                    'completed' => $completed ,
                    'lessons' => $lessonDetails,
                    'next_date' => Carbon::parse($date['date'])->format('d-M-Y')
                ];
        });
    }

    /**
     * @param $item
     * @return mixed
     * Returning function with the logic of completed, active, and also in sorted format.
     */
    public function gettingSortedLesson($lessonsContent, $batchId)
    {
        //Doing json decoding for module content and
        $moduleContent = json_decode($lessonsContent->module_content, true);

        //sorting the upcoming lesson based on order.
        usort($moduleContent, function ($object1, $object2) {
            return strcmp($object1['order'], $object2['order']);
        });

        //Making as collection and then filtering the lessons where active = 1 .
        $lessons = collect($moduleContent)->filter(function ($item){
            return $item['active'] == 1;
        })->values();

        // Againg using map function on lessons for adding some extra key and also validating whether it is completed or not or active.
        return $lessons->map(function ($content) use ($batchId, $lessonsContent){
            // Checking Lesson is completed or not from lesson progress table and updating completed as true or false.
            LessonProgress::whereBatchId($batchId)->whereCourseContentId($lessonsContent->id)->whereLessonHashId($content['id'])->exists() ? $completed = True : $completed = False;

            return[
                'id' => $content['id'] ,
                'slug' => $content['slug'] ,
                'name' => $content['lesson_name'] ,
                'order' => $content['order'] ,
                'quiz_count' => count($content['quiz']) ,
                'completed' => $completed ,
            ];

        });

    }

    /**
     * @param Request $request
     * @return \App\Http\Controllers\jsonResponse|\Illuminate\Http\JsonResponse
     * This function is used for fetching the lesson detail for the given id and also it will indicate completed
     * as true or false for given batch and course_content_id.
     */
    public function lessonContentForGivenHashId(Request $request)
    {
        $lesson_id = $request->get('lesson_id');
        $course_content_id = $request->get('course_content_id');
        $batchId = $request->get('batch_id');

        //Checking this lesson is completed or not from lesson progress and updating completed as true or false
        LessonProgress::where(['lesson_hash_id' => $lesson_id, 'course_content_id' => $course_content_id, 'batch_id' => $batchId])->exists() ? $completed = True : $completed = False;

        $course_content = CourseContent::whereId($course_content_id)->first();
        $lesson_content = json_decode($course_content->module_content, true);

        //Sending the json data to sungle value function for fetching the object  for given key and value. it works like where conditions.
        $response = SupportingFunctions::singleValue('id',$lesson_id ,$lesson_content);

        if($response)
        {
            $object = new \stdClass();
            $object->video_url = $response['video'];
            $object->quiz = $response['quiz'];
            $object->school_course_id = $course_content->school_course_id;
            $object->module_id = $course_content->module_id;
            $object->lesson_name = $response['lesson_name'];
            $object->completed = isset($batchId)?$completed:False;
            return response()->json(['data' => collect($object), 'status' => 200]);

        }
        return SupportingFunctions::success('No lesson found!');

    }

    public function completeLesson(Request $request, $lesson_id)
    {
        $scid = $request->get('school_course_id');
        $ccid = $request->get('course_content_id');
        $module_id = $request->get('module_id');
        $batch_id = $request->get('batch_id');

        if(isset($batch_id)) {
            $completionDateData = $this->lessonWithCompletionDate($batch_id);
            $date = SupportingFunctions::singleValue('module', $module_id, $completionDateData);
            if (Carbon::parse($date['date'])->gt(Carbon::now())) {
                return response()->json(['error' => 'Sorry you can not mark this module as complete.'], 433);
            }
        }

//      Adding upcoming data to lesson progress table for marking as complete.
        LessonProgress::create(['school_course_id' => $scid,
            'course_content_id' => $ccid,
            'batch_id' => $batch_id,
            'module_id' => $module_id,
            'lesson_hash_id' => $lesson_id,
            'entered_date' => Carbon::now()->format('y-m-d'),
            'updated_by' => auth()->user()->id]);

        $courseContent = CourseContent::whereSchoolCourseId($scid)->whereModuleId($module_id)->first();
        if(isset($batch_id)){
            // for engagement type by class or student.
            $moduleComplete = false;
            $nextLesson = $this->fetchingNextLesson($courseContent, $scid ,$batch_id, $module_id);
            if(empty($nextLesson)){
                $moduleComplete = $this->markModuleComplete($courseContent, $batch_id, $scid);
            }
            return response(['batchCompleted' => $moduleComplete, 'message' => 'Lesson completed', 'nextLessonId' => $nextLesson['id']]);
        }
        else{
            //for unlimited access.
            $nextLesson = $this->fetchingNextLessonForUnlimite($courseContent, $lesson_id);
            return response(['batchCompleted' => False, 'message' => 'Lesson completed', 'nextLessonId' => $nextLesson]);
        }
    }

    public function lessonWithCompletionDate($batch_id)
    {
        $batch = Batch::find($batch_id);
        if($batch){
            $calendars = $batch->calendars->sortBy('scheduled_date')->values()->toArray();
            $contents = array_chunk($batch->course->contents->sortBy('order')->toArray(),$batch->modules_per_day);

            $d = array_map(function($i,$j){
                $data = [];
                foreach ($i as $content){
                    $d['module'] = $content['module_id'];
                    $d['name'] = Module::find($content['module_id'])->module;
                    $d['date'] = $j['scheduled_date'];
                    array_push($data,$d);
                }
                return $data;

            },$contents, $calendars
            );

            $newArray = array_reduce($d, function ($carry, $item) {
                return array_merge($carry, $item);
            }, []);
            return $newArray;
        }
        return [];

    }

    public function fetchingNextLesson($courseContent, $scid ,$batch_id, $module_id)
    {
        $lesson_contents = json_decode($courseContent->module_content, true);
        $completed_lesson = LessonProgress::where(['school_course_id' => $scid, 'batch_id' => $batch_id, 'module_id' => $module_id])->pluck('lesson_hash_id')->toArray();

        //sorting the upcoming lesson based on order.
        usort($lesson_contents, function ($object1, $object2) {
            return strcmp($object1['order'], $object2['order']);
        });

        $lesson_contents = collect($lesson_contents)->filter(function ($item){
            return $item['active'] == 1;
        })->values();

        $nextLessonId = null;
        foreach ($lesson_contents as $lesson_content)
        {
            if(in_array($lesson_content['id'],$completed_lesson)){
                continue;
            }else{
                $nextLessonId = $lesson_content;
                break;
            }
        }
        return $nextLessonId;
    }

    public function fetchingNextLessonForUnlimite($courseContent, $lesson_id)
    {
        $lesson_contents = json_decode($courseContent->module_content, true);

        //sorting the upcoming lesson based on order.
        usort($lesson_contents, function ($object1, $object2) {
            return strcmp($object1['order'], $object2['order']);
        });

        $lesson_contents = collect($lesson_contents)->filter(function ($item){
            return $item['active'] == 1;
        })->values();

        $nextLessonId = null;
        $index = null;
        foreach ($lesson_contents as $key => $lesson_content)
        {
            if($lesson_content['id'] == $lesson_id){
                $index = $key;
                break;
            }
        }
        if($index !== null){
            if(count(collect($lesson_contents)) > ($index+1))
            {
                return $lesson_contents[$index+1]['id'];
            }
        }

        return $nextLessonId;
    }

    public function markModuleComplete($courseContent, $batch_id, $scid)
    {
        $courseProgress = new \stdClass();
        $courseProgress->module_id = $courseContent->module_id;
        $courseProgress->lessons = $courseContent->module_content;

        CourseProgress::create([
            'module_completed' => json_encode($courseProgress),
            'batch_id' => $batch_id,
            'entered_date' => Carbon::now()->toDateString(),
            'completed_by' => auth()->user()->id,
            'course_content_id' => $courseContent->id,
            'module_id' => $courseContent->module_id,
            'school_course_id' => $scid
        ]);

        $moduleCount = CourseContent::where('school_course_id', $courseContent->school_course_id)->count();
        $completedModuleCount = CourseProgress::whereBatchId($batch_id)->whereSchoolCourseId($scid)->count();

        if($moduleCount == $completedModuleCount){
            Batch::whereId($batch_id)->update(['completed' => 1, 'active_module_id' => json_encode([])]);
            return True;
        }else{
            return False;
        }

    }

    public function lessonList(Request $request)
    {
        $courseContentId = $request->get('course_content_id');
        $batchId = $request->get('batch_id');

        $courseContentDetails = CourseContent::where(['id' => $courseContentId])->first();
        $lessons = collect(json_decode($courseContentDetails->module_content, true));
        $completedLessons = LessonProgress::where(['course_content_id' => $courseContentDetails->id, 'batch_id' => $batchId])->pluck('lesson_hash_id')->toArray();

        $lessons = $lessons->filter(function ($item){
            return $item['active'] == 1;
        })->values();

        $data = $lessons->map(function ($item) use ($completedLessons) {
            if (in_array($item['id'], $completedLessons)) {
                $lessonCompleted = true;
            }
            else{
                $lessonCompleted = false;
            }
            return [
                'id' => $item['id'],
                'slug' => $item['slug'],
                'name' => $item['lesson_name'],
                'order' => $item['order'],
                'completed' => isset($batchId)? $lessonCompleted:False
            ];
        });

        return response()->json(['data' => $data, 'status' => 200]);
    }

    /**
     * List of module for unlimited school
     */

    public function unlimitedSchoolCourseContent(Request $request)
    {
        $schoolCourseId = $request->get('school_course_id');

//      Getting all course content for the school course id
        $schoolCourseContent = CourseContent::whereActive(1)->whereSchoolCourseId($schoolCourseId)->orderBY('order')->get();


//      Using map function on schoolCourseContent object to modified and validate the module content        and pass the desired key for frontend.
        return $schoolCourseContent->map(function ($item){

            //Calling the function for lesson modification.
            $lessonDetails = $this->gettingUnlimitedSortedLesson($item);

            return [
                'id' => $item->module_id ,
                'name' => $item->module->module ,
                'courseContentId' => $item->id ,
                'order' => $item->order ,
                'unlock' => True ,
                'completed' => False ,
                'lessons' => $lessonDetails,
            ];
        });
    }

    /**
     * @param $item
     * @return mixed
     * Returning function with the logic of completed, active, and also in sorted format.
     */
    public function gettingUnlimitedSortedLesson($lessonsContent)
    {
        //Doing json decoding for module content and
        $moduleContent = json_decode($lessonsContent->module_content, true);

        //sorting the upcoming lesson based on order.
        usort($moduleContent, function ($object1, $object2) {
            return strcmp($object1['order'], $object2['order']);
        });

        //Making as collection and then filtering the lessons where active = 1 .
        $lessons = collect($moduleContent)->filter(function ($item){
            return $item['active'] == 1;
        })->values();

        // Againg using map function on lessons for adding some extra key and also validating whether it is completed or not or active.
        return $lessons->map(function ($content) {

            return[
                'id' => $content['id'] ,
                'slug' => $content['slug'] ,
                'name' => $content['lesson_name'] ,
                'order' => $content['order'] ,
                'quiz_count' => count($content['quiz']) ,
                'completed' => False ,
            ];

        });

    }



}
