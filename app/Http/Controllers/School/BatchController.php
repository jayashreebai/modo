<?php

namespace App\Http\Controllers\School;


use Image;
use App\DataSerializer;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Storage;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Carbon\Carbon;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Models\School\Batch;
use App\Models\School\School;
use App\Models\School\Calendar;
use App\Models\School\SchoolCourse;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\AddBatchRequest;
use App\Http\Requests\School\UpdateBatchRequest;
use App\Transformers\SchoolManagement\BatchListTransformer;
use App\Transformers\SchoolManagement\CourseWithModuleForBatchTransformer; 

class BatchController extends Controller
{
    /**
     * This controller has services related to Batches of school
     */

    /**
    * Batch Controller constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Add Batch API
     *
     * 1. Fetch school courses for given school and course
     * 2. Given post data is for adding batches
     * 3. batch, cluster, schoolId
     *
     * @param  \Illuminate\Http\AddBatchRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(AddBatchRequest $request, School $school)
    {

        $schoolCourseId = SchoolCourse::whereId($request->course_id)->value('id');
        if (!empty($schoolCourseId)) {
            if($request->modules_per_day < 1){
                return SupportingFunctions::error('Module per day is not ');
            }
            if (Batch::where(['cluster' => $request->cluster, 'batch' => $request->batch, 'school_course_id' => $schoolCourseId])->exists()) {
               return SupportingFunctions::error('This course is already assigned to this Batch and cluster');
            } else {

                $batch = Batch::create(['cluster' => $request->cluster_name,
                'batch' => $request->batch_name,
                'coordinator_id' => $request->coordinator_id,
                'school_course_id' => $schoolCourseId ,
                'modules_per_day' =>  $request->modules_per_day,
                'active_module_id' => json_encode([])
                ]);
                return response()->json(['message' => 'Batch added successfully!', 'id' => $batch->id, 'status' => 200]);

            }            
        }
        return SupportingFunctions::error('Given course is not assigned to school!');
    }

    /**
    * Update Batch API
    *
    * 1. for given batch update all fields except course
    * 2. if coordinator is changed then change everywhere like calendar etc.
    *
    * @param  \Illuminate\Http\UpdateBatchRequest $request
    * @return \Illuminate\Http\JsonResponse 
    *
    */
    public function update(UpdateBatchRequest $request, $batch)
    {
        $batch = Batch::whereId($batch)->first();
        if ($request->coordinator_id == $batch->coordinator_id) {
            $batch->update(['coordinator_id' => $request->coordinator_id, 'modules_per_day' => $request->modules_per_day]);
            return SupportingFunctions::success('Batch updated successfully!');
        }
        $batch->update(['coordinator_id' => $request->coordinator_id, 'modules_per_day' => $request->modules_per_day]);
        Calendar::where('batch_id', $batch->id)->update(['coordinator_id' => $request->coordinator_id, 'updated_by' => auth()->user()->id]);
        return SupportingFunctions::success('Batch updated successfully!');
    }

    /**
     * List of batches for given school
     * 
    * @param  \Illuminate\Http\$school
    * @return \Illuminate\Http\JsonResponse
     * 
     */ 
    public function index(School $school)
    {
        $batches = array();
        $batches_new = array();
        $sc =  $school->courses;
        
        foreach ($sc as $key => $value) { 
            $date = json_decode($value->expiry_date, true);
            if ($date['to'] >= Carbon::now()->format('Y-m-d')) {              
              array_push($batches,$value->id);
            }            
        }
        if(!empty($batches))
        {
            $batch = Batch::whereIn('school_course_id',$batches)->whereNull('deleted_at')->get();
            if (!empty($batch)) {
                return SupportingFunctions::transform($batch, new BatchListTransformer);
            }            
        }
        return SupportingFunctions::success('Batches not found for given school');        
    }

    /**
     * Batch delete 
     * 
    * @param  \Illuminate\Http\$batch
    * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($batch)
    {
        if (!Batch::whereId($batch)->exists()) {
           return SupportingFunctions::success('Batch doesnot exists!');
        }
        $data = Batch::whereId($batch)->first();
        if (count(json_decode($data->active_module_id,true)) > 0) {
            return SupportingFunctions::success('Batch cannot be deleted, since classes are started!');
        } else {
            $data->delete();
            return SupportingFunctions::success('Batch deleted successfully!');
        }        
    }
}
