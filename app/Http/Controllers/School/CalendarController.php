<?php

namespace App\Http\Controllers\School; 

use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\Calander\CreateCalendar;
use App\Http\Requests\School\Calander\UpdateCalendar;
use App\Jobs\EventNotifyToUser;
use App\Jobs\SendingEventNotification;
use App\Jobs\SendMailForCalendar;
use App\Models\School\Batch;
use App\Models\School\Calendar;
use App\Models\User;
use App\Transformers\Coordinator\CalendarDetail;
use Carbon\Carbon;

use App\DataSerializer;

use Illuminate\Support\Facades\DB;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\SchoolManagement\BatchDetailTransformer;

class CalendarController extends Controller
{
    /**
     * CourseController constructor.
     */
    public function __construct() 
    {
        $this->middleware('jwt.auth')->except('notify','getEventNotification');
    }

    /**
     * @param CreateCalendar $request
     * This function is used for create calender
     */
    public function store(Request $request, $batchId)
    {
        $batch = Batch::whereId($batchId)->first();
        $course_expiry = json_decode($batch->course->expiry_date,true);

//        if(Carbon::parse($request->get('date'))->lte(Carbon::parse($course_expiry['from'])))
//        {
//            return response()->json(['error' => 'Sorry you can not create the event in the given date, because course will start from '. $course_expiry['from']],422);
//        }

        if (!Carbon::parse($request->get('date'))->between(Carbon::parse($course_expiry['from'])->addDay(),Carbon::parse($course_expiry['to'])))
        {
            return response()->json(['error' => 'Sorry you can not create the event in the given date, because course duration is from '. $course_expiry['from'].' to '.$course_expiry['to'].'. So please choose date between this.'],422);
        }

        $calendar = Calendar::whereBatchId($batch->id)->whereCoordinatorId($batch->coordinator_id)->whereScheduledDate($request->get('date'));
        //checking if calendar is already created for the date or not
        if($calendar->exists()){
            return response()->json(['error' => 'Calendar already created for the given date!'],422);
        }
        //=============  Checking the condition for no of event in calrndar should not exceed the no. of module take per day in the class.===================
        $modulePerDay = $batch->modules_per_day;
        $moduleCount = $batch->course->contents->where('active',1)->count();
        $count = (int)($moduleCount / $modulePerDay);
        if($moduleCount % $modulePerDay != 0)
        {
            $count += 1;
        }
        $calendarCOunt = Calendar::whereBatchId($batch->id)->whereCoordinatorId($batch->coordinator_id)->count();
        if($calendarCOunt == $count){
            return response()->json(['error' => 'You already created the enough no. of event for taking the class!'],422);

        }
        //============================= End Checking=============================
        $calendar = Calendar::create([
            'coordinator_id'=> $batch->coordinator_id,
            'batch_id'=> $batchId,
            'scheduled_date'=> $request->get('date'),
            'scheduled_time'=> $request->get('time'),
            'created_by'=> auth()->user()->id,
            'updated_by'=> auth()->user()->id
        ]);
//      Sending mail notification.
//        sending 1 as a third param for denoting it is from create calender
        dispatch(new SendingEventNotification($calendar, $batch, 1));


        return SupportingFunctions::success('Calendar create successfully!');

    }

    /**
     * @param CreateCalendar $request
     * This function is used for update calender
     */
    public function update(UpdateCalendar $request, Calendar $calendar)
    {
        $batch = Batch::whereId($calendar->batch_id)->first();
        $course_expiry = json_decode($batch->course->expiry_date,true);

        if (!Carbon::parse($request->get('date'))->between(Carbon::parse($course_expiry['from']),Carbon::parse($course_expiry['to'])))
        {
            return response()->json(['error' => 'Sorry you can not create the event in the given date, because course duration is from '. $course_expiry['from'].' to '.$course_expiry['to'].'. So please choose date between this.'],422);

        }

        $calendar->update([
            'scheduled_date'=> $request->get('date'),
            'scheduled_time'=> $request->get('time'),
            'updated_by'=> auth()->user()->id
        ]);

        return SupportingFunctions::success('Calendar updated successfully!');
    }


    /**
     * @param CreateCalendar $request
     * This function is used for update calender
     */
    public function destroy(Calendar $calendar)
    {
        if($calendar->scheduled_date < Carbon::now()->addDays(2)->toDateString()){
            return SupportingFunctions::error('Sorry you can not delete this event!');
        }
//      Sending mail notification.
//      sending 2 as a third param for denoting it is from create calender
        $batch = Batch::find($calendar->batch_id);
        dispatch(new SendingEventNotification($calendar, $batch, 2));

        $calendar->delete();
        return SupportingFunctions::success('Calendar deleted successfully!');
    }

    /**
     * @param CreateCalendar $request
     * This function is used for getting calendar detail for batch
     */
    public function getCalenderForBatch($batch_id)
    {
        $calendar = Calendar::where('batch_id', $batch_id)->orderBy('scheduled_date')->get();

        $manager = new \League\Fractal\Manager();
        $manager->setSerializer(new DataSerializer());
        $result = new Collection($calendar, new CalendarDetail($batch_id));
        $data = $manager->createData($result)->toArray();
        $batchData = Batch::whereId($batch_id)->get(['id','batch','cluster']);

        return response()->json(['data' => $data , 'batch' => $batchData]);
    }

    /**
     * @param CreateCalendar $request
     * This function is used for getting calendar detail for batch and particular coordinator
     */
    public function getCalenderForBatchAndCoordinator(Batch $batch, $coordinatorId)
    {
        $calendar = Calendar::where(['batch_id' => $batch->id, 'coordinator_id' => $coordinatorId])->get();
        return SupportingFunctions::transform($calendar, new CalendarDetail());
    }


    /**
     * This function is used for cron job
     * in this you have to take tommorow date and checked in callender table and fecth batch id
     * After that from that batch id you have to update the next active module
     */

    public function notify()
    {

//      fetching all the record from calendar table which date is tomorrow date
        $calendars = Calendar::where('scheduled_date', Carbon::now()->addDay()->toDateString())->get();
//        if($calendars){
        //      Using loop for performing action one by one
        foreach ($calendars as $calendar) {
            $batch = $calendar->batch;  // getting batch detail from relation of calendar (belong to)
                $courseProgresses = $calendar->batch->courseProgress; //getting course progress from relation

            $moduleCompletedId = [];
//          using loop for getting the completed module id and store it $moduleCompletedId variable                 because pluck is not working on collection json.
            foreach ($courseProgresses as $courseProgress) {
                $moduleContent = json_decode($courseProgress->module_completed, true);
                array_push($moduleCompletedId, $moduleContent['module_id']);
            }

//          getting detail of module should be completed per day
            $modulesPerDay = $calendar->batch->modules_per_day;
//          ===============Checking if he completed the existing activated
//                  module on the prevous day or not if not then append it to next
//                  day .====================

            $activated_module_id = json_decode($calendar->batch->active_module_id, true);
            $activatedModuleId = [];
            foreach ($activated_module_id as $key => $activated_module){
                $key_string = (string)$key+1;
                array_push($activatedModuleId, $activated_module['module_'.$key_string]);
            }
            $actualNotCompletedId = [];
            foreach ($activatedModuleId as $id){
                if(!in_array($id,$moduleCompletedId)){
                    array_push($actualNotCompletedId,$id);
                }
            }
           $actualCompletedModuleId = array_unique(array_merge($actualNotCompletedId,$moduleCompletedId));
//          ===================================
//          fetching next module to be complete using exclude $moduleCompletedId from course content                table and also order by order and take only $modulesPerDay then pluck only module id.
            $nextCourseContentModuleId = $calendar->batch->course->contents->whereNotIn('module_id', $actualCompletedModuleId)->sortBy('order')->take($modulesPerDay)->pluck('module_id');

            $nextModule = [];
//          getting module id from $nextCourseContentModuleId and converting as object like
//              {
//                  "module_1" : 1,
//                  "module_2" : 2,
//                  .
//                  .
//                  .
//              }
            $count = count($actualNotCompletedId);
            if ($count > 0){
                foreach ($actualNotCompletedId as $key => $id){
                    $var = 'module_'.($key+1);
                    $moduleContent = new \stdClass();
                    $moduleContent->$var = $id;
                    array_push($nextModule, $moduleContent);
                }
                foreach ($nextCourseContentModuleId as $key => $moduleId) {
                    $var = 'module_'.($key+1+$count);
                    $moduleContent = new \stdClass();
                    $moduleContent->$var = $moduleId;
                    array_push($nextModule, $moduleContent);
                }
            }else{
                foreach ($nextCourseContentModuleId as $key => $moduleId) {
                    $var = 'module_'.($key+1);
                    $moduleContent = new \stdClass();
                    $moduleContent->$var = $moduleId;
                    array_push($nextModule, $moduleContent);
                }
            }
//          updating active_module_id in batch table from $nextModule json
            $batch->update([
                    'active_module_id' => json_encode($nextModule),
                ]);

//          getting coordinator detail for sending mail for upcoming module activated....

            $coordinator = $calendar->coordinator; 
            dispatch(new SendMailForCalendar($coordinator));
//            }
//        }else{
//            return "error";
        }
    }

    public function getEventNotification()
    {
        $data = DB::table('calendars')
            ->select('coordinator_id','batch_id',  DB::raw('min(scheduled_date) as date'))
            ->groupBy('coordinator_id','batch_id')
            ->where('scheduled_date', '>=', Carbon::now()->toDateString())
            ->get();

        $data->map(function ($item) {
            $batch = Batch::find($item->batch_id);
            $user = User::find($item->coordinator_id);

           $date = Carbon::parse($item->date)->diffInDays(Carbon::parse(Carbon::now()->toDateString()));

//            if ($date == 0){
//                $message = "You have a schedule today for the batch : $batch->batch , Cluster : $batch->cluster , please have look.";
//            }else{
//                $message = "You have left $date day to take the class for the batch : $batch->batch , Cluster : $batch->cluster ";
//            }

            dispatch(new EventNotifyToUser($user, $batch, $date));
        });



    }


}