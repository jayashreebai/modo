<?php

namespace App\Http\Controllers\School;

use Illuminate\Http\Request;
use App\Models\School\School;
use App\Models\School\Inventory;
use App\Models\InventoryCategory;
use App\Http\Controllers\Controller;
use App\Models\School\SchoolInventory;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\AssignInventoryRequest;
use App\Http\Requests\School\InventoryUpdateRequest;
use App\Http\Requests\School\InventoryUpdateSchoolRequest;
use App\Http\Requests\School\StoreInventoryCategoryRequest;
use App\Transformers\SchoolManagement\InventoryCategoryTransformer;
use App\Transformers\SchoolManagement\InventorySchoolTransformer;

class InventoryController extends Controller
{
    /**
     *
     * This Controller will have functions related to
     * inventory corresponding to iventory and schools.
     *
    */

    /**
     * CourseController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     *  Store Inventory API
     *
     *  @param  \Illuminate\Http\StoreInventoryCategoryRequest $request
     *  @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreInventoryCategoryRequest $request)
    {
        InventoryCategory::create($request->all());
        return SupportingFunctions::success($request->item_name.' added successfully!');
    }

    /**
     *  Update Inventory API
     *  Use store request only since same field will be updated always.
     *  For given Inventory category id , update with the given details.
     *
     *  @param  \Illuminate\Http\StoreInventoryCategoryRequest $request
     *  @return \Illuminate\Http\JsonResponse
     */
    public function update(StoreInventoryCategoryRequest $request, InventoryCategory $inventoryCategory)
    {
        $inventoryCategory->update($request->all());
        return SupportingFunctions::success($request->item_name.' updated successfully!');
    }

    /**
     *  View InventoryCategory Details
     *  Get all inventory category details.
     *
     *  @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $data = InventoryCategory::get();
        return SupportingFunctions::transform($data, new InventoryCategoryTransformer); 
    }

    /**
     * Destroy InventoryCategory Details.
     * For given inventory details.
     *
     * @param  \Illuminate\Http\InventoryCategory $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(InventoryCategory $inventoryCategory)
    {
        $inventoryCategory->delete();
        return SupportingFunctions::success('Inventory category deleted successfully!');
    }

    /**
     * Assign items to school
     * one at a time.
     * @param  \Illuminate\Http\AssignInventoryRequest $request, $school
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(AssignInventoryRequest $request, School $school)
    {
        if ($request->serial_number != null){
            if($request->quantity != 1){
                return response()->json(['error' => 'Quantity should be equal to 1, because the serial number you had provided that belongs to only one item.'], 433);
            }
        }
        Inventory::create(array_merge($request->all(), ['school_id' => $school->id]));
        return SupportingFunctions::success('Item assigned successfully to school!');
    }

    /**
    * UnAssign items from school
    * Softdelete a inventory for school
    *
    * @param  \Illuminate\Http\$inventory
    * @return \Illuminate\Http\JsonResponse
    */
    public function unAssign($inventory)
    {
        Inventory::where('id', $inventory)->delete();
        return SupportingFunctions::success('Item unassigned from school!');
    }

    /**
     * Daily updates for inventory by coordinator or modoadmin
     *
     * @param  \Illuminate\Http\InventoryUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function inventoryUpdate(InventoryUpdateRequest $request)
    {
        if (auth()->user()->role->id == 1 || auth()->user()->role->id == 3) {
            SchoolInventory::create(array_merge($request->all(), ['updated_by' => auth()->user()->id]));
            return SupportingFunctions::success('Inventory details updated successfully!');
        }
        return SupportingFunctions::error('You are not authorized to perfoem this action!');
    }

    /**
     * Get list of inventories assigned for school
     * 
     */
    public function inventoriesSchool(School $school)
    {
        return SupportingFunctions::transform($school->inventories, new InventorySchoolTransformer);

    }

    /**
     * Inventory update for the school
     * 
     */
    public function inventoryUpdateSchool(InventoryUpdateSchoolRequest $request, $inventory)
    {
        if(Inventory::where('id', $inventory)->exists())
        {
            if ($request->serial_number != null){
                if($request->quantity != 1){
                    return response()->json(['error' => 'Quantity should be equal to 1, because the serial number you had provided that belongs to only one item.'], 433);
                }
            }

            Inventory::where('id', $inventory)->update(['item_name' => $request->item_name,'quantity' => $request->quantity]);
            return SupportingFunctions::success('Item name updated successfully!');

        }
        return SupportingFunctions::success('Inventory does not exists!');

    }

}
