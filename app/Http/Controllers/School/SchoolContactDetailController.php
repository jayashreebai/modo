<?php

namespace App\Http\Controllers\School;

use App\DataSerializer;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\AddContactDetailRequest;
use App\Http\Requests\School\UpdateContactDetailRequest;
use App\Http\Requests\School\ContactHeymodoRequest;
use App\Models\School\Contact;
use App\Transformers\School\SchoolContactDetailTransformer;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;

use Illuminate\Support\Facades\Mail;
use App\Mail\ContactHeymodo;
use League\Fractal\Resource\Collection;

class SchoolContactDetailController extends Controller
{/**
     * Coordinator Controller constructor.
    */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddContactDetailRequest $request)
    {
        Contact::create([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'designation' => $request->get('designation'),
            'school_id' => $request->get('school_id'),
            'email' => $request->get('email'),
        ]);
        return SupportingFunctions::success('Contact added successfuly.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function update(UpdateContactDetailRequest $request, $contactId)
    {
        $contact = Contact::whereId($contactId)->first();
        $contact->update([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'designation' => $request->get('designation'),            
            'email' => $request->get('email')
        ]);
        return SupportingFunctions::success('Contact updated successfully.');
    }

    /**
     * Remove the specified resource from storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return SupportingFunctions::success('Contact deleted successfully.');
    }

    public function index($schoolId)
    {
        $contacts = Contact::where('school_id', $schoolId)->get();
        $manager = new Manager();
        $manager->setSerializer(new DataSerializer());
        $result = new Collection($contacts, new SchoolContactDetailTransformer());
        $data = $manager->createData($result)->toArray();
        return response()->json(['data' =>$data, 'status'=> 200]);
    } 

    /**
     * send email to contact
     * 
     * 
     */
    public function contactHeymodo(ContactHeymodoRequest $request)
    {   
        $user = auth()->user();
        $data = $request;
        Mail::to('support@heymodo.com')->send(new ContactHeymodo($user ,$data));
        return response()->json(['message' => 'Thank you for your feedback!', 'status' => 200]);
    }
    
}
