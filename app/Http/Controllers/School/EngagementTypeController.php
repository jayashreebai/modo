<?php

namespace App\Http\Controllers\School;

use App\DataSerializer;
use App\Models\EngagementType;
use App\Models\School\Batch;
use App\Models\School\School;
use App\Http\Controllers\Controller;
use League\Fractal\Resource\Collection;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\School\SchoolEngagementTypeRequest;
use App\Transformers\School\SchoolEngagementTypeTransformer;
use App\Transformers\MasterTransformer\EngagementTypeTransformer;


class EngagementTypeController extends Controller
{
    /**
     * Controller stores all services for Engagement-type tables.
     *
     */
    /**
     * Engagement Type list
     */
    public function index()
    {
        $engagementTypes = EngagementType::all();
        return SupportingFunctions::transform($engagementTypes, new EngagementTypeTransformer);
    }

    /**
     * engagement-type for school
     * 
     * if school is null from url then return master engagement-type
     * 
     * @param \Illuminate\Http\Request $school
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(School $school = null)
    {
        $engagementType = EngagementType::all();
        $manager = new \League\Fractal\Manager();
        $manager->setSerializer(new DataSerializer());
        if ($school) {
            $result = new Collection($engagementType, new SchoolEngagementTypeTransformer(['school_id' => $school->id]));
        } else {
            $result = new Collection($engagementType, new SchoolEngagementTypeTransformer());
        }
        
        $data = $manager->createData($result)->toArray();
        return response()->json(['data' => $data, 'status_code' => 200]);       
    }

    /**
     * Update engagement_type_id for school_id 
     * @param \Illuminate\Http\Request $school
     * @return \Illuminate\Http\JsonResponse
     * 
     */
    public function update(SchoolEngagementTypeRequest $request, School $school)
    {

        if(Batch::whereIn('school_course_id',$school->courses->pluck('id'))->exists())
        {
            return response()->json(['error' => 'Sorry, you can\'t change the engagement type because batch had already been created.'], 433);
        }
        $school->update(['engagement_type_id' => $request->engagement_type_id]);
        return SupportingFunctions::success('Engagement type updated successfully!');
    }

}
