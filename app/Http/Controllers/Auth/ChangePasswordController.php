<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Jobs\Password\SendPasswordChangedMail;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Change Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password change services.
    |
     */

    /**
     * ChangePasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Change password of the logged in user
     *
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request)
    {

        // If given password matches with auth password
        if (!(Hash::check($request->current_password, auth()->user()->password))) {
            return response()->json(['error' => 'Please enter Correct Old Password.'], 403);
        }

        // If current password and new password are same
        if (strcmp($request->current_password, $request->password) == 0) {
            return response()->json(['error' => 'New Password cannot be same as your current password. Please choose a different password.'], 403);
        }

        //Change Password
        $user = auth()->user();
        $user->password = Hash::make($request->password);
        $user->save();

        // Process the mail in queue
        dispatch(new SendPasswordChangedMail($user));

        return response()->json(['success' => 'Password changed successfully'], 200);
    }
}
