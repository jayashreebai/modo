<?php

namespace App\Http\Controllers\Auth;

use JWTAuth;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use PhpOption\None;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public $maxAttempts = 3;
    public $decayMinutes = 10;

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('jwt.auth')->except(['login', 'refresh']);
    }

    /**
     * Login service to log in a user with email and password
     *
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try {
            $credentials = request(['email', 'password']);

            // if (User::onlyTrashed()->where('email', $request->email)->exists()) {
            //     return response()->json(['error' => 'Account Deactivated. Required to activate the account to continue.'], 400);
            // }

            // If maximum limit crossed, trigger this function
            // if ($this->hasTooManyLoginAttempts($request)) {
            //     // Lock the users attempts for the given time period
            //     $this->fireLockoutEvent($request);
            //     // Return with the error message
            //     return response()->json(['error' => 'Your account is blocked now. You can try login again after 10 minutes.'], 400);
            // }

            // Generate auth token for login
            // If not, then return error message
            if (!$token = auth()->attempt($credentials)) {
                // Increment
                // $this->incrementLoginAttempts($request);
                return response()->json(['error' => 'Invalid credentials.'], 401);
            }
        } catch (JWTException $e) {
            // Something went wrong while attempting to encode the token
            return response()->json(['error' => 'Something went wrong'], 500);
        }

        if (User::where(['email' => $request->email])->exists()) {
            $user = User::where(['email' => $request->email])->first();
            if ($user->role_id == 2){
                return response()->json($this->respondWithToken($token,$user->schoolAdmin->slug));
            }
            return response()->json($this->respondWithToken($token));
        } else {
            return response()->json(['error' => 'Invalid credentials.'], 403);
        }
    }

    /**
    * Get the authenticated user
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['success' => 'Successfully logged out'], 200);
    }

    /**
    * Refresh a token
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function refresh()
    {
        // print_r($this->respondWithToken(auth()->refresh()));
        return response()->json(auth()->refresh());
    }

     /**
    * Refresh a token- jay
    *
    * @return \Illuminate\Http\JsonResponse
    */
    // public function refresh(Request $request)
    // {
    //     return JWTAuth::refresh('$request->token');
    //     $newtoken = JWTAuth::refresh($request->token);
    //     return response()->json(['new_token' => $newtoken]);
    //     // return $this->respondWithToken(auth()->refresh());
    // }

    /**
     * Get the token array structure
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $school_id=NULL)
    {
        $obj = new \stdClass();
        $obj->access_token = $token;
        $obj->token_type = 'bearer';
        $obj->expires_in = auth()->factory()->getTTL() * 60;
        $obj->school = $school_id;
        return $obj;
    }
}
