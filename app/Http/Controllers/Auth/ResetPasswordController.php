<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('verifyToken');
    }

    /**
     * Check token got expired or not
     *
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyToken($token)
    {
        if (!SupportingFunctions::getUserIfTokenExists($token) && SupportingFunctions::expireToken($token)) {
            return response()->json(['error' => 'Token got expired!!'], 444);
        } else {
            return response()->json(['success' => 'Token exists'], 200);
        }
    }

    /**
     * Password reset service
     * Accepts generated token and new password
     *
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(ResetPasswordRequest $request)
    {
        // Get the user details if the given reset password token exists
        if ($user = SupportingFunctions::getUserIfTokenExists($request->token)) {

            // Update the old password with new password
            User::where('email', $user->email)->update(['password' => Hash::make($request->password)]);

            // After updating, deleting the password token
            SupportingFunctions::deleteVerificationToken($request->token);

            return response()->json(['success' => 'Password reset successfully'], 200);
        }

        return response()->json(['error', 'error' => 'Token expired!!'], 440);
    }
}
