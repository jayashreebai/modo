<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Jobs\Password\SendPasswordResetMail;
use App\Http\Controllers\SupportingFunctions;
use App\Http\Requests\Auth\ForgotPasswordRequest;

// use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails.
    |
     */

    /**
     * ForgotPasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user after clicking "Forgot Password?"
     *
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getToken(ForgotPasswordRequest $request)
    {
        // fetching the user details of given email
        $user = User::where('email', $request->email)->first();

        // If user not found, through error message
        if (!$user) {
            return response()->json(['error' => 'User with given email id does not exist'], 400);
        }

        // Generate Email verification token
        $token = SupportingFunctions::saveToken($request->email);

        // Process the mail in queue
        dispatch(new SendPasswordResetMail($user, $token, $flag=2));

        return response()->json(['token' => $token, 'success' => 'Please check your inbox to reset your password'], 200);
    }
}
