<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserQuizData extends Model
{    
    use SoftDeletes;

    protected $fillable=[
        'user_id', 'lesson_id', 'course_content_id', 'quiz'
    ];
}
