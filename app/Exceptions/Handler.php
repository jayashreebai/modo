<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof UnauthorizedHttpException) {
            $preException = $exception->getPrevious();

            if ($preException instanceof TokenExpiredException) {
                return response()->json(['error' => 'Session expired, please login'], 440);
            } elseif ($preException instanceof TokenInvalidException) {
                return response()->json(['error' => 'Session expired, please login'], 440);
            } elseif ($preException instanceof TokenBlacklistedException) {
                return response()->json(['error' => 'You are blacklisted, please contact admin'], 400);
            } elseif ($exception->getMessage() === 'User not found') {
                return response()->json(['error' => 'User not found'], 404);
            }

            if ($exception->getMessage() === 'Token not provided') {
                return response()->json(['error' => 'You need to Sign in or Sign Up before continuing'], 401);
            }
        } elseif ($exception instanceof NotFoundHttpException) {
            return response()->json(['error' => 'Page not found'], 404);
        } elseif ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json(['error' => 'Wrong request type'], 405);
        } elseif ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['error' => 'Model not found'], 404);
        }

        return parent::render($request, $exception);
    }
}
