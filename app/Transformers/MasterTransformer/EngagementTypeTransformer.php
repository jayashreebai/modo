<?php

namespace App\Transformers\MasterTransformer;

use League\Fractal\TransformerAbstract;

class EngagementTypeTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | EngagementType Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display Engagement type.
    |
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($engagementType)
    {
        return [
            'type' => $engagementType->type,
            'icon' => $engagementType->icon,
            'description' => $engagementType->description,
        ];
    }
}
