<?php

namespace App\Transformers\School;

use App\Models\School\Contact;
use League\Fractal\TransformerAbstract;

class SchoolContactDetailTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | SchoolContactDetail Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display school contacts for the application.
    |
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Contact $contact)
    {
        return [
            "id" => $contact->id,
            "name" => $contact->name,
            "phone" => $contact->phone,
            "designation" => $contact->designation,
//            "school_id" => $contact->school_id,

        ];
    }
}
