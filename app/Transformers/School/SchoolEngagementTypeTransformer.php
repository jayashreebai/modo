<?php

namespace App\Transformers\School;

use App\Models\EngagementType;
use App\Models\School\School;
use League\Fractal\TransformerAbstract;

class SchoolEngagementTypeTransformer extends TransformerAbstract
{
    /**
     * To handle for both school and null school value
     * The main aim of this transformer is to return values for 
     * engagement-type as true if its existing , else false.
     * 
     */
    protected $school;

    public function __construct($school = null) {
        $this->school = $school;
    }
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(EngagementType $engagementType)
    {
        if ($this->school == null) {
            return [
                'id' => $engagementType->id,
                'type' => $engagementType->type,
                'icon' => $engagementType->icon,
                'description' => $engagementType->description,
                'active' => 'false'
            ];
        } else {
           $engagementTyp = School::where('id', $this->school)->value('engagement_type_id');
           if ($engagementType->id == $engagementTyp) {
            return [
                'id' => $engagementType->id,
                'type' => $engagementType->type,
                'icon' => $engagementType->icon,
                'description' => $engagementType->description,  
                'active' => 'true'     
            ]; 
           } else {
            return [
                'id' => $engagementType->id,
                'type' => $engagementType->type,
                'icon' => $engagementType->icon,
                'description' => $engagementType->description,  
                'active' => 'false'     
            ];
           }          
           
        }    
        
    }
}
