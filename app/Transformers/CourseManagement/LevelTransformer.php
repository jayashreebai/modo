<?php

namespace App\Transformers\CourseManagement;

use League\Fractal\TransformerAbstract;

class LevelTransformer extends TransformerAbstract
{

    /*
    |--------------------------------------------------------------------------
    | Level Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display task activities for the application.
    |
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($level)
    {
        return [
            'id'  => $level->id,
            'name' => $level->level,
            'icon' => $level->icon,
        ];
    }
}
