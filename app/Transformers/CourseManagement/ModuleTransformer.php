<?php

namespace App\Transformers\CourseManagement;

use App\Models\Course\Course;
use League\Fractal\TransformerAbstract;
use App\Transformers\SchoolManagement\CourseModuleTransformer;


class ModuleTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Module Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display modules for course.
    |
     */

    
    protected $defaultIncludes = [
        'modules'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Course $course)
    {
        return [
            'id' => $course->id,
            'course' => $course->course,
            'slug' => $course->slug,
            'description' => $course->description,
            'video' => $course->video_url,
            'course_duration' => $course->course_duration,
        ];
    }

    public function includeModules(Course $course)
    {
        $module = $course->modules();
        if ($module) {            
            return $this->collection($module, new CourseModuleTransformer());
        }
        else  
        {
            return $this->null();
        }
    }
}
