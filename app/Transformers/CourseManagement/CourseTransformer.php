<?php

namespace App\Transformers\CourseManagement;

use App\Models\Course\Course;
use League\Fractal\TransformerAbstract;

class CourseTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Course Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display course content for the application.
    |
     */
    protected $defaultIncludes = [
        'level'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($course)
    {
        return [
            'id' => $course->id,
            'slug' => $course->slug,
            'title' => $course->course,
            'description' => $course->description,
            'course_duration' => $course->course_duration,
            'video' => $course->video_url,
            'active' => $course->active
        ];
    }

    /**
     * Include level 
     *
     * @param $course
     * @return \League\Fractal\Resource\Item
     */
    public function includeLevel(Course $course)
    {
        $level = $course->level;
        return $this->item($level, new LevelTransformer());
    }
}
