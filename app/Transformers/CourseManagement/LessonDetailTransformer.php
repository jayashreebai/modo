<?php

namespace App\Transformers\CourseManagement;

use League\Fractal\TransformerAbstract;

class LessonDetailTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Lesson Detail Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display lesson details.
    |
     */

    /**
     * A Fractal transformer for Lesson Details.
     *
     * @return array
     */
    public function transform($lessonDetail)
    {
        // return $lessonDetail;
        return [
            'id' => $lessonDetail['id'],
            'slug' => $lessonDetail['slug'],
            'title' => $lessonDetail['lesson_name'],
            'description' => isset($lessonDetail['description']) ? $lessonDetail['description'] : null,
            'video' =>isset( $lessonDetail['video']) ? $lessonDetail['video'] : null,
            'active' =>isset( $lessonDetail['active']) ? $lessonDetail['active'] : null,
            'quiz' => isset($lessonDetail['quiz']) ? $lessonDetail['quiz'] : []
        ];
    }
}
