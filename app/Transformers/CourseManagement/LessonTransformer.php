<?php

namespace App\Transformers\CourseManagement;

use League\Fractal\TransformerAbstract;

class LessonTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Lesson Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display lessons for the modules.
    |
     */
    /**
     * A Fractal transformer is for Lesson Transformer
     *
     * @return array
     */
    public function transform($lesson)
    {
        return [
            'id' => $lesson['id'],
            'title' => $lesson['lesson_name'],
            'order' => $lesson['order'],
            'active' => boolval($lesson['active'])
        ];
    }
}
