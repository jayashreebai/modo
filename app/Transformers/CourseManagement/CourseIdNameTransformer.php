<?php

namespace App\Transformers\CourseManagement;

use App\Models\Course\Course;
use App\Models\School\Calendar;
use App\Models\School\CourseContent;
use App\Models\School\CourseProgress;
use League\Fractal\TransformerAbstract;

class CourseIdNameTransformer extends TransformerAbstract
{
    public $batch;

    public function  __construct($batch)
    {
        $this->batch = $batch;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Course $course)
    {
        $Calender = Calendar::where('batch_id', $this->batch->id)->orderBy('id','DESC')->first();
        return [
            'id' => $course->id,
            'course' => $course->course,
            'title' => $course->course,
            'slug' => $course->slug,
            'course_duration' => $course->course_duration,
            'description' => $course->description,
            'level' => $course->level->id,
            'video' => $course->video_url,
            'totalModule' => CourseContent::where('school_course_id', $this->batch->school_course_id)->count(),
            'completedModule' => CourseProgress::where(['school_course_id'=> $this->batch->school_course_id,'batch_id'=> $this->batch->id])->count(),
            'batch_end' => isset($Calender) ?  $Calender->scheduled_date : null
        ];
    }
}
