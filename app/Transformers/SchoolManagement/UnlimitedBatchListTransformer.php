<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\SchoolCourse;
use App\Models\Course\Course;
use Illuminate\Support\Carbon;
use App\Models\School\CourseProgress;
use App\Models\School\CourseContent;
use App\Models\School\LessonProgress;
use League\Fractal\TransformerAbstract;

class UnlimitedBatchListTransformer extends TransformerAbstract
{ 
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($content)
    {
        $course = SchoolCourse::whereId($content->school_course_id)->value('course_id');
        $school = SchoolCourse::whereId($content->school_course_id)->value('id');
        $courseName = Course::whereId($course)->value('course');
        // $nextlesson = json_decode($content->module_content,true);
        
        if (!CourseProgress::where(['batch_id'=> null, 'course_content_id' => $content->id])->exists()) 
        {               
            $courseContent = CourseContent::whereId($content->id)->first();
            $lesson = json_decode($courseContent->module_content, true);
            return [
                'batchId' => null,
                'clusterName' => null,  
                'batchName' => null,
                'courseName' => $courseName,
                'nextLessonName' => $lesson[0]['lesson_name'],
                'NextlessonId' => $lesson[0]['id'],
                'last_updated_time' => null,
                'courseContentId' => $courseContent->id,
                'module_id' => $courseContent->module->id,
                'completed' => 'false',
                'schoolCourseId' => $school,
                'engagementType' => 1
            ];           
        }
        else
        {
            $moduleList = CourseProgress::where(['batch_id'=> null, 'course_content_id' => $content->id])->pluck('module_id')->toArray();
            $activeModuleArray = CourseContent::where('school_course_id',$content->school_course_id)->pluck('module_id')->toArray();
            $newModuleId = array_diff($activeModuleArray, $moduleList);
            if (count($newModuleId)) {
                $courseContent = CourseContent::where(['school_course_id' => $content->school_course_id, 'module_id' => $newModuleId[1]])->first(); 
                $lesson = json_decode($courseContent->module_content, true);
    
                if (!LessonProgress::where(['course_content_id' => $courseContent->id, 'batch_id' => null])->exists())
                {
                    return [
                        'batchId' =>null,
                        'clusterName' => null,  
                        'batchName' => null,
                        'courseName' => $courseName,
                        'nextLessonName' => $lesson[0]['lesson_name'],
                        'NextlessonId' => $lesson[0]['id'],
                        'last_updated_time' => null,
                        'courseContentId' => $courseContent->id,
                        'module_id' => $courseContent->module->id,
                        'completed' => 'false',                        
                        'schoolCourseId' => $school,
                        'engagementType' => 1
                    ];

                    
                }
                else
                {
                    $lesson_id = LessonProgress::where(['course_content_id' => $courseContent->id, 'batch_id' => null])->latest()->first();
                    $date = LessonProgress::where(['course_content_id' => $courseContent->id, 'batch_id' => null])->latest()->value('entered_date');
                    foreach($lesson as $key => $value)
                    {
                        if ($value['id'] == $lesson_id->value('lesson_hash_id')) {
                            $found = 1;
                            $index = $key+1;
                        }            
                    }
                    if(isset($found))
                    {
                        foreach($lesson as $key => $value)
                        {
                            if($key == $index)
                            {
                                $nextLesson = $value; 
                            }
                        }
                        return [
                        'batchId' => null,
                        'clusterName' => null,
                        'batchName' => null,
                        'courseName' => $courseName,
                        'nextLessonName' => $nextlesson[0]['lesson_name'],
                        'NextlessonId' => $nextlesson[0]['id'],
                        'last_updated_time' => $date,
                        'courseContentId' => $courseContent->id,
                        'module_id' => $courseContent->module->id,
                        'completed' => 'false',                        
                        'schoolCourseId' => $school,
                        'engagementType' => 1

                    ];
    
                    }
                    
                  
                }
            
            }
            if (!SchoolCourse::where(['id' => $content->school_course_id ])->where('expiry_date->to', '>=', Carbon::now()->format('Y-m-d'))->exists()) {
                return [
                    'batchId' => null,
                    'clusterName' => null,
                    'batchName' => null,
                    'courseName' => $courseName,
                    'nextLessonName' => null,
                    'NextlessonId' => null,
                    'last_updated_time' => null,
                    'courseContentId' => null,
                    'module_id' => null,
                    'completed' => 'true',                
                    'schoolCourseId' => $school,
                    'engagementType' => 1
    
                ];                
            }
            else{
                return [
                    'batchId' => null,
                    'clusterName' => null,
                    'batchName' => null,
                    'courseName' => $courseName,
                    'nextLessonName' => null,
                    'NextlessonId' => null,
                    'last_updated_time' => null,
                    'courseContentId' => null,
                    'module_id' => null,
                    'completed' => 'false',                
                    'schoolCourseId' => $school,
                    'engagementType' => 1
    
                ];
            }
           
            // Take latest module compeleted by batch_id in course progress( module_completed->module_id)
            // Take next module_id 
        } 
    }
}
