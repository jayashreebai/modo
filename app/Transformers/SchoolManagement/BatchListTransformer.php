<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\Batch;
use App\Models\School\Student;
use App\Models\Course\Course;
use App\Transformers\CourseManagement\CourseIdNameTransformer;
use League\Fractal\TransformerAbstract;

class BatchListTransformer extends TransformerAbstract
{
    /** 
     * A Fractal transformer.
     *
     * @return array
     */

    protected $defaultIncludes = [
        'coordinator', 
        'course'
    ];

    public function transform(Batch $batch)
    {
        if (Student::where('batch_id', $batch->id)->exists()) {
            return [
                'id' => $batch->id,
                'cluster' => $batch->cluster,
                'batch' => $batch->batch,
                'modules_per_day' => $batch->modules_per_day,
                'student_imported' => 'true',
                'compeleted' => boolval($batch->completed),
                'school_course_id' => $batch->school_course_id
            ];
        } else {
            return [
                'id' => $batch->id,
                'cluster' => $batch->cluster,
                'batch' => $batch->batch,
                'modules_per_day' => $batch->modules_per_day,
                'student_imported' => 'false',
                'compeleted' => boolval($batch->completed),
                'school_course_id' => $batch->school_course_id

            ];
        }   
       
    }

    public function includeCoordinator(Batch $batch)
    {
        if ($batch->coordinator) 
        {           
            return $this->item($batch->coordinator, new SchoolAdminTransformer());           
        }       
    }

    public function includeCourse(Batch $batch)
    {
        if ($batch->course) {  
            $course = Course::whereId($batch->course->course_id)->get();
            return $this->collection($course, new CourseIdNameTransformer($batch));
        }       
    }
}
