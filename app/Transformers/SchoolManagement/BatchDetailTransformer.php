<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\Batch;
use League\Fractal\TransformerAbstract;

class BatchDetailTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Batch $batch)
    {
        return [
            'id' => $batch->id,
            'batch' => $batch->batch,
            'cluster' => $batch->cluster,
        ];
    }
}
