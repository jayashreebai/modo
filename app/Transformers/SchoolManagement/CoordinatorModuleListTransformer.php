<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\CourseContent;
use App\Models\School\CourseProgress;
use App\Models\School\LessonProgress;
use App\Models\School\SchoolCourse;
use App\Models\Course\Module;
use League\Fractal\TransformerAbstract;

class CoordinatorModuleListTransformer extends TransformerAbstract
{
    protected $batch;

    public function __construct($batch) {
        $this->batch = $batch;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    // public function transform($module)
    // {
    //     $array = array();
    //     $courseContent  = CourseContent::where('school_course_id', $batch->school_course_id)->get(); 
    //     $activeModules = json_decode($batch->active_module_id);  
    //     $completedModules = CourseProgress::where(['batch_id' => $batch->id])->pluck('module_id')->toArray(); 
    //     $completed = 'false';
    //     $unlock = 'false'; 
    //     $activeModuleArray = array();
    //     foreach ($activeModules as $key =>$row) 
    //     {  
    //         $data = 'module_'.++$key;
    //         $activeModuleArray[] = ($row->{$data});
    //     }
    //     return $data = $courseContent->map(function ($item) use ($activeModuleArray, $completedModules) {
    //         $unlock = 'false';
    //         $completed = 'false';
    //         if (in_array($item['module_id'], $activeModuleArray)) {
    //             $unlock = 'true';
    //         }
    //         if (in_array($item['module_id'], $completedModules)) {
    //             $completed = 'true';
    //         }
    //         return [
    //             'id' => $item['id'],
    //             'lessons' => $item['module_content'],
    //             'module' => $item['module']->module,
    //             'completed' => $completed,
    //             'unlock' => $unlock
    //         ];           
    //     })->toArray();

    //     // foreach ($courseContent as $key => $value) {
    //     //     if (in_array($value->module_id, $activeModuleArray)) 
    //     //     {
    //     //         $unlock = 'true';
    //     //     }  
    //     //     if (in_array($value->module_id, $completedModules)) {
    //     //         $completed = 'true';
    //     //     }

    //     //    return [
    //     //         'id' => $value->id,
    //     //         'lessons' => $value->module_content,
    //     //         'module' => $value->module->module,
    //     //         'completed' => $completed,
    //     //         'unlock' => $unlock
    //     //     ];  
    //     //     array_push($array, $data); 
                 
    //     // }
        
    // }

    public function transform($module)
    {         
        $courseContentDetails = CourseContent::where(['school_course_id' => $this->batch->school_course_id, 'module_id' => $module->id])->first(); 
        $activeModules = json_decode($this->batch->active_module_id);  
        $completedModules = CourseProgress::where(['batch_id' => $this->batch->id, 'course_content_id' => $courseContentDetails->id])->pluck('module_id')->toArray(); 
        // $completed = false;
        // var_dump($completedModules);die;
        $unlock = false; 
        $activeModuleArray = array();
        foreach ($activeModules as $key =>$row) 
        {  
            $data = 'module_'.++$key;
            $activeModuleArray[] = ($row->{$data});
        }
        if (in_array($module->id, $activeModuleArray)) 
        {
            $unlock = true;
        }
        if (in_array($module->id, $completedModules)) 
        {
            $unlock = false;
            $completed = true;
        }
        else{
            $completed = false;
        }
        $lessons = collect(json_decode($courseContentDetails->module_content, true));
        $count = count(json_decode($courseContentDetails->module_content));
        $completedLessons = LessonProgress::where(['course_content_id' => $courseContentDetails->id, 'module_id' => $module->id, 'batch_id' => $this->batch->id])->pluck('lesson_hash_id')->toArray();
        // $completedLessons = LessonProgress::where(['course_content_id' => $courseContentDetails->id])->pluck('lesson_hash_id')->toArray();
        $data = $lessons->map(function ($item) use ($completedLessons, $count) {
                    if (in_array($item['id'], $completedLessons)) {
                        $lessonCompleted = true;
                    }
                    else{
                        $lessonCompleted = false;
                    }       
                    return [
                        'id' => $item['id'],
                        'slug' => $item['slug'],
                        'name' => $item['lesson_name'],
                        'order' => $item['order'],
                        'completed' => $lessonCompleted,
                        'count' => $count
                    ];           
                });
        
        return [
                'id' => $module->id,
                'name' => $module->module,
                'order' => $courseContentDetails->order,
                'completed' => $completed,
                'unlock' => $unlock,
                'lessons' => $data,
                'courseContentId' => $courseContentDetails->id

        ]; 

    }
}
