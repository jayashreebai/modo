<?php

namespace App\Transformers\SchoolManagement;

use League\Fractal\TransformerAbstract;

class SchoolInventory extends TransformerAbstract
{
    protected $defaultIncludes = [
         'inventory'
     ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(\App\Models\School\SchoolInventory $scInventory)
    {
        return [
            "id" => $scInventory->id ,
            "entered_date" => $scInventory->entered_date ,
            "working" => $scInventory->working ,
            "not_working" => $scInventory->not_working ,
            "lost" => $scInventory->lost,
            "categoryName" => $scInventory->inventory->category->item_name
        ];
    }

    public function includeInventory(\App\Models\School\SchoolInventory $scInventory)
    {
        return $this->Item($scInventory->inventory, new InventoryList());

    }
}
