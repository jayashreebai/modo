<?php

namespace App\Transformers\SchoolManagement;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class CoordinatorOrSchoolAdminTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            
            "id" => $user->id ,
            "slug" => $user->slug ,
            "first_name" => $user->first_name ,
            "last_name" => $user->last_name ,
            "email" => $user->email ,
            "phone" => $user->phone ,
            "occupation" => $user->coordinator['occupation'],
            "qualification" => $user->coordinator['qualification'],
            "address" => $user->coordinator['address'],
            
        ];
    }
}
