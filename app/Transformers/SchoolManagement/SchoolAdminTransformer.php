<?php

namespace App\Transformers\SchoolManagement;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class SchoolAdminTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [[
            
            "id" => $user->id ,
            "first_name" => $user->first_name ,
            "last_name" => $user->last_name ,
            "email" => $user->email ,
            "phone" => $user->phone ,
            
        ]];
    }
}
