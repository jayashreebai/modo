<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\School;
use App\Models\EngagementType;
use League\Fractal\TransformerAbstract;

class SchoolTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | School Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display school details.
    |
     */
    // protected $defaultIncludes = [
    //     'engagement_type'
    // ];

    /**
     * A Fractal transformer for school show details.
     *
     * @return array
     */
    public function transform($school)
    {
        return [
            'id' => $school['id'],
            'slug' => $school['slug'],
            'name' => $school['school'],
            'logo' => $school['logo'],
            'address' => $school['address'],
            'website' => $school['website_url'],
            'contact_number' => $school['contact_number'],
        ];
    }

    /**
     * Include engagement_type
     *
     * @param $school
     * @return \League\Fractal\Resource\Collection
     */
    // public function includeEngagementType(School $school)
    // {
    //     $engagementType = EngagementType::all();
    //     return $this->Collection($engagementType, new EngagementTypeForSchoolTransformer($school));
    // }
}
