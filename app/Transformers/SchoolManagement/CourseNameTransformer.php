<?php

namespace App\Transformers\SchoolManagement;

use App\Models\Course\Course;
use League\Fractal\TransformerAbstract;

class CourseNameTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Course $course)
    {
        return [
            'id' => $course->id,
            'course' => $course->course,
            'title' => $course->course,
            'slug' => $course->slug,
            'course_duration' => $course->course_duration,
            'description' => $course->description,
            'level' => $course->level->id,
            'video' => $course->video_url,
        ];
    }
}
