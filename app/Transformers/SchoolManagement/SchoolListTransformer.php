<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\School;
use App\Models\EngagementType;
use League\Fractal\TransformerAbstract;

class SchoolListTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */

    // protected $defaultIncludes = [
    //     'engagement_type'
    // ];

    public function transform($school)
    {
        return [
            'id' => $school->id,
            'slug' => $school->slug,
            'name' => $school->school,
            'logo' => $school->logo,
            'address' => $school->address,
            'contact_number' => $school->contact_number,
            'engagement_type' => $school->engagementType 
        ];
    }

   
}
