<?php

namespace App\Transformers\SchoolManagement;

use App\Models\InventoryCategory;
use App\Models\School\Inventory;
use League\Fractal\TransformerAbstract;

class InventoryManagement extends TransformerAbstract
{
    public $schoolId;

    public function __construct($schoolId)
    {
        $this->schoolId = $schoolId;
    }

    protected $defaultIncludes = [
         'inventories'
     ];


    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(InventoryCategory $inventory)
    {
        return [
            'id' => $inventory->id,
            'item_name' => $inventory->item_name,
            'slug' => $inventory->slug,
        ];
    }


    public function includeInventories(InventoryCategory $inventory)
     {
         $inventories = $inventory->inventories->where('school_id',$this->schoolId);

         return $this->Collection($inventories, new InventoryList($inventories));
     }
}
