<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\Inventory;
use League\Fractal\TransformerAbstract;

class InventoryList extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Inventory $inventory)
    {
        return [
            'id' => $inventory->id,
            'item_name' => $inventory->item_name,
            'serial_number' => $inventory->serial_number,
        ];
    }
}
