<?php

namespace App\Transformers\SchoolManagement;

use League\Fractal\TransformerAbstract;

class AttendanceTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Attendance Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display attendances for the application.
    |
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($attendance)
    {
        return [
            'attendance_id' => $attendance->id,
            'student_name' => $attendance->student->name,
            'present' => boolval($attendance->present)
        ];
    }
}
