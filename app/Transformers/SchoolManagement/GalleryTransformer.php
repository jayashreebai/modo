<?php

namespace App\Transformers\SchoolManagement;

use League\Fractal\TransformerAbstract;

class GalleryTransformer extends TransformerAbstract
{
    /**
     * Gallery Transformer used for using selected values to be sent as a response.
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($gallery)
    {
        return [
            'id' => $gallery->id,
            'url' => $gallery->url,
            'description' => $gallery->description,
        ];
    }
}
