<?php

namespace App\Transformers\SchoolManagement;

use League\Fractal\TransformerAbstract;

class SchoolLevelLessonTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($lesson)
    {
        // if ($lesson['active']) {
            return [            
                'id' => $lesson['id'],
                'slug' => $lesson['slug'],
                'title' => $lesson['lesson_name'],
                'order' => $lesson['order'],
                'active' => boolval($lesson['active'])
            ];
        // }
        
    }
}
