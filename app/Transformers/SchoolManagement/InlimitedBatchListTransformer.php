<?php

namespace App\Transformers\SchoolManagement;

use League\Fractal\TransformerAbstract;

class InlimitedBatchListTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform()
    {
        return [
            //
        ];
    }
}
