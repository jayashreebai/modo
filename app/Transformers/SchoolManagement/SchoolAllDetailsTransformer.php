<?php

namespace App\Transformers\SchoolManagement;
 
use App\Models\Course\Course;
use App\Models\School\School;
use App\Transformers\School\SchoolContactDetailTransformer;
use App\Transformers\SchoolManagement\SchoolAdminTransformer;
use League\Fractal\TransformerAbstract;

class SchoolAllDetailsTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    protected $defaultIncludes = [
        'school_admin', 'coordinators', 'contacts'
    ];

    public function transform(School $school)
    {
//      Fetching all the courses from master table and course id from school course and the i mapped in to one object whether that is selected in the school or not.
        $schoolCourse = $school->courses->pluck('course_id')->toArray();
        $course = Course::get();
        $active_course = $course->map(function ($item) use ($schoolCourse) {
            if (in_array($item['id'], $schoolCourse)) {
                return [
                        "id" => $item['id'],
                        "name" => $item['course'],
                        "description" => $item['description'],
                        "is_selected_in_school" => true
                    ] ;
            } else {
                return [
                        "id" => $item['id'],
                        "name" => $item['course'],
                        "description" => $item['description'],
                        "is_selected_in_school" => false
                    ] ;
            }
        });

        return [
            "id" => $school->id,
            "school" =>$school->school,
            "logo" =>$school->logo,
            "courses" => $active_course
        ];
    }
    //  fetching school admin
    public function includeSchoolAdmin(School $school)
    {
        if ($school->schoolAdmin) {
            $schoolAdmin = $school->schoolAdmin;
            return $this->item($schoolAdmin, new SchoolAdminTransformer());
        }
        else  
        {
            return $this->null();
        }
    }
    //  fetching school Coordinators
    public function includeCoordinators(School $school)
    {
        $schoolCoordinators= $school->coordinators;
        return $this->Collection($schoolCoordinators, new CoordinatorOrSchoolAdminTransformer());
    }
    //  fetching school Contacts
    public function includeContacts(School $school)
    {
        $schoolContacts = $school->contacts;
        return $this->Collection($schoolContacts, new SchoolContactDetailTransformer);
    }
}
