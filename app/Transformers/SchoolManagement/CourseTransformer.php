<?php

namespace App\Transformers\SchoolManagement;

use Carbon\Carbon;
use App\Models\School\School;
use App\Models\School\SchoolCourse;
use App\Models\Course\Course;
use League\Fractal\TransformerAbstract;

class CourseTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(School $school)
    {
        $schoolCourse = $school->courses->pluck('course_id')->toArray();
        $course = Course::where('active',1)->get();
        
        $active_course = $course->map(function ($item) use ($schoolCourse, $school) {
            if (in_array($item['id'], $schoolCourse)) {
                $data = SchoolCourse::where(['school_id' => $school->id, 'course_id' => $item['id']])->first();
                $date = json_decode($data->expiry_date, true);
                $retVal = (Carbon::now()->format('Y-m-d') > $date['to']) ? true : false ;
                return [
                        "id" => $item['id'],
                        "slug" => $item['slug'],
                        "title" => $item['course'],
                        "description" => $item['description'],
                        "is_selected_in_school" => true,
                        "expired" => $retVal,
                        "date" => $data->expiry_date,
                        "school_course_id" => $data->id
                    ];
            } else {
                return [
                        "id" => $item['id'],
                        "slug" => $item['slug'],
                        "title" => $item['course'],
                        "description" => $item['description'],
                        "is_selected_in_school" => false,
                        "expired" => null,
                        "date" => null,
                        "school_course_id" => null
                    ] ;
            }
        });

        return [
            "courses" => $active_course
        ];
    }
}
