<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\CourseContent;
use App\Models\Course\Course;
use App\Models\Course\Module;
use App\Models\School\SchoolCourse;
use League\Fractal\TransformerAbstract;
use App\Transformers\SchoolManagement\ModuleLessonSchoolLevelTransformer;

class CourseContentTransformer extends TransformerAbstract
{ 
    
    // protected $defaultIncludes = [
    //     'modules'
    // ];
    /**
     * A Fractal transformer.
     *
     * @return array 
     */
    public function transform($schoolCourseId)
    {
        $courseId = SchoolCourse::whereId($schoolCourseId)->value('course_id');
        $course = Course::whereId($courseId)->first();
        $modules = CourseContent::where('school_course_id', $schoolCourseId)->get();
        $module = Module::where(['course_id' => $courseId, 'active' => 1])->pluck('id')->toArray();
        $active_module = $modules->map(function ($item) use ($module) {
            if (in_array($item['module_id'], $module)) {
                $details = Module::whereId($item['module_id'])->first();
                return [
                        "id" => $item['module_id'],
                        "name" => $details->module,
                        "slug" => $details->slug,
                        "order" => $item['order'],
                        "active" => boolval($item['active']),
                        "course_content_id" => $item['id']
                    ] ;
            }
            else
            {
                return [
                   
                ] ;
            }
        });

        return [
            "id" => $course->id,
            "course" =>$course->course,
            "slug" =>$course->slug,
            "description" =>$course->description,
            "video" =>$course->video_url,
            "course_duration" =>$course->course_duration,
            "modules" => array_filter(($active_module)->toArray())
        ];
    }

    // public function includeModules(CourseContent $c)
    // {
    //     if ($c->module) {
    //         $courseContent = $c->module;
    //         return $this->item($c, new ModuleLessonSchoolLevelTransformer());
    //     }
    //     else  
    //     {
    //         return $this->null();
    //     }
    // }
}
