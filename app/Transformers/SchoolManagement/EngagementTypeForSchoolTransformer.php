<?php

namespace App\Transformers\SchoolManagement;

use App\Models\EngagementType;
use League\Fractal\TransformerAbstract;

class EngagementTypeForSchoolTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | EngagementType for school Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display engagement types for school.
    |
     */

    //since school engagement type is required to set true or false.
    private $school = [];

    public function __construct($school = [])
    {
        $this->school = $school;
    }
    /**
     * A Fractal transformer for EngagementType in show school details.
     *
     * @return array
     */
    public function transform(EngagementType $engagementType)
    {
        $selected = false;
        if ($this->school->engagementType->id == $engagementType->id) {
            $selected = true;
        }
        return [
            'id' => $engagementType->id,
            'type' => $engagementType->type,
            'selected' => $selected,
        ];
    }
}
