<?php

namespace App\Transformers\SchoolManagement;

use Carbon\Carbon;
use App\Models\Course\Course;
use App\Models\School\School;
use App\Models\School\SchoolCourse;
use League\Fractal\TransformerAbstract;

class ForDropdownTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(School $school)
    {

        $schoolCourse = $school->courses->pluck('course_id')->toArray();
        $course = Course::get();
        
        $active_course = $course->map(function ($item) use ($schoolCourse, $school) {
            if (in_array($item['id'], $schoolCourse)) {
                $data = SchoolCourse::where(['school_id' => $school->id, 'course_id' => $item['id']])->first();
                $date = json_decode($data->expiry_date, true);
                $retVal = (Carbon::now()->format('Y-m-d') < $date['to']) ? false : true ;
                if ($retVal == false) {
                    
                    return [
                        "id" => $data->id,
                        "total_modules" => $item['course_duration'],
                        "course" => $item['course'],
                        "expired" => $retVal,
                    ];
                }
                else {
                    return [
                           
                        ] ;
                }
                
            } else {
                return [    
                       
                    ] ;
            }
        });

        return [
            "courses" => array_filter($active_course->toArray())
        ];
        
    }
}
