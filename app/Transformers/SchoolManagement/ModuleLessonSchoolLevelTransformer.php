<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\CourseContent;
use League\Fractal\TransformerAbstract;

class ModuleLessonSchoolLevelTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CourseContent $module)
    {
        return [
            'id' => $module->module_id,
            'module' => $module->module->module,
            'slug' => $module->module->slug,
            'order' => $module->module->order,
            'lesson' => $module->module_content
        ];
    }
}
