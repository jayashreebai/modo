<?php

namespace App\Transformers\SchoolManagement;

use League\Fractal\TransformerAbstract;

class InventoryCategoryTransformer extends TransformerAbstract
{
    /**
     * InventoryCategory Transformer to fetch fields
     * regarding category
     *
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($inventoryCategory)
    {
        return [
            'id' => $inventoryCategory->id,
            'name' => $inventoryCategory->item_name,
            'slug' => $inventoryCategory->slug
        ];
    }
}
