<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\Batch;
use App\Models\School\School;
use App\Models\Course\Course;
use App\Models\School\SchoolCourse;
use App\Models\EngagementType;
use App\Models\School\CourseProgress;
use App\Models\School\LessonProgress;
use App\Models\School\CourseContent;
use League\Fractal\TransformerAbstract;

class BatchForCoordinatorTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Batch Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display batch details for coordinator.
    |
     */
    /**
     * A Fractal transformer for a coordinator to see all batches assigned to him
     *
     * @return array
     */
    public function transform($batches)
    { 
        // print_r($batches);die;
        $school = SchoolCourse::where('id', $batches->batch->school_course_id)->first();
        $course = Course::where('id', $school->course_id)->first();
        $engagementType = School::whereId($school->school_id)->value('engagement_type_id');
 
        $modules = Batch::where('id', $batches->batch->id)->value('active_module_id');
        $activeModule = json_decode($modules);
        $activeModuleArray = array();
        foreach ($activeModule as $key =>$row) 
        { 
            $data = 'module_'.++$key;
            $activeModuleArray[] = ($row->{$data});
        }
        if (!CourseProgress::where('batch_id', $batches->batch->id)->exists()) 
        {               
            $activeModules = json_decode($modules, true);
            $courseContent = CourseContent::where(['school_course_id' => $batches->batch->school_course_id, 'module_id' => $activeModules[0]['module_1']])->first();
            $lesson = json_decode($courseContent->module_content, true);
            return [
                'batchId' => $batches->batch->id,
                'clusterName' => $batches->batch->cluster,  
                'batchName' => $batches->batch->batch,
                'courseName' => $course->course,
                'nextLessonName' => $lesson[0]['lesson_name'],
                'NextlessonId' => $lesson[0]['id'],
                'last_updated_time' => null,
                'courseContentId' => $courseContent->id,
                'module_id' => $courseContent->module->id,
                'completed' => 'false',
                'schoolCourseId' => $school->id,
                'engagementType' => $engagementType
            ];           
        }
        else
        {
            $moduleList = CourseProgress::where('batch_id', $batches->batch->id)->pluck('module_id')->toArray();
            $newModuleId = array_diff($activeModuleArray, $moduleList);
            // print_r($newModuleId);die;

            
            $courseContentCount = CourseContent::where(['school_course_id' => $batches->batch->school_course_id])->count();
            if ($courseContentCount == count($moduleList)) 
            {
                return [
                    'batchId' => $batches->batch->id,
                    'clusterName' => $batches->batch->cluster,
                    'batchName' => $batches->batch->batch,
                    'courseName' => $course->course,
                    'nextLessonName' => null,
                    'NextlessonId' => null,
                    'last_updated_time' => null,
                    'courseContentId' => null,
                    'module_id' => null,
                    'completed' => 'true',
                    'schoolCourseId' => $school->id,
                    'engagementType' => $engagementType
    
    
                ];
            }



            if (count($newModuleId)) {
                // print_r($newModuleId);die;
                $courseContent = CourseContent::where(['school_course_id' => $batches->batch->school_course_id, 'module_id' => $newModuleId[0]])->first(); 
                $lesson = json_decode($courseContent->module_content, true);
    
                if (!LessonProgress::where(['course_content_id' => $courseContent->id, 'batch_id' => $batches->batch->id])->exists())
                {
                    return [
                        'batchId' => $batches->batch->id,
                        'clusterName' => $batches->batch->cluster,  
                        'batchName' => $batches->batch->batch,
                        'courseName' => $course->course,
                        'nextLessonName' => $lesson[0]['lesson_name'],
                        'NextlessonId' => $lesson[0]['id'],
                        'last_updated_time' => null,
                        'courseContentId' => $courseContent->id,
                        'module_id' => $courseContent->module->id,
                        'completed' => 'false',
                        'schoolCourseId' => $school->id,
                        'engagementType' => $engagementType
                    ];
                }
                else
                {
                    $lesson_id = LessonProgress::where(['course_content_id' => $courseContent->id, 'batch_id' => $batches->batch->id])->latest()->value('lesson_hash_id');
                    $date = LessonProgress::where(['course_content_id' => $courseContent->id, 'batch_id' => $batches->batch->id])->latest()->value('entered_date');
                    foreach($lesson as $key => $value)
                    {
                        if ($value['id'] == $lesson_id) {
                            $found = 1;
                            $index = $key+1;
                        }            
                    }
                    if(isset($found))
                    {
                        foreach($lesson as $key => $value)
                        {
                            if($key == $index)
                            {
                                $nextlesson = $value; 
                            }
                        }
    
                    }
                    return [
                        'batchId' => $batches->batch->id,
                        'clusterName' => $batches->batch->cluster,
                        'batchName' => $batches->batch->batch,
                        'courseName' => $course->course,
                        'nextLessonName' => $nextlesson[0]['lesson_name'],
                        'NextlessonId' => $nextlesson[0]['id'],
                        'last_updated_time' => $date,
                        'courseContentId' => $courseContent->id,
                        'module_id' => $courseContent->module->id,
                        'completed' => 'false',
                        'schoolCourseId' => $school->id,
                        'engagementType' => $engagementType


                    ];
                  
                }
            
            }            
            return [
                'batchId' => $batches->batch->id,
                'clusterName' => $batches->batch->cluster,
                'batchName' => $batches->batch->batch,
                'courseName' => $course->course,
                'nextLessonName' => null,
                'NextlessonId' => null,
                'last_updated_time' => null,
                'courseContentId' => null,
                'module_id' => null,
                'completed' => 'false',
                'schoolCourseId' => $school->id,
                'engagementType' => $engagementType


            ];
            // Take latest module compeleted by batch_id in course progress( module_completed->module_id)
            // Take next module_id 
        }   
        
    } 
}
