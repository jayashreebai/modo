<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\CourseContent;
use App\Models\School\CourseProgress;
use App\Models\School\LessonProgress;
use App\Models\School\SchoolCourse;
use App\Models\Course\Module;
use League\Fractal\TransformerAbstract;

class ModuleListUnlimitedTransformer extends TransformerAbstract
{
    protected $schoolCourse;

    public function __construct($schoolCourse) {
        $this->schoolCourse = $schoolCourse;
    }
    /**
     * A Fractal transformer.
     *
     * @return array
     * 
     */
    public function transform($module)
    {
        $courseContentDetails = CourseContent::where(['school_course_id' => $this->schoolCourse->id, 'module_id' => $module->id])->first(); 
        // print_r($this->schoolCourse);die;
        // $activeModules = json_decode($this->batch->active_module_id);  
        $completedModules = CourseProgress::where(['batch_id' => null, 'course_content_id' => $courseContentDetails->id])->pluck('module_id')->toArray(); 
        $completed = false;        
        
        if (in_array($module->id, $completedModules)) 
        {
            $completed = true;
        }
        $lessons = collect(json_decode($courseContentDetails->module_content, true));
        $completedLessons = LessonProgress::where(['course_content_id' => $courseContentDetails->id, 'module_id' => $module->id,'batch_id' => null])->pluck('lesson_hash_id')->toArray();
        $data = $lessons->map(function ($item) use ($completedLessons, $courseContentDetails) {
                    if (in_array($item['id'], $completedLessons)) {
                        $lessonCompleted = true;
                    }
                    else{
                        $lessonCompleted = false;
                    }       
                    return [
                        'id' => $item['id'],
                        'slug' => $item['slug'],
                        'name' => $item['lesson_name'],
                        'order' => $item['order'],
                        'completed' => $lessonCompleted,
                        'count' => count(json_decode($courseContentDetails->module_content, true))
                    ];           
                });
        
        return [
                'id' => $module->id,
                'name' => $module->module,
                'order' => $courseContentDetails->order,
                'completed' => $completed,
                'unlock' => true,
                'lessons' => $data,
                'courseContentId' => $courseContentDetails->id
        ]; 

    }
}
