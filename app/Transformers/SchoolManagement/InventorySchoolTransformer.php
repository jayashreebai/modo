<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\Inventory;
use League\Fractal\TransformerAbstract;

class InventorySchoolTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Inventory $inventory)
    {
        return [
            'inventory_id' => $inventory->id,
            'category' => $inventory->category,
            'serial_number' => $inventory->serial_number,
            'name' => $inventory->item_name,
            'quantity' => $inventory->quantity,
        ];
    }
}
