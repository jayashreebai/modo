<?php

namespace App\Transformers\SchoolManagement;

use App\Models\School\CourseContent;
use League\Fractal\TransformerAbstract;

class SchoolCourseModuleTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | School course module Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display school course modules.
    |
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CourseContent $content)
    {
        return [
            'id' => $content->module_id,
            'name' => $content->module->module,
            'active' => $content->active,
            'order' => $content->order,
            'lesson' => $content->module_content,
        ];
    }
}
