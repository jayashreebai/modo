<?php

namespace App\Transformers\SchoolManagement;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class AnnouncementTransformer extends TransformerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Announcement Transformer
    |--------------------------------------------------------------------------
    |
    | This file used to transform data in proper format
    | to display announcements for the application.
    |
     */
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($announcement)
    {
        return [
            'id' => $announcement->id,
            'title' => $announcement->title,
            'slug' => $announcement->slug,
            'description' => $announcement->description,
            'url' => $announcement->url,
            'date' => count($announcement->school) > 0 ? Carbon::parse($announcement->school[0]->pivot->date)->format('d-m-Y') : null
        ];
    }
}
