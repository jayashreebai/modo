<?php

namespace App\Transformers\SchoolManagement;

use League\Fractal\TransformerAbstract;

class CourseModuleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($module)
    {
        return [
            'id' => $module->id,
            'name' => $module->module,
            'active' => boolval($module->active),
            'slug' => $module->slug,
            'order' => $module->order,
        ];
    }
}
