<?php

namespace App\Transformers\Coordinator;

use App\Http\Controllers\School\CoordinatorBatchCourseController;
use App\Models\School\Calendar;
use League\Fractal\TransformerAbstract;

class CalendarDetail extends TransformerAbstract
{
    public $batch_id;

    public function __construct($batch_id)
    {
        $this->batch_id = $batch_id;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Calendar $calendar)
    {
        $coordinator = new CoordinatorBatchCourseController();
        $dateWithModule = $coordinator->lessonWithCompletionDate($this->batch_id);
        $module = [];
        foreach ($dateWithModule as $value){
            if($calendar->scheduled_date == $value['date']){
                array_push($module,$value['name']);
            }
        }
        return [
            'id' => $calendar->id,
            'date' => $calendar->scheduled_date,
            'time' => $calendar->scheduled_time,
            'moduleName' => $module
        ];
    }
    
}
