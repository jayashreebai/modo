<?php

use App\Models\Course\Level;
use Illuminate\Database\Seeder;

class CourseLevelsTableSeeder extends Seeder
{
    /*
    |--------------------------------------------------------------------------
    | CourseLevels Table Seeder
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | as CourseLevels throughout the application.
    |
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::create(['level' => 'Jumpstart']);
        Level::create(['level' => 'Level 1']);
        Level::create(['level' => 'Level 2']);
    }
}
