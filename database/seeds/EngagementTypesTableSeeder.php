<?php

use App\Models\EngagementType;
use Illuminate\Database\Seeder;

class EngagementTypesTableSeeder extends Seeder
{
    /*
    |--------------------------------------------------------------------------
    | EngagementType Table Seeder
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | as Engagement Type throughout the application.
    |
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EngagementType::create([
            'type' => 'Unlimited',
            'description' => 'Unlimited access to course'
        ]);

        EngagementType::create([
            'type' => 'By class',
            'description' => 'By Class engagement type as access to class level'
        ]);

        EngagementType::create([
            'type' => 'By student',
            'description' => 'By Student engagement type as access to Student level'
        ]);
    }
}
