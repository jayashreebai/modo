<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /*
    |--------------------------------------------------------------------------
    | Users Table Seeder
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | as users throughout the application.
    | Modo Admin details is populated in users table , so Users Table Seeder is used.
    |
    */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Jude',
            'email' => 'jude@heymodo.com',
            'phone' => 9999999999,
            'password' => Hash::make('Test1234'),
            'role_id' => 1
        ]);
    }
}
