<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /*
    |--------------------------------------------------------------------------
    | Roles Table Seeder
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | as roles throughout the application.
    | Roles table is master table and to populate that table we are using
    | this Roles Table Seeder file.
    |
    */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'role' => 'Modo Admin'
        ]);

        Role::create([
            'role' => 'School Admin'
        ]);

        Role::create([
            'role' => 'Coordinator'
        ]);
    }
}
