<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolAnnouncementsTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | SchoolAnnouncement Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for announcements for schools throughout the application.
    | This is populated by ModoAdmin.
    |
    /**
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('school_announcements', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('school_id')->index();
            $table->foreign('school_id')->references('id')->on('schools');

            $table->unsignedBigInteger('announcement_id')->index();
            $table->foreign('announcement_id')->references('id')->on('announcements');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_announcements');
    }
}
