<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Announcement Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for announcements throughout the application.
    | This is populated by ModoAdmin.
    |
    /**
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->string('url')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
