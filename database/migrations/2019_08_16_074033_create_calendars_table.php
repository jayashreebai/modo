<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Calendar Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for Calendar throughout the application.
    | This is populated by both modoadmin and coordinator
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('coordinator_id')->index();
            $table->foreign('coordinator_id')->references('id')->on('users');

            $table->unsignedBigInteger('batch_id')->index();
            $table->foreign('batch_id')->references('id')->on('batches');

            $table->date('scheduled_date');
            $table->json('scheduled_time');

            $table->unsignedBigInteger('created_by')->index();
            $table->foreign('created_by')->references('id')->on('users');

            $table->unsignedBigInteger('updated_by')->index();
            $table->foreign('updated_by')->references('id')->on('users');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
