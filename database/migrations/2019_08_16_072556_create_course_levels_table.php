<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseLevelsTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | CourseLevels Table Migration (Master)
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for CourseLevels throughout the application.
    |
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_levels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('level')->comment('1.Jumpstart 2.Level-1 3.Level-2');
            $table->string('slug');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_levels');
    }
}
