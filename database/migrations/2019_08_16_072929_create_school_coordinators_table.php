<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolCoordinatorsTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | SchoolCoordinators Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for SchoolCoordinators throughout the application.
    | This is populated by ModoAdmin.
    | This is populated when Modoadmin assigns a coordinator for the particular school.
    |
    /**
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('school_coordinators', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('school_id')->index();
            $table->foreign('school_id')->references('id')->on('schools');

            $table->unsignedBigInteger('coordinator_id')->index();
            $table->foreign('coordinator_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_coordinators');
    }
}
