<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchesTable extends Migration
{

    /*
    |--------------------------------------------------------------------------
    | Batches Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for Batches throughout the application.
    | This is populated by ModoAdmin.
    | This is populated when Modoadmin creates cluster, batches.
    |
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batches', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('cluster')->comment('class');
            $table->string('batch')->comment('batch');

            $table->unsignedBigInteger('coordinator_id')->index();
            $table->foreign('coordinator_id')->references('id')->on('users');

            $table->unsignedBigInteger('school_course_id')->index();
            $table->foreign('school_course_id')->references('id')->on('school_courses');

            $table->integer('modules_per_day')->comment('To track calendar for the coordinator');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batches');
    }
}
