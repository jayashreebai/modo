<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseContentsTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | CourseContents Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for CourseContents throughout the application.
    | This is populated by ModoAdmin.
    | This is populated when Modoadmin does (course-customization).
    |
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_contents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('school_course_id')->index();
            $table->foreign('school_course_id')->references('id')->on('school_courses');

            $table->unsignedBigInteger('module_id')->index();
            $table->foreign('module_id')->references('id')->on('modules');
            /**
            * Lessons are stored in the format of json.
            *[{
            *   id,
            *   name,
            *   description
            *   video_url,
            *   order,
            *   deleted
            * }]
            */
            $table->json('module_content')->comment('basically lesson json');
            $table->boolean('active')->default(1);
            $table->integer('order');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_contents');
    }
}
