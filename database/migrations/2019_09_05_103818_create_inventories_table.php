<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Inventory Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for inventory for the school throughout the application.
    |  This is populated by ModoAdmin.
    |
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->unsignedBigInteger('school_id')->index();
            $table->foreign('school_id')->references('id')->on('schools');

            $table->unsignedBigInteger('category_id')->index();
            $table->foreign('category_id')->references('id')->on('inventory_categories');

            $table->string('quantity');
            $table->string('serial_number');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
