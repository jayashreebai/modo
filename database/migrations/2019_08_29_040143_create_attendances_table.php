<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Attendence Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for attendence for students batchwise throughout the application.
    |
    |
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->date('entered_date');

            $table->unsignedBigInteger('student_id')->index();
            $table->foreign('student_id')->references('id')->on('students');

            $table->unsignedBigInteger('batch_id')->index();
            $table->foreign('batch_id')->references('id')->on('batches');

            $table->unsignedBigInteger('created_by')->index();
            $table->foreign('created_by')->references('id')->on('users');

            $table->boolean('present')->comment('1. Present 0. Absent');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
