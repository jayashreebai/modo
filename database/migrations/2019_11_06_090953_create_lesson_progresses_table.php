<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonProgressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_progresses', function (Blueprint $table) {
            $table->bigIncrements('id');            
            
            $table->unsignedBigInteger('school_course_id')->index();
            $table->foreign('school_course_id')->references('id')->on('school_courses');
            
            $table->unsignedBigInteger('batch_id')->index()->nullable();
            $table->foreign('batch_id')->references('id')->on('batches');
            
            $table->unsignedBigInteger('course_content_id')->index();
            $table->foreign('course_content_id')->references('id')->on('course_contents'); 
            
            $table->unsignedBigInteger('module_id')->index();
            $table->foreign('module_id')->references('id')->on('modules');
            
            $table->string('lesson_hash_id');
            $table->date('entered_date');

            $table->unsignedBigInteger('updated_by')->index();
            $table->foreign('updated_by')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_progresses');
    }
}
