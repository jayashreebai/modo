<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryCategoriesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Inventory Categories Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for inventory category throughout the application.
    |  This is populated by ModoAdmin.
    |
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_name')->comment('Pc, screen');
            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_categories');
    }
}
