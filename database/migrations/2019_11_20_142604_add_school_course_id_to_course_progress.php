<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchoolCourseIdToCourseProgress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_progresses', function (Blueprint $table) {
            $table->unsignedBigInteger('school_course_id')->index()->nullable();
            $table->foreign('school_course_id')->references('id')->on('school_courses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_progresses', function (Blueprint $table) {
            //
        });
    }
}
