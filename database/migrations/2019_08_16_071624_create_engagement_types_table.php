<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEngagementTypesTable extends Migration
{
    /*
     |--------------------------------------------------------------------------
     | EngagementTypes Table Migration (Master)
     |--------------------------------------------------------------------------
     |
     | This file contains all the fields which will be used
     | for EngagementTypes throughout the application.
     |
      */

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engagement_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 20)->comment('1.Unlimited 2. By class 3. By student');
            $table->string('slug');
            $table->string('description', 100);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engagement_types');
    }
}
