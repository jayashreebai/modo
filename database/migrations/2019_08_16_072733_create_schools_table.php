<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{

    /*
    |--------------------------------------------------------------------------
    | Schools Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for Schools throughout the application.
    | This is populated by ModoAdmin.
    |
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('school');
            $table->string('logo')->nullable();
            $table->string('slug');
            $table->text('address');
            $table->string('website_url');

            $table->unsignedBigInteger('school_admin_id')->index();
            $table->foreign('school_admin_id')->references('id')->on('users');

            $table->unsignedBigInteger('engagement_type_id')->index();
            $table->foreign('engagement_type_id')->references('id')->on('engagement_types');

            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
