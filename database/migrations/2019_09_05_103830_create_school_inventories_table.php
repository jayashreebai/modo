<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolInventoriesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | School Inventories Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for school inventory daily data for the school throughout the application.
    |  This is populated by coordinator.
    |
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('inventory_id')->index();
            $table->foreign('inventory_id')->references('id')->on('inventories');

            $table->date('entered_date');

            $table->integer('working')->default(0);
            $table->integer('not_working')->default(0);
            $table->integer('lost')->default(0);

            $table->unsignedBigInteger('updated_by')->index();
            $table->foreign('updated_by')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_inventories');
    }
}
