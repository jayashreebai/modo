<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Gallery Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for gallery for the school throughout the application.
    |  This is populated by coordinator, school-admin, admin
    |
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('description');
            $table->string('url');

            $table->unsignedBigInteger('school_id')->index();
            $table->foreign('school_id')->references('id')->on('schools');

            $table->unsignedBigInteger('added_by')->index();
            $table->foreign('added_by')->references('id')->on('users');

            $table->unsignedBigInteger('updated_by')->index();
            $table->foreign('updated_by')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');
    }
}
