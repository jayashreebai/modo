<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Courses Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for Courses throughout the application.
    |  This is populated by ModoAdmin.
    |
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('course');
            $table->string('slug');
            $table->text('description');
            $table->string('video_url')->comment('video link for each course to explain about particular course');
            $table->string('course_duration')->comment('total number of hours to complete particular course');

            $table->unsignedBigInteger('level_id')->index();
            $table->foreign('level_id')->references('id')->on('course_levels');

            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
