<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolCoursesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | SchoolCourses Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for SchoolCourses throughout the application.
    | This is populated by ModoAdmin.
    | This is populated when Modoadmin assigns a course to the school(course-customization).
    |
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_courses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('school_id')->index();
            $table->foreign('school_id')->references('id')->on('schools');

            $table->unsignedBigInteger('course_id')->index();
            $table->foreign('course_id')->references('id')->on('courses');

            $table->json('expiry_date')->comment('[{ from: , to: }]');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_courses');
    }
}
