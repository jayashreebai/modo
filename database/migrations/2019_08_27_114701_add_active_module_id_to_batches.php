<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveModuleIdToBatches extends Migration
{
    /**
    * This migration file is to add 'active_module_id' to batches table
    */

    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::table('batches', function (Blueprint $table) {
            $table->json('active_module_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batches', function (Blueprint $table) {
            $table->dropColumn('active_module_id');
        });
    }
}
