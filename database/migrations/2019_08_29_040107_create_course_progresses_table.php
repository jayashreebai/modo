<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseProgressesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | CourseProgresses Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for course progress for schools batchwise throughout the application.
    |
    |
    /**
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('course_progresses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('batch_id')->index()->nullable();
            $table->foreign('batch_id')->references('id')->on('batches');

            $table->unsignedBigInteger('course_content_id')->index();
            $table->foreign('course_content_id')->references('id')->on('course_contents');

            $table->date('entered_date');
            $table->json('module_completed')->comment('module json');

            $table->unsignedBigInteger('completed_by')->index();
            $table->foreign('completed_by')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_progresses');
    }
}
