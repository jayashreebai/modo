<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | Modules Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for Modules throughout the application which belongs to particular course.
    | This is populated by ModoAdmin.
    |
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('module');
            $table->integer('order');
            $table->string('slug');

            /**
             * [{
             *       id,
             *      name,
             *      description
             *      video_url,
             *      order,
             *      deleted,
             *      quiz:[{
             *      question,
             *      options: [{
             *      option: "dfd"
             *     correct_answer: true
             *      }, {
             *     option: "dfd"
             *      correct_answer: true
             *      },{
             *      option: "dfd"
             *      correct_answer: true
             *      },{
             *      option: "dfd"
             *      correct_answer: true
             *      }]
             *      }]
             *      }]
             */
            $table->json('lessons')->comment('video link for each course to explain about particular course');

            $table->unsignedBigInteger('course_id')->index();
            $table->foreign('course_id')->references('id')->on('courses');

            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
