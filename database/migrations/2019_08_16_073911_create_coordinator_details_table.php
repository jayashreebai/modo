<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoordinatorDetailsTable extends Migration
{
    /*
    |--------------------------------------------------------------------------
    | CoordinatorDetails Table Migration
    |--------------------------------------------------------------------------
    |
    | This file contains all the fields which will be used
    | for CoordinatorDetails throughout the application.
    | This is populated by ModoAdmin.
    | This is populated when Modoadmin creates coordinator).
    |
    /**
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordinator_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('coordinator_id')->index();
            $table->foreign('coordinator_id')->references('id')->on('users');

            $table->string('qualification');
            $table->string('occupation');
            $table->text('address');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordinator_details');
    }
}
