{{-- This blade template will be displayed to the user on mail
 when password is changed --}}

 @component('mail::layout')

 {{-- Header --}}
 @slot('header')
     @component('mail::header', ['url' => config('app.url')])
         Password changed
     @endcomponent
 @endslot

 {{-- Body --}}
 Hello {{ $user->first_name }},


 @slot('subcopy')
     Your password has been changed.

     Regards,<br>
     {{ config('app.name') }}
 @endslot

 {{-- Subcopy --}}
 @isset($subcopy)
     @slot('subcopy')
         @component('mail::subcopy')
             {{ $subcopy }}
         @endcomponent
     @endslot
 @endisset

 {{-- Footer --}}
 @slot('footer')
     @component('mail::footer')
         © {{ date('Y') }} {{ config('app.name') }}
     @endcomponent
 @endslot
@endcomponent
