{{-- This blade template will be displayed to the user on mail--}}
 {{--when user will click "Forgot Password?" link --}}

 {{--@component('mail::layout')--}}

 {{-- Header --}}
 {{--@slot('header')--}}
     {{--@component('mail::header', ['url' => config('app.url')])--}}
         {{--Reset password--}}
     {{--@endcomponent--}}
 {{--@endslot--}}

 {{-- Body --}}
 {{--Hi {{ $user->first_name }},--}}


 {{--@slot('subcopy')--}}
     {{--Welcome to Modo.<br>--}}

     {{--  Password reset link is given bellow. Your password won't change until you access the link below and create a new one.  --}}
     {{--Please click on the link below to reset your password--}}


     {{--@component('mail::button', ['url' => 'https://ecole.zysk.in/set-password/'.$token])--}}

         {{--Reset password--}}
     {{--@endcomponent--}}

     {{--If you didn't request to reset your password, please ignore this email.<br><br>--}}


     {{--Cheers,<br>--}}
     {{--Modo Team--}}
     {{--  {{ config('app.name') }}  --}}
 {{--@endslot--}}

 {{-- Subcopy --}}
 {{--@isset($subcopy)--}}
     {{--@slot('subcopy')--}}
         {{--@component('mail::subcopy')--}}
             {{--{{ $subcopy }}--}}
         {{--@endcomponent--}}
     {{--@endslot--}}
 {{--@endisset--}}

 {{-- Footer --}}
 {{--@slot('footer')--}}
     {{--@component('mail::footer')--}}
         {{--© {{ date('Y') }} {{ config('app.name') }}--}}
     {{--@endcomponent--}}
 {{--@endslot--}}
{{--@endcomponent--}}


<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style>
        .logo {
            text-align: center;
            margin-bottom: 1%;
        }

        .email {
            background-color: whitesmoke;
            padding: 35px 0px;
        }

        .content-body {
            background-color: white;
            margin: 0px 30px;
            padding: 30px;

            border-radius: 7px;
            color: #8c8c8c;
            width: 50%;
            margin: auto;
        }

        .content-body h3 {
            color: #696767;
        }

        .footer-btn {
            text-align: center;
            width: 50%;
            margin: 20px 0px;
            margin-left: auto;
            margin-right: auto;
            padding: 10px;
            background: darkgreen;
            border: 1px solid darkgreen;
            color: white;
            text-decoration: none;
            border-radius: 3px;
        }

        .footer {
            text-align: center;
            margin-top: 1%;
            color: #8c8c8c;
        }
    </style>
</head>

<body>
<div class="email">
    @if($flag == 1)
    <div class="content-body">
        <h3 align="center"> Create password instructions</h3>
        <p>Hello {{$user->first_name}},</p>
        <p>Welcome to the Modo Ecole web application. A unique login has been created for your course access. For security reasons, we recommend that you reset the password the first time you access.</p>
        <p>Click below to reset your password.</p>
        <div class="px-5 mx-5">
            <a style="text-decoration: none;" href="https://ecole.zysk.in/set-password/{{$token}}">
                <div class="footer-btn">Create password</div>
            </a>
        </div>
        Alternatively, open the following link in your browser: <br/>
        <p>http://ecole.zysk.in/set-password/{{$token}}	</p>
        <p>If you are not sure why you received this email (or) unable to click the above link, please contact us immediately. </p>
        <p>
            Email: contact@heymodo.com<br>
            Phone: 9742306333
        </p>
        <div>Regards,</div>
        <div>Modo</div>
    </div>
    @elseif($flag == 3)
        <div class="content-body">
            <h3 align="center"> Create password instructions</h3>
            <p>Hello {{$user->first_name}},</p>
            <p>Welcome to the Modo Ecole web application. A unique login has been created for you. For security reasons, we recommend that you reset the password the first time you access.</p>
            <p>Click below to reset your password.</p>
            <div class="px-5 mx-5">
                <a style="text-decoration: none;" href="https://ecole.zysk.in/set-password/{{$token}}">
                    <div class="footer-btn">Create password</div>
                </a>
            </div>
            Alternatively, open the following link in your browser: <br/>
            <p>http://ecole.zysk.in/set-password/{{$token}}	</p>
            <p>If you are not sure why you received this email (or) unable to click the above link, please contact us immediately. </p>
            <p>
                Email: contact@heymodo.com<br>
                Phone: 9742306333
            </p>
            <div>Regards,</div>
            <div>Modo</div>
        </div>
    @else
        <div class="content-body">
            <h3 align="center"> Reset password instructions</h3>
            <p>Hello {{$user->first_name}},</p>
            <p>Welcome to Modo.</p>
            <p>Password reset link is given below :</p>
            <div class="px-5 mx-5">
                <a style="text-decoration: none;" href="https://ecole.zysk.in/set-password/{{$token}}">
                    <div class="footer-btn">Reset password</div>
                </a>
            </div>
            Alternatively, open the following link in your browser: <br/>
            <p>http://ecole.zysk.in/set-password/{{$token}}	</p>
            <p>If you are not sure why you received this email (or) unable to click the above link, please contact us immediately. </p>
            <p>
                Email: contact@heymodo.com<br>
                Phone: 9742306333
            </p>
            <div>Regards,</div>
            <div>Modo</div>
        </div>
    @endif
        <div class="footer">
        {{--<img src="copyright.png" alt="copyright" width="12" height="12">--}}

        ©2019 <a href="">
            Modo</a> All rights reserved</div>
</div>


</body>
</html>


























