<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>

        .content-body {
            background-color: white;
            margin: 0 30px;
            padding: 30px;

            border-radius: 7px;
            color: #8c8c8c;
            width: 50%;
            margin: auto;
        }

        .content-body h3 {
            color: #696767;
        }

        .footer-btn {
            text-align: center;

            margin: 20px 0;
            padding: 10px;
            background: #f49150;
            border: 1px solid #f49150;
            color: white;
            text-decoration: none;
            border-radius: 3px;
        }

        .footer {
            text-align: center;
            margin-top: 1%;
            color: #8c8c8c;
        }
    </style>
</head>
<body>
<div class="content-body">
    <h3 align="center">Event Notification</h3>
    <p>Hello {{$user->first_name}},</p>
    <p>Welcome to the Modo Ecole web application.</p>
<p>
    @if ($date == 0)
    You have a schedule today, for the batch : {{$batch->batch}} , Cluster : {{$batch->cluster}} , please have look.
    @else
    You have {{$date}} {{$date == 1? 'day':'days'}} left to take the class for the batch : {{$batch->batch}} , Cluster : {{$batch->cluster}}.
    @endif
</p>
    <div>Regards,</div>
    <div>Modo</div>
</div>
</body>
</html>