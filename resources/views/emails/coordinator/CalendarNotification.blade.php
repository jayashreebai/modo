<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style>
        .logo {
            text-align: center;
            margin-bottom: 1%;
        }

        .email {
            background-color: whitesmoke;
            padding: 35px 0px;
        }

        .content-body {
            background-color: white;
            margin: 0px 30px;
            padding: 30px;

            border-radius: 7px;
            color: #8c8c8c;
            width: 50%;
            margin: auto;
        }

        .content-body h3 {
            color: #696767;
        }

        .footer-btn {
            text-align: center;
            width: 50%;
            margin: 20px 0px;
            margin-left: auto;
            margin-right: auto;
            padding: 10px;
            background: darkgreen;
            border: 1px solid darkgreen;
            color: white;
            text-decoration: none;
            border-radius: 3px;
        }

        .footer {
            text-align: center;
            margin-top: 1%;
            color: #8c8c8c;
        }
    </style>
</head>

<body>
<div class="email">
        <div class="content-body">
            <h3 align="center">Event Notification</h3>
            <p>Hello {{$calendar->coordinator->first_name}},</p>
            <p>Welcome to the Modo Ecole web application.</p>
            @if($flag==1)
            <p>Class has been scheduled on {{\Carbon\Carbon::parse($calendar->scheduled_date)->format('d-m-Y')}} at {{\Carbon\Carbon::parse(json_decode($calendar->scheduled_time)->from)->format('H:m')}} for Batch : {{$batch->batch}} Cluster : {{$batch->cluster}}.<br>
                You will be able to access on {{\Carbon\Carbon::parse($calendar->scheduled_date)->subDay()->format('d-m-Y')}}.</p>
            @elseif($flag==2)
                <p>Class which was scheduled on {{\Carbon\Carbon::parse($calendar->scheduled_date)->format('d-m-Y')}} has been cancelled.</p>
            @endif

            <div>Regards,</div>
            <div>Modo</div>
        </div>


    <div class="footer">
        ©2019 <a href="">
            Modo</a> All rights reserved
    </div>
</div>


</body>
