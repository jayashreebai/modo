@component('mail::message')
Hi Team,

You have a question from customer,

 {{ $data->question }} , 

Below is the customer details :

<ol>Name     :  {{ $data->name }}</ol>
<ol>Email    :  {{ $data->email }}</ol>
<ol>Contact  :  {{ $data->contact }}</ol>

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
Support Team
@endcomponent
