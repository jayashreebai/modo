# Modo Ecole app

Laravel 5.8 application for Modo ecole app. 

## Dependencies
- PHP >= 7.1.3
- Composer  
- Laravel 5.8


## Installation
Clone the project and install dependencies

`composer install`

## Install npm dependencies
`npm install`

## Run database migrations
`php artisan migrate`

## Run application
`php artisan serve`
