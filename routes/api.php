<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| 
*/


/*
|--------------------------------------------------------------------------
| ROUTES FOR USER MANAGEMENT
|--------------------------------------------------------------------------
|
| Followings are the routes for user management
|
*/

Route::group([

    'middleware' => ['cors'],

], function () {

    /**
     * Routes for forgot password
     */
    // 1. Send verification token
    Route::post('password/email', 'Auth\ForgotPasswordController@getToken');
    // 2. Check if token got expired or not
    Route::get('password/{token}', 'Auth\ResetPasswordController@verifyToken');
    // 3. Based on token save the new password
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group([

    'middleware' => ['cors'],
    'prefix' => 'auth',

], function () {
    /**
     * User Login Routes
     */
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('refresh/{token}', 'Auth\LoginController@refresh');
    Route::post('me', 'Auth\LoginController@me');
    /**
     * Route for change password
     */
    Route::post('password/change', 'Auth\ChangePasswordController@changePassword');
});


/*
|--------------------------------------------------------------------------
| ROUTES FOR COURSE MANAGEMENT
|--------------------------------------------------------------------------
|
| Followings are the routes for course management
|
*/

Route::group([

    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'course',

], function () {
    Route::post('store', 'Course\CourseController@store');
    Route::get('active/{flag}', 'Course\CourseController@activeCourses');
    Route::get('list', 'Course\CourseController@index');
    Route::post('update/{course}', 'Course\CourseController@update');    
    Route::post('{course}', 'Course\CourseController@destroy');
});


Route::group([
    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'module',

], function () {
    Route::post('store', 'Course\ModuleController@store');
    Route::post('{module}/update', 'Course\ModuleController@update');
    Route::post('{module}', 'Course\ModuleController@destroy');
    Route::post('reorder/{course}', 'Course\ModuleController@reorder');
});

Route::post('module-active', 'Course\ModuleController@updateActive');

Route::get('school', 'School\SchoolController@index');



Route::group([
    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'lesson',

], function () {
    // Route::get('lesson-details/{module}/{lessonId}', 'Course\LessonController@lessonDetail');
    Route::post('reorder-lesson', 'Course\LessonController@reorderLesson');
    Route::post('add-lesson-detail', 'Course\LessonController@storeLessonDetail');

    Route::post('store', 'Course\LessonController@store');
    Route::post('update', 'Course\LessonController@update');
    Route::post('delete/{active}', 'Course\LessonController@destroy');
    // Route::get('index/{module}', 'Course\LessonController@index');
    // Route::get('show/{module}/{lessonId}', 'Course\LessonController@show');
});



/*
|--------------------------------------------------------------------------
| ROUTES FOR SCHOOL MANAGEMENT
|--------------------------------------------------------------------------
|
| Followings are the routes for school management
|
*/
Route::group([

    'middleware' => ['cors', 'isAdmin'], 
    'prefix' => 'school',

], function () {
    Route::post('store', 'School\SchoolController@store');
    Route::post('update/{school}', 'School\SchoolController@update');
    Route::post('delete/{school}', 'School\SchoolController@destroy');
    Route::post('assign-course/{school}', 'School\SchoolController@assignCourse');
    Route::post('unassign-course/{school}', 'School\SchoolController@unAssignCourse');

    //school-course routes

    // Route::get('course/{school}', 'School\SchoolController@course');
    // Route::get('{schoolCourseId}/modules', 'School\SchoolController@modules');

    //  Course Customisation for school  
    Route::post('delete-module/{courseContent}/{active}', 'School\SchoolController@deleteModule');
    Route::post('delete-lesson/{courseContent}/{active}', 'School\SchoolController@deleteLesson');
    Route::post('reorder-lesson/{courseContent}', 'School\SchoolController@reorderLessonForCourseContent');
    Route::post('reorder-module/{courseContent}', 'School\SchoolController@reorderModuleForCourseContent');
    Route::get('course-customization/{schoolCourse}', 'School\SchoolController@courseCustomization');
    Route::get('all-lesson/{courseContent}', 'Course\LessonController@allLesson');
    Route::get('lesson-detail/{courseContent}/{lesson}', 'Course\LessonController@lessonDetail');


    //  Route for adding school contact detail
    // Route::resource('contact', 'School\SchoolContactDetailController')->except(['edit', 'create', 'show']);
    // Route::post('update-contact/{contact}', 'School\SchoolContactDetailController@update');
    // Route::get('contacts/{schoolId}', 'School\SchoolContactDetailController@index');
    // Route::get('master/engagement-type', 'School\EngagementTypeController@index');

    //  Calender feature api
    // Route::post('create-calender/{batchId}', 'School\CalenderFeatureController@createCalender');
    // Route::post('batch-calender/{batch}', 'School\CalenderFeatureController@getCalenderForBatch');
    // Route::get('coordinator-calender/{batch}/{coordinatorId}', 'School\CalenderFeatureController@getCalenderForBatchAndCoordinator');
    // Route::post('cron-job', 'School\CalenderFeatureController@cronJob');
    //  API for getting School admin, coordinators , contact and courses
    // Route::get('all-detail-for-school/{school}', 'School\SchoolController@getAllDetailForSchool');
});

/*
|--------------------------------------------------------------------------
| ROUTES FOR SCHOOL-ADMIN
|--------------------------------------------------------------------------
|
| Followings are the routes for school-admin
|

*/

Route::group([

    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'school-admin',

], function () {
    Route::post('store', 'School\SchoolAdminController@store');
    Route::post('update/{school}', 'School\SchoolAdminController@update');
    Route::get('list/{school}', 'School\SchoolAdminController@index');
});

/*
|--------------------------------------------------------------------------
| ROUTES RELATED TO STUDENTS
|--------------------------------------------------------------------------
|
| Followings are the routes related to student
|
*/

Route::group([

    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'student',
 
], function () {
    Route::post('import-student/{schoolId}', 'School\StudentController@importStudent');
    Route::post('store/{batch}', 'School\StudentController@store');
    Route::post('update/{student}', 'School\StudentController@update');
    Route::post('delete/{student}', 'School\StudentController@destroy');
});

/*
|--------------------------------------------------------------------------
| ROUTES FOR COORDINATOR
|--------------------------------------------------------------------------
|
| Followings are the routes for coordinator
|
*/
Route::group([

    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'coordinator',

], function () {
    Route::post('store/{school}', 'School\CoordinatorController@store');
    Route::post('update/{user}', 'School\CoordinatorController@update');//modoadmin will update this
    Route::post('delete', 'School\CoordinatorController@destroy');
    // Route::get('{user}/batches', 'School\CoordinatorController@batches');
    Route::post('complete-module/{batchId}', 'School\CoordinatorController@completeModule');
});

/*
|--------------------------------------------------------------------------
| ROUTES FOR BATCH
|--------------------------------------------------------------------------
|
| Followings are the routes for batch.
|
*/
Route::group([

    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'batch',

], function () {
    Route::post('store/{school}', 'School\BatchController@store');
    Route::post('update/{school}', 'School\BatchController@update');
});

/*
|--------------------------------------------------------------------------
| ROUTES FOR ANNOUNCEMENT MANAGEMENT
|--------------------------------------------------------------------------
|
| Followings are the routes for announcement management
|
*/
Route::group([
 
    'middleware' => ['cors'],
    'prefix' => 'announcement',

], function () {
    Route::post('store', 'School\AnnouncementController@store');
    Route::post('update/{announcement}', 'School\AnnouncementController@update');
    Route::post('delete/{announcement}', 'School\AnnouncementController@destroy');
    Route::get('list', 'School\AnnouncementController@index');
    Route::get('school/{schoolcourse}', 'School\AnnouncementController@announcementsForSchool');
    Route::get('get/{announcement}', 'School\AnnouncementController@getAnnouncement');
});

/*
|--------------------------------------------------------------------------
| ROUTES FOR INVENTORY MANAGEMENT
|--------------------------------------------------------------------------
|
| Followings are the routes for inventory management
|
*/
Route::group([

    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'inventory-category', 

], function () {
    Route::post('store', 'School\InventoryController@store');
    Route::post('update/{inventoryCategory}', 'School\InventoryController@update');
    Route::post('delete/{inventoryCategory}', 'School\InventoryController@destroy');
    Route::get('list', 'School\InventoryController@index');
});

Route::group([

    'middleware' => ['cors', 'isAdmin'],
    'prefix' => 'inventory',

], function () {
    Route::post('assign/{school}', 'School\InventoryController@assign');//assign items to school
    Route::post('unassign/{inventory}', 'School\InventoryController@unAssign');//unassign items to school
    Route::post('update/{inventory}', 'School\InventoryController@inventoryUpdateSchool');//update inventory for school

});

//==================common routes accessible for all roles==========================//
Route::get('cron-job', 'School\CalendarController@notify');
Route::get('cron-job2', 'School\CalendarController@getEventNotification');

Route::group([  
    'middleware' => ['cors'] 
], function () {
    Route::get('module/{course}', 'Course\ModuleController@index');

    Route::get('lesson/lesson-details/{module}/{lessonId}', 'Course\LessonController@lessonDetail');
    Route::get('lesson/index/{module}', 'Course\LessonController@index');
    Route::get('lesson/show/{module}/{lessonId}', 'Course\LessonController@show');

    Route::get('school/{school}', 'School\SchoolController@show');
    //school-course routes
    Route::get('school/course/{school}', 'School\SchoolController@course');
    Route::get('school/{schoolCourseId}/modules', 'School\SchoolController@modules');
    //  Route for adding school contact detail
    Route::resource('school/contact', 'School\SchoolContactDetailController')->except(['edit', 'create', 'show']);
    Route::post('school/update-contact/{contact}', 'School\SchoolContactDetailController@update');
    Route::get('school/contacts/{schoolId}', 'School\SchoolContactDetailController@index');
    Route::get('school/master/engagement-type', 'School\EngagementTypeController@index');
    //  Calender feature api
    Route::post('school/create-calendar/{batchId}', 'School\CalendarController@store');
    Route::get('school/batch-calendar/{batch}', 'School\CalendarController@getCalenderForBatch');
    Route::get('school/coordinator-calendar/{batch}/{coordinatorId}', 'School\CalendarController@getCalenderForBatchAndCoordinator');
    //  API for getting School admin, coordinators , contact and courses
    Route::get('school/all-detail-for-school/{school}', 'School\SchoolController@getAllDetailForSchool');

    Route::get('coordinator/batches', 'School\CoordinatorController@batches');

    Route::get('announcement/{school}', 'School\AnnouncementController@announcementsForSchool');

    Route::post('attendance/store/{batch_id}', 'School\AttendanceController@store');
    Route::post('attendance/update/{attendance_id}', 'School\AttendanceController@update');
    Route::get('attendance/{batch_id}/{date}', 'School\AttendanceController@index');

    Route::post('inventory/update', 'School\InventoryController@inventoryUpdate');

    Route::post('gallery/store', 'School\GalleryController@upload');

    Route::post('gallery/delete/{galleryId}', 'School\GalleryController@destroy');
    Route::post('gallery/update/{galleryId}', 'School\GalleryController@updateDescription');

    Route::get('gallery/list/{schoolcourse}', 'School\GalleryController@index');

    Route::get('level','Course\CourseController@level');    
    Route::get('engagement-type/{school?}', 'School\EngagementTypeController@show'); //get school engagement type
    Route::post('engagement-type/update/{school}', 'School\EngagementTypeController@update'); //update engagement-type for school

    Route::get('coordinator/list/{school}', 'School\CoordinatorController@index');

    Route::get('batch/list/{school}', 'School\BatchController@index');
    Route::post('batch/delete/{batch_id}', 'School\BatchController@destroy');

    Route::get('course-for-school/{school}', 'School\SchoolController@courseForSchool');
    Route::get('course-details/{course}', 'Course\CourseController@show');
    Route::post('update-course', 'School\SchoolController@updateCourseDate');
    Route::post('school/update-calendar/{calendar}', 'School\CalendarController@update');
    Route::get('school/delete-calendar/{calendar}', 'School\CalendarController@destroy');

    Route::post('lesson_complete/{lesson_id}','School\CoordinatorController@lessonComplete');   

    Route::get('stages/{school}', 'School\SchoolController@stages');    

    Route::get('student/list/{batch}', 'School\StudentController@index');
    Route::get('modules/list/{batch}/{schoolCourseId}', 'School\CoordinatorController@moduleList');
    Route::get('lesson/{lessonId}/{contentId}/{batchId}', 'School\CoordinatorController@lessonDetails');
    Route::post('contact-heymodo', 'School\SchoolContactDetailController@contactHeymodo');//contact
    Route::post('quiz-answer', 'School\CoordinatorController@quizData');
    Route::get('lesson-list/{contentId}/{batchId}', 'School\CoordinatorController@lessonList');
    Route::get('attendance-date/{batchId}', 'School\StudentController@attendenceDate');
    Route::get('cal-attendance-date/{batchId}', 'School\StudentController@attendenceCalendarDate');


    Route::get('batch-list', 'School\CoordinatorBatchCourseController@coordinatorBatchlist');
    Route::post('course-content', 'School\CoordinatorBatchCourseController@schoolCourseContent');
    Route::post('lesson-content', 'School\CoordinatorBatchCourseController@lessonContentForGivenHashId');
    Route::post('complete-lesson/{lesson_id}','School\CoordinatorBatchCourseController@completeLesson');
    Route::get('module-completion-date/{batch_id}','School\CoordinatorBatchCourseController@lessonWithCompletionDate');
//  For unlimited type school routes
    Route::post('course-content-unlimited', 'School\CoordinatorBatchCourseController@unlimitedSchoolCourseContent');
    Route::post('lesson-list-unlimited', 'School\CoordinatorBatchCourseController@lessonList');

    Route::get('school-list', 'School\SchoolController@schoolListAccrodindToUser');
    Route::get('inventory-list/{school}', 'School\InventoryManagementController@listInventory');//inventories for school
    Route::post('inventory-management', 'School\InventoryManagementController@updateInventoryManagement');//inventories for school
    Route::post('list-inventory-management', 'School\InventoryManagementController@listManageInventory');//inventories for school

    Route::get('list/{cid}', 'Course\LessonController@updateCourseContent');//inventories for school
    Route::get('inventory/list/{school}', 'School\InventoryController@inventoriesSchool');//inventories for school

});
